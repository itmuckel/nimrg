# Package

version       = "0.1.0"
author        = "itmuckel"
description   = "A new awesome nimble package"
license       = "Apache-2.0"
srcDir        = "src"

# Dependencies

requires "nim >= 1.0.4"
requires "nimterop >= 0.4.4"
requires "winim >= 3.0"