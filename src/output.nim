﻿# Overriding ENET_BUFFER_MAXIMUM zpl_opts_entry_sub zpl_opts_entry zpl_xy zpl_xyz zpl_rgb zpl_vec2 zpl_vec3 zpl_xyzw zpl_rgba zpl_xy_zw_vec4 zpl_vec4 zpl_vec2_mat2 zpl_mat2 zpl_vec3_mat3 zpl_mat3 zpl_vec4_mat4 zpl_mat4 zpl_quat in6_addr ENetHost zpl_atomic_ptr zpl_affinity zpl_json_object_real zpl_json_object_union zpl_json_object

{.passC: "-IC:/Users/Admin/Documents/Code/nim/nimrg/include".}
# Importing C:\Users\Admin\Documents\Code\nim\nimrg\include\librg.h
# Generated at 2020-02-03T20:42:46+01:00
# Command line:
#   C:\Users\Admin\.nimble\pkgs\nimterop-0.4.4\nimterop\toast.exe --preprocess --recurse --includeDirs+=C:\Users\Admin\Documents\Code\nim\nimrg\include --pnim --dynlib=dynlibFile --symOverride=ENET_BUFFER_MAXIMUM,zpl_opts_entry_sub,zpl_opts_entry,zpl_xy,zpl_xyz,zpl_rgb,zpl_vec2,zpl_vec3,zpl_xyzw,zpl_rgba,zpl_xy_zw_vec4,zpl_vec4,zpl_vec2_mat2,zpl_mat2,zpl_vec3_mat3,zpl_mat3,zpl_vec4_mat4,zpl_mat4,zpl_quat,in6_addr,ENetHost,zpl_atomic_ptr,zpl_affinity,zpl_json_object_real,zpl_json_object_union,zpl_json_object --nim:C:\Users\Admin\.choosenim\toolchains\nim-1.0.4\bin\nim.exe --pluginSourcePath=C:\Users\Admin\nimcache\nimterop\cPlugins\nimterop_1483635604.nim C:\Users\Admin\Documents\Code\nim\nimrg\include\librg.h

{.hint[ConvFromXtoItselfNotNeeded]: off.}

import nimterop/types



# 
# 
#  Custom Allocation
# 
# 
defineEnum(zplAllocationType)
defineEnum(zplAllocatorFlag)
defineEnum(zpl_regex_error)
defineEnum(zpl_file_mode_flag)

#  NOTE: Only used internally and for the file operations
defineEnum(zpl_seek_whence_type)
defineEnum(zpl_file_error)
defineEnum(zpl_dir_type)
defineEnum(zpl_file_standard_type)

# ! @}
# * @file json.c
# @brief JSON5 Parser/Writer
# @defgroup json JSON5 parser/writer
# Easy to use and very fast JSON5 parser that can easily load 50 megabytes of JSON content under half a second. It also contains simple JSON5 writer and acts as a good library for handling config files. The parser also supports the **Simplified JSON (SJSON)** format.
# We can parse JSON5 files in two different modes:
#     @n 1) Fast way (useful for raw data), which can not handle comments and might cause parsing failure if comment is present.
#     @n 2) Slower way (useful for config files), which handles comments perfectly but **might** have performance impact
#         on bigger JSON files. (+50MiB)
# @{
# 
# ! Debug mode
# ! JSON object types
defineEnum(zpl_json_type)

# ! Field value properties
defineEnum(zpl_json_props)

# ! Value constants
defineEnum(zpl_json_const)

#  TODO(ZaKlaus): Error handling
# ! Parser error types
defineEnum(zpl_json_error)

# ! Field name decoration style
defineEnum(zpl_json_naming_style)

# ! Field value assign style
defineEnum(zpl_json_assign_style)

# ! Field delimiter style
defineEnum(zpl_json_delim_style)

# ! @}
# * @file opts.c
# @brief CLI options processor
# @defgroup cli CLI options processor
#  Opts is a CLI options parser, it can parse flags, switches and arguments from command line and offers an easy way to express input errors as well as the ability to display help screen.
# @{
# 
defineEnum(zpl_opts_types)
defineEnum(zpl_opts_err_type)

# ! @}
# * @file process.c
# @brief Process creation and manipulation methods
# @defgroup process Process creation and manipulation methods
# Gives you the ability to create a new process, wait for it to end or terminate it.
# It also exposes standard I/O with configurable options.
# @{
# 
#  TODO(zaklaus): Add Linux support
defineEnum(zpl_pr_opts)
defineEnum(zpl_jobs_status)
defineEnum(zpl_co_status)

#  =======================================================================
#  !
#  ! Protocol
#  !
#  =======================================================================
defineEnum(Enum_librgh1)
defineEnum(ENetProtocolCommand)
defineEnum(ENetProtocolFlag)

#  =======================================================================
#  !
#  ! General ENet structs/enums
#  !
#  =======================================================================
defineEnum(ENetSocketType)
defineEnum(ENetSocketWait)
defineEnum(ENetSocketOption)
defineEnum(ENetSocketShutdown)

# *
#      * Packet flag bit constants.
#      *
#      * The host must be specified in network byte-order, and the port must be in
#      * host byte-order. The constant ENET_HOST_ANY may be used to specify the
#      * default server host.
#      *
#      * @sa ENetPacket
# 
defineEnum(ENetPacketFlag)
defineEnum(ENetPeerState)
defineEnum(Enum_librgh2)

# *
#      * An ENet event type, as specified in @ref ENetEvent.
# 
defineEnum(ENetEventType)
defineEnum(librg_mode)
defineEnum(librg_space_type)
defineEnum(librg_pointer_type)
defineEnum(librg_thread_state)

#  =======================================================================
#  !
#  ! Entities
#  !
#  =======================================================================
defineEnum(librg_entity_flag)
defineEnum(librg_visibility)

#  =======================================================================
#  !
#  ! Events
#  !
#  =======================================================================
# *
#  * Default built-in events
#  * define your events likes this:
#  *     enum {
#  *         MY_NEW_EVENT_1 = LIBRG_LAST_EVENT,
#  *         MY_NEW_EVENT_2,
#  *         MY_NEW_EVENT_3,
#  *     };
# 
defineEnum(librg_events)
defineEnum(librg_event_flags)

#  =======================================================================
#  !
#  ! Options
#  !
#  =======================================================================
defineEnum(librg_options)

const

  # *
  #  * librg - a library for building simple and elegant cross-platform multiplayer client-server solutions.
  #  *
  #  * Usage:
  #  * #define LIBRG_IMPLEMENTATION exactly in ONE source file right BEFORE including the library, like:
  #  *
  #  * #define LIBRG_IMPLEMENTATION
  #  * #include <librg.h>
  #  *
  #  * Credits:
  #  * Vladyslav Hrytsenko (GitHub: inlife)
  #  * Dominik Madarasz (GitHub: zaklaus)
  #  *
  #  * Dependencies:
  #  * zpl.h
  #  * enet.h
  #  *
  #  * For the demo:
  #  * sdl2.h
  #  *
  #  * Version History:
  #  *
  #  * 5.0.5
  #  *  - Fixes to selection and deduplication flow
  #  *
  #  * 5.0.3
  #  *  - Minor fixes by @markatk
  #  *
  #  * 5.0.2
  #  *  - Fixed issue related to visiblity destruction
  #  *
  #  * 5.0.1
  #  *  - Fixed entity visiblity states after disconnection
  #  *
  #  * 5.0.0
  #  *  - Changed API for visibility feature:
  #  *    - Instead of booleans setting whether or not entity would be included in the query or not
  #  *      a multiple constant-based states were inroduced:
  #  *        - LIBRG_VISIBILITY_DEFAULT - the original state of visibility, entity is only visible if it is in the stream range
  #  *        - LIBRG_ALWAYS_VISIBLE - the entity is visible disregarding if it is the stream range or not
  #  *        - LIBRG_ALWAYS_INVISIBLE - opposite of the above, entity will be always invisible
  #  *      Entity visibility can be set on per entity-to-entity or global levels. The entity-to-entity relation has a bigger
  #  *      priority, so f.e. setting entity to be LIBRG_ALWAYS_VISIBLE on relation level, will override global visibility.
  #  *   - Aditionally, if the virtual world feature is enabled, it gains main priotity over visibility, thus entities located in 2 different
  #  *     virtual worlds will not be able to see each other in spite of visibility settings.
  #  *
  #  * 4.1.5
  #  *  - Fix to data send
  #  *
  #  * 4.1.4
  #  *  - Fixed issue with async flow inconsitensies of control_generations
  #  *  - Fixed boolean validation in disconnection flow
  #  *  - Added proxying of user_data from msg to evt in disconnect event
  #  *
  #  * 4.1.1
  #  * - Added compile-time 'features':
  #  *     - Ability to enable/disable some librg compile-time features
  #  *     - Entity igore tables are now optional, and can be disabled
  #  *     - Implmented simple optional Virtual world feature for entities
  #  *     - Implemented a feature to enable/disable octree culler (falls back to linear check)
  #  *     - Multiple features can be combined
  #  * - Added 'generation' to entity control lists:
  #  *     Setting, removing and setting control to the same entity again with same owner
  #  *     will now distinct between old and new controllers, and messages still coming
  #  *     from old control generation will be rejected in favor of new ones.
  #  * - Added guard to minimum sized packet in receive for both sides
  #  * - Added spherical culler handler, and ability to do runtime switch (LIBRG_USE_RADIUS_CULLING)
  #  * - Added return codes to some functions @markatk
  #  * - Streamed entities are now going to be always returned in query for controlling peer
  #  * - Fixed issue with host setting on the server side
  #  * - Fixed nullptr crash on empty host string for client on connect
  #  * - Removed experimental multithreading code
  #  *
  #  * 4.1.0
  #  * - Added new, extended message methods and sending options
  #  *
  #  * 4.0.0
  #  * - Coding style changes and bug fixes
  #  *
  #  * 3.3.1
  #  * - Updated zpl dependencies
  #  * - Removed zpl_math dependency (replaced by internal module in zpl)
  #  *
  #  * 3.3.0
  #  * - Added ipv6 support
  #  * - Added safe bitstream reads for internal methods
  #  * - Updated enet to latest version (2.0.1, ipv6 support)
  #  * - Updated zpl to latest version
  #  *
  #  * 3.2.0
  #  * - Fixed minor memory client-side memory leak with empty control list
  #  * - Fixed issue with client stream update and removed entity on server
  #  * - Updated zpl to new major version, watch out for possible incompatibilities
  #  * - Added method for alloc/dealloc the librg_ctx, librg_data, librg_event for the bindings
  #  * - Added experimental support for update buffering, disabled by default, and not recommended to use
  #  * - Added built-in timesyncer, working on top of monotonic time, syncing client clock to server one
  #  * - Added helper methods: librg_time_now, librg_standard_deviation
  #  * - Changed ctx->tick_delay from u16 to f64 (slightly more precision)
  #  *
  #  * Version History:
  #  * 3.1.0
  #  * - Removed zpl_cull and zpl_event dependencies
  #  * - added librg_network_kick()
  #  * - saving current librg_address to ctx->network
  #  * - refactor to proper disconnection code
  #  * - exclude local client entity from LIBRG_CONNECTION_DISCONNECT
  #  * - moved options and some few other things to the implementation part
  #  * - fixed issue with replacing entity control
  #  * - fixed issue with add control queuing beign sent before create entity packet
  #  *
  #  * 3.0.7 - Fix for entity query dublication for player entities
  #  * 3.0.5 - Patched librg_callback_cb arg value
  #  * 3.0.4 - Fixed Android and iOS support
  #  * 3.0.3 - Small fixes
  #  * 3.0.2 - Dependency updates
  #  * 3.0.1 - minor api patch
  #  * 3.0.0 - contexts, major api changes, fried potato, other stuff
  #  *
  #  * 2.2.3 - fixed mem leak on net event
  #  * 2.2.2 - Fixed client issue with librg_message_send_instream_except
  #  * 2.2.1 - Fixed cpp issues with librg_data pointers
  #  * 2.2.0 - Inner message system rafactor
  #  * 2.1.0 - Inner bitstream refactors, with slight interface changes
  #  * 2.0.2 - C++ and MSVC related fixes
  #  * 2.0.0 - Initial C version rewrite
  #  *
  #  * Things TODO:
  #  *     find a nice way to decrease size on client for peer struct
  #  *     try to remember stuff about bistram safety
  #  *     fix disconnection code issues
  #  * - Add method to check if entity is in stream of other entity
  #  * - Add DEBUG packet size validation (FEATURE)
  #  * - refactoring librg_table (FEATURE)
  #  * - remove entity ignore for target entity that was disconnected/deleted (BUG)
  #  * - remove controller peer for entity, on owner disconnect (BUG)
  #  * - possibly adding stream_range to the query, to make it bi-sided (FEATURE)
  #  * - tree/space node flattening (FEATURE)
  #  *
  #  * Useful links:
  #  * - https:antriel.com/post/online-platformer-5/#server-update-data
  #  * - https:github.com/fnuecke/Space/blob/5ddfec5fc8139c4b0def8fdfc16b5f5357a8cd38/EngineController/AbstractTssClient.cs#L325
  #  *
  #  * License notice:
  #  *
  #  * Copyright 2017-2019 Vladyslav Hrytsenko
  #  *
  #  * Licensed under the Apache License, Version 2.0 (the "License");
  #  * you may not use this file except in compliance with the License.
  #  * You may obtain a copy of the License at
  #  *
  #  *     http:www.apache.org/licenses/LICENSE-2.0
  #  *
  #  * Unless required by applicable law or agreed to in writing, software
  #  * distributed under the License is distributed on an "AS IS" BASIS,
  #  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  #  * See the License for the specific language governing permissions and
  #  * limitations under the License.
  # 
  LIBRG_VERSION_MAJOR* = 5
  LIBRG_VERSION_MINOR* = 0
  LIBRG_VERSION_PATCH* = 5

  #  include definitions
  # 
  #   ZPL - Global module
  # Usage:
  #   #define ZPL_IMPLEMENTATION exactly in ONE source file right BEFORE including the library, like:
  #   #define ZPL_IMPLEMENTATION
  #   #include "zpl.h"
  #   To make use of platform layer, define ZPL_PLATFORM, like:
  #   #define ZPL_PLATFORM
  #   #include "zpl.h"
  #   To make use of OpenGL methods, define ZPL_OPENGL, like:
  #   #define ZPL_OPENGL
  #   #include "zpl.h"
  #   Note that ZPL_OPENGL requires the presence of:
  #     - zpl_glgen.h (generated by zpl_glgen.py)
  #     - stb_image.h (for texture support)
  #     if font rendering is enabled, you also need:
  #       - stb_rect_pack.h
  #       - stb_truetype.h
  #       unless ZPLGL_NO_FONTS is defined
  # Options:
  #   ZPL_PREFIX_TYPES - to make sure all ZPL defined types have a prefix to avoid cluttering the global namespace.
  #   ZPL_DEFINE_NULL_MACRO - to let ZPL define what NULL stands for in case it is undefined.
  #   ZPL_PLATFORM - enables platform layer module.
  #   ZPL_OPENGL - enables OpenGL module.
  #     ZPLGL_NO_FONTS - disables font rendering support for OpenGL module.
  #     ZPLGL_NO_BASIC_STATE - disables basic state API (contains useful 2D rendering methods as well as font rendering)
  # Credits:
  #   Read AUTHORS.md
  # GitHub:
  #   https:github.com/zpl-c/zpl
  # Version History:
  #   9.8.0 - Incorporated OpenGL into ZPL core as an optional module
  #   9.7.0 - Added co-routine module
  #   9.6.0 - Added process module for creation and manipulation
  #   9.5.2 - zpl_printf family now prints (null) on NULL string arguments
  #   9.5.1 - Fixed JSON5 real number export support + indentation fixes
  #   9.5.0 - Added base64 encode/decode methods
  #   9.4.10- Small enum style changes
  #   9.4.8 - Fix quote-less JSON node name resolution
  #   9.4.7 - Additional change to the code
  #   9.4.6 - Fix issue where zpl_json_find would have false match on substrings
  #   9.4.5 - Mistakes were made, fixed compilation errors
  #   9.4.3 - Fix old API shenanigans
  #   9.4.2 - Fix small API typos
  #   9.4.1 - Reordered JSON5 constants to integrate better with conditions
  #   9.4.0 - JSON5 API changes made to zpl_json_find
  #   9.3.0 - Change how zpl uses basic types internally
  #   9.2.0 - Directory listing was added. Check dirlist_api.c test for more info
  #   9.1.1 - Fix WIN32_LEAN_AND_MEAN redefinition properly
  #   9.1.0 - get_env rework and fixes
  #   9.0.3 - Small fixes and removals
  #   9.0.0 - New documentation format, removed deprecated code, changed styles
  #   8.14.1 - Fix string library
  #   8.14.0 - Added zpl_re_match_all
  #   8.13.0 - Update system command API
  #   8.12.6 - Fix warning in CLI options parser
  #   8.12.5 - Support parametric options preceding positionals
  #   8.12.4 - Fixed opts positionals ordering
  #   8.12.3 - Fixed incorrect handling of flags preceding positionals
  #   8.12.2 - JSON parsing remark added
  #   8.12.1 - Fixed a lot of important stuff
  #   8.12.0 - Added helper constructors for containers
  #   8.11.2 - Fix bug in opts module
  #   8.11.1 - Small code improvements
  #   8.11.0 - Ported regex processor from https:github.com/gingerBill/gb/ and applied fixes on top of it
  #   8.10.2 - Fix zpl_strtok
  #   8.10.1 - Replace zpl_strchr by zpl_char_last_occurence
  #   8.10.0 - Added zpl_strchr
  #   8.9.0  - API improvements for JSON5 parser
  #   8.8.4  - Add support for SJSON formatting http:bitsquid.blogspot.com/2009/10/simplified-json-notation.html
  #   6.8.3  - JSON5 exp fix
  #   6.8.2  - Bugfixes applied from gb
  #   6.8.1  - Performance improvements for JSON5 parser
  #   6.8.0  - zpl.h is now generated by build.py
  #   6.7.0  - Several fixes and added switches
  #   6.6.0  - Several significant changes made to the repository
  #   6.5.0  - Ported platform layer from https:github.com/gingerBill/gb/
  #   6.4.1  - Use zpl_strlen in zpl_strdup
  #   6.4.0  - Deprecated zpl_buffer_free and added zpl_array_end, zpl_buffer_end
  #   6.3.0  - Added zpl_strdup
  #   6.2.1  - Remove math redundancies
  #   6.2.0  - Integrated zpl_math.h into zpl.h
  #   6.1.1  - Added direct.h include for win c++ dir methods
  #   6.1.0  - Added zpl_path_mkdir, zpl_path_rmdir, and few new zplFileErrors
  #   6.0.4  - More MSVC(++) satisfaction by fixing warnings
  #   6.0.3  - Satisfy MSVC by fixing a warning
  #   6.0.2  - Fixed warnings for json5 i64 printfs
  #   6.0.1  - Fixed warnings for particual win compiler in dirlist method
  #   6.0.0  - New build, include/ was renamed to code/
  #   5.8.3  - Naming fixes
  #   5.8.2  - Job system now supports prioritized tasks
  #   5.8.1  - Renames zpl_pad to zpl_ring
  #   5.8.0  - Added instantiated scratch pad (circular buffer)
  #   5.7.2  - Added Windows support for zpl_path_dirlist
  #   5.7.1  - Fixed few things in job system + macOS support for zpl_path_dirlist
  #   5.7.0  - Added a job system (zpl_thread_pool)
  #   5.6.5  - Fixes extra error cases for zpl_opts when input is:
  #          - missing a value for an option,
  #          - having an extra value for a flag (e.g. --enable-log shouldn't get a value.)
  #   5.6.4  - Several tweaks to the zpl_opts API
  #   5.6.3  - Added support for flags without values
  #   5.6.2  - Improve error handling for zpl_opts
  #   5.6.1  - Added support for strings with spaces in zpl_opts
  #   5.6.0  - Added zpl_opts for CLI argument parsing
  #   5.5.1  - Fixed time method for win
  #   5.5.0  - Integrate JSON5 writer into the core
  #   5.4.0  - Improved storage support for numbers in JSON5 parser
  #   5.3.0  - Integrated zpl_json into ZPL
  #   5.2.0  - Added zpl_string_sprintf
  #   5.1.1  - Added zpl_system_command_nores for output-less execution
  #   5.1.0  - Added event handler
  #   5.0.4  - Fix alias for zpl_list
  #   5.0.3  - Finalizing syntax changes
  #   5.0.2  - Fix segfault when using zpl_stack_memory
  #   5.0.1  - Small code improvements
  #   5.0.0  - Project structure changes
  #   4.7.2  - Got rid of size arg for zpl_str_split_lines
  #   4.7.1  - Added an example
  #   4.7.0  - Added zpl_path_dirlist
  #   4.6.1  - zpl_memcopy x86 patch from upstream
  #   4.6.0  - Added few string-related functions
  #   4.5.9  - Error fixes
  #   4.5.8  - Warning fixes
  #   4.5.7  - Fixed timer loops. zpl_time* related functions work with seconds now
  #   4.5.6  - Fixed zpl_time_now() for Windows and Linux
  #   4.5.5  - Small cosmetic changes
  #   4.5.4  - Fixed issue when zpl_list_add would break the links
  #          - when adding a new item between nodes
  #   4.5.3  - Fixed malformed enum values
  #   4.5.1  - Fixed some warnings
  #   4.5.0  - Added zpl_array_append_at
  #   4.4.0  - Added zpl_array_back, zpl_array_front
  #   4.3.0  - Added zpl_list
  #   4.2.0  - Added zpl_system_command_str
  #   4.1.2  - GG, fixed small compilation error
  #   4.1.1  - Fixed possible security issue in zpl_system_command
  #   4.1.0  - Added zpl_string_make_reserve and small fixes
  #   4.0.2  - Warning fix for _LARGEFILE64_SOURCE
  #   4.0.1  - include stdlib.h for getenv (temp)
  #   4.0.0  - ARM support, coding style changes and various improvements
  #   3.4.1  - zpl_memcopy now uses memcpy for ARM arch-family
  #   3.4.0  - Removed obsolete code
  #   3.3.4  - Added Travis CI config
  #   3.3.3  - Small macro formatting changes + ZPL_SYSTEM_IOS
  #   3.3.2  - Fixes for android arm
  #   3.3.1  - Fixed some type cast warnings
  #   3.3.0  - Added Android support
  #   3.1.5  - Renamed userptr to user_data in timer
  #   3.1.4  - Fix for zpl_buffer not allocating correctly
  #   3.1.2  - Small fix in zpl_memcompare
  #   3.1.1  - Added char* conversion for data field in zpl_array_header
  #   3.1.0  - Added data field to zpl_array_header
  #   3.0.7  - Added timer userptr as argument to callback
  #   3.0.6  - Small changes
  #   3.0.5  - Fixed compilation for emscripten
  #   3.0.4  - Small fixes for tiny cpp warnings
  #   3.0.3  - Small fixes for various cpp warnings and errors
  #   3.0.2  - Fixed linux part, and removed trailing spaces
  #   3.0.1  - Small bugfix in zpl_file_open
  #   3.0.0  - Added several fixes and features
  #   2.4.0  - Added remove to hash table
  #   2.3.3  - Removed redundant code
  #   2.3.2  - Eliminated extra warnings
  #   2.3.1  - Warning hunt
  #   2.3.0  - Added the ability to copy array/buffer and fixed bug in hash table.
  #   2.2.1  - Used tmpfile() for Windows
  #   2.2.0  - Added zpl_file_temp
  #   2.1.1  - Very small fix (forgive me)
  #   2.1.0  - Added the ability to resize bitstream
  #   2.0.8  - Small adjustments
  #   2.0.7  - MinGW related fixes
  #   2.0.0  - New NPM based version
  #   1.2.2  - Small fix
  #   1.2.1  - Macro fixes
  #   1.2.0  - Added zpl_async macro
  #   1.1.0  - Added timer feature
  #   1.0.0  - Initial version
  #   This Software is dual licensed under the following licenses:
  #   Unlicense
  #   This is free and unencumbered software released into the public domain.
  #   Anyone is free to copy, modify, publish, use, compile, sell, or
  #   distribute this software, either in source code form or as a compiled
  #   binary, for any purpose, commercial or non-commercial, and by any
  #   means.
  #   In jurisdictions that recognize copyright laws, the author or authors
  #   of this software dedicate any and all copyright interest in the
  #   software to the public domain. We make this dedication for the benefit
  #   of the public at large and to the detriment of our heirs and
  #   successors. We intend this dedication to be an overt act of
  #   relinquishment in perpetuity of all present and future rights to this
  #   software under copyright law.
  #   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  #   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  #   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  #   IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  #   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  #   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  #   OTHER DEALINGS IN THE SOFTWARE.
  #   For more information, please refer to <http:unlicense.org/>
  #   Apache 2.0
  #   Copyright 2017-2019 Dominik Madar├ísz <zaklaus@outlook.com>
  #   Licensed under the Apache License, Version 2.0 (the "License");
  #   you may not use this file except in compliance with the License.
  #   You may obtain a copy of the License at
  #       http:www.apache.org/licenses/LICENSE-2.0
  #   Unless required by applicable law or agreed to in writing, software
  #   distributed under the License is distributed on an "AS IS" BASIS,
  #   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  #   See the License for the specific language governing permissions and
  #   limitations under the License.
  # 
  # 
  #  TOP
  # 
  # 
  #  This file was generated by build.py script. Do not modify it!
  #  Instead, visit a specific module and edit it, then execute build.py to generate this file.
  # 
  # * @file header.c
  # @brief Macro helpers and symbols
  # @defgroup header Macro helpers and symbols
  #  This module contains many useful macros helpful for debugging as well as development itself.
  #  @{
  # 
  ZPL_ARCH_64_BIT* = 1
  ZPL_SYSTEM_WINDOWS* = 1
  ZPL_COMPILER_GCC* = 1
  ZPL_CPU_X86* = 1
  ZPL_CACHE_LINE_SIZE* = 64
  ZPL_THREADING* = 1

  # 
  # 
  #  Headers
  # 
  # 
  #  TODO(ZaKlaus): Find a better way to get this flag in MinGW.
  WC_ERR_INVALID_CHARS* = 0x0080
  ZPL_I8_MAX* = 0x7f
  ZPL_I16_MAX* = 0x7fff
  ZPL_I32_MAX* = 0x7fffffff
  ZPL_ALLOCATION_ALLOC* = 0.zplAllocationType
  ZPL_ALLOCATION_FREE* = 1.zplAllocationType
  ZPL_ALLOCATION_FREE_ALL* = 2.zplAllocationType
  ZPL_ALLOCATION_RESIZE* = 3.zplAllocationType
  ZPL_ALLOCATOR_FLAG_CLEAR_TO_ZERO* = ((1  shl  (0))).zplAllocatorFlag

  #  TODO: Fixed heap allocator
  #  TODO: General heap allocator. Maybe a TCMalloc like clone?
  # ! @}
  # * @file threads.c
  # @brief Threading methods, blocking models...
  # @defgroup threads Threading management
  # This module features common threading and blocking principles. It contains thread merge operation based on stb_sync, as well as CPU affinity management.
  # @{
  # 
  #  Atomics
  #  TODO: Be specific with memory order?
  #  e.g. relaxed, acquire, release, acquire_release
  ZPL_ATOMIC_PTR_ALIGNMENT* = 8
  ZPL_RE_ERROR_NONE* = 0.zpl_regex_error
  ZPL_RE_ERROR_NO_MATCH* = 1.zpl_regex_error
  ZPL_RE_ERROR_TOO_LONG* = 2.zpl_regex_error
  ZPL_RE_ERROR_MISMATCHED_CAPTURES* = 3.zpl_regex_error
  ZPL_RE_ERROR_MISMATCHED_BLOCKS* = 4.zpl_regex_error
  ZPL_RE_ERROR_BRANCH_FAILURE* = 5.zpl_regex_error
  ZPL_RE_ERROR_INVALID_QUANTIFIER* = 6.zpl_regex_error
  ZPL_RE_ERROR_INTERNAL_FAILURE* = 7.zpl_regex_error
  ZPL_FILE_MODE_READ* = ((1  shl  (0))).zpl_file_mode_flag
  ZPL_FILE_MODE_WRITE* = ((1  shl  (1))).zpl_file_mode_flag
  ZPL_FILE_MODE_APPEND* = ((1  shl  (2))).zpl_file_mode_flag
  ZPL_FILE_MODE_RW* = ((1  shl  (3))).zpl_file_mode_flag
  zpl_file_mode_modes_ev* = (ZPL_FILE_MODE_READ  or  ZPL_FILE_MODE_WRITE  or  ZPL_FILE_MODE_APPEND  or  ZPL_FILE_MODE_RW).zpl_file_mode_flag
  ZPL_SEEK_WHENCE_BEGIN* = (0).zpl_seek_whence_type
  ZPL_SEEK_WHENCE_CURRENT* = (1).zpl_seek_whence_type
  ZPL_SEEK_WHENCE_END* = (2).zpl_seek_whence_type
  ZPL_FILE_ERROR_NONE* = 0.zpl_file_error
  ZPL_FILE_ERROR_INVALID* = 1.zpl_file_error
  ZPL_FILE_ERROR_INVALID_FILENAME* = 2.zpl_file_error
  ZPL_FILE_ERROR_EXISTS* = 3.zpl_file_error
  ZPL_FILE_ERROR_NOT_EXISTS* = 4.zpl_file_error
  ZPL_FILE_ERROR_PERMISSION* = 5.zpl_file_error
  ZPL_FILE_ERROR_TRUNCATION_FAILURE* = 6.zpl_file_error
  ZPL_FILE_ERROR_NOT_EMPTY* = 7.zpl_file_error
  ZPL_FILE_ERROR_NAME_TOO_LONG* = 8.zpl_file_error
  ZPL_FILE_ERROR_UNKNOWN* = 9.zpl_file_error
  ZPL_DIR_TYPE_FILE* = 0.zpl_dir_type
  ZPL_DIR_TYPE_FOLDER* = 1.zpl_dir_type
  ZPL_DIR_TYPE_UNKNOWN* = 2.zpl_dir_type
  ZPL_FILE_STANDARD_INPUT* = 0.zpl_file_standard_type
  ZPL_FILE_STANDARD_OUTPUT* = 1.zpl_file_standard_type
  ZPL_FILE_STANDARD_ERROR* = 2.zpl_file_standard_type
  ZPL_FILE_STANDARD_COUNT* = 3.zpl_file_standard_type
  ZPL_JSON_TYPE_OBJECT* = 0.zpl_json_type
  ZPL_JSON_TYPE_STRING* = 1.zpl_json_type
  ZPL_JSON_TYPE_MULTISTRING* = 2.zpl_json_type
  ZPL_JSON_TYPE_ARRAY* = 3.zpl_json_type
  ZPL_JSON_TYPE_INTEGER* = 4.zpl_json_type
  ZPL_JSON_TYPE_REAL* = 5.zpl_json_type
  ZPL_JSON_TYPE_CONSTANT* = 6.zpl_json_type
  ZPL_JSON_PROPS_NONE* = (0).zpl_json_props
  ZPL_JSON_PROPS_NAN* = (1).zpl_json_props
  ZPL_JSON_PROPS_NAN_NEG* = (2).zpl_json_props
  ZPL_JSON_PROPS_INFINITY* = (3).zpl_json_props
  ZPL_JSON_PROPS_INFINITY_NEG* = (4).zpl_json_props
  ZPL_JSON_PROPS_IS_EXP* = (5).zpl_json_props
  ZPL_JSON_PROPS_IS_HEX* = (6).zpl_json_props
  ZPL_JSON_PROPS_IS_PARSED_REAL* = (7).zpl_json_props
  ZPL_JSON_CONST_FALSE* = 0.zpl_json_const
  ZPL_JSON_CONST_TRUE* = 1.zpl_json_const
  ZPL_JSON_CONST_NULL* = 2.zpl_json_const
  ZPL_JSON_ERROR_NONE* = 0.zpl_json_error
  ZPL_JSON_ERROR_INVALID_NAME* = 1.zpl_json_error
  ZPL_JSON_ERROR_INVALID_VALUE* = 2.zpl_json_error
  ZPL_JSON_ERROR_OBJECT_OR_SOURCE_WAS_NULL* = 3.zpl_json_error
  ZPL_JSON_NAME_STYLE_DOUBLE_QUOTE* = 0.zpl_json_naming_style
  ZPL_JSON_NAME_STYLE_SINGLE_QUOTE* = 1.zpl_json_naming_style
  ZPL_JSON_NAME_STYLE_NO_QUOTES* = 2.zpl_json_naming_style
  ZPL_JSON_ASSIGN_STYLE_COLON* = 0.zpl_json_assign_style
  ZPL_JSON_ASSIGN_STYLE_EQUALS* = 1.zpl_json_assign_style
  ZPL_JSON_ASSIGN_STYLE_LINE* = 2.zpl_json_assign_style
  ZPL_JSON_DELIM_STYLE_COMMA* = 0.zpl_json_delim_style
  ZPL_JSON_DELIM_STYLE_LINE* = 1.zpl_json_delim_style
  ZPL_JSON_DELIM_STYLE_NEWLINE* = 2.zpl_json_delim_style
  # Const 'zpl_json_object' skipped
  ZPL_OPTS_STRING* = 0.zpl_opts_types
  ZPL_OPTS_FLOAT* = 1.zpl_opts_types
  ZPL_OPTS_FLAG* = 2.zpl_opts_types
  ZPL_OPTS_INT* = 3.zpl_opts_types
  ZPL_OPTS_ERR_VALUE* = 0.zpl_opts_err_type
  ZPL_OPTS_ERR_OPTION* = 1.zpl_opts_err_type
  ZPL_OPTS_ERR_EXTRA_VALUE* = 2.zpl_opts_err_type
  ZPL_OPTS_ERR_MISSING_VALUE* = 3.zpl_opts_err_type
  ZPL_PR_OPTS_COMBINE_STD_OUTPUT* = ((1  shl  (1))).zpl_pr_opts
  ZPL_PR_OPTS_INHERIT_ENV* = ((1  shl  (2))).zpl_pr_opts
  ZPL_PR_OPTS_CUSTOM_ENV* = ((1  shl  (3))).zpl_pr_opts
  ZPL_JOBS_STATUS_READY* = 0.zpl_jobs_status
  ZPL_JOBS_STATUS_BUSY* = 1.zpl_jobs_status
  ZPL_JOBS_STATUS_WAITING* = 2.zpl_jobs_status
  ZPL_JOBS_STATUS_TERM* = 3.zpl_jobs_status

  # ! @}
  # * @file coroutines.c
  # @brief Coroutines module
  # @defgroup misc Coroutines module
  #  This module implements co-routines feature for C99.
  #  @{
  # 
  # 
  #    See test/coroutines.c for an example usage
  # 
  ZPL_CO_ARG_STACK_CAPACITY* = 128
  ZPL_CO_READY* = 0.zpl_co_status
  ZPL_CO_ENQUEUED* = 1.zpl_co_status
  ZPL_CO_RUNNING* = 2.zpl_co_status
  ZPL_CO_WAITING* = 3.zpl_co_status
  ZPL_CO_DEAD* = 4.zpl_co_status

  # /
  # 
  #  Code Snippets
  # 
  # 
  # #ifndef FOO_H
  # #define FOO_H
  # #ifdef __cplusplus
  # extern "C" {
  # #endif
  # #ifdef __cplusplus
  # }
  # #endif
  # #if defined(FOO_IMPLEMENTATION) && !defined(FOO_IMPLEMENTATION_DONE)
  # #define FOO_IMPLEMENTATION_DONE
  # #ifdef __cplusplus
  # extern "C" {
  # #endif
  # #ifdef __cplusplus
  # }
  # #endif
  # #endif  FOO_IMPLEMENTATION
  # #endif  FOO_H
  # 
  # *
  #  * include/enet.h - a Single-Header auto-generated variant of enet.h library.
  #  *
  #  * Usage:
  #  * #define ENET_IMPLEMENTATION exactly in ONE source file right BEFORE including the library, like:
  #  *
  #  * #define ENET_IMPLEMENTATION
  #  * #include <enet.h>
  #  *
  #  * License:
  #  * The MIT License (MIT)
  #  *
  #  * Copyright (c) 2002-2016 Lee Salzman
  #  * Copyright (c) 2017-2018 Vladyslav Hrytsenko, Dominik Madar├ísz
  #  *
  #  * Permission is hereby granted, free of charge, to any person obtaining a copy
  #  * of this software and associated documentation files (the "Software"), to deal
  #  * in the Software without restriction, including without limitation the rights
  #  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  #  * copies of the Software, and to permit persons to whom the Software is
  #  * furnished to do so, subject to the following conditions:
  #  *
  #  * The above copyright notice and this permission notice shall be included in all
  #  * copies or substantial portions of the Software.
  #  *
  #  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  #  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  #  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  #  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  #  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  #  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  #  * SOFTWARE.
  #  *
  # 
  ENET_VERSION_MAJOR* = 2
  ENET_VERSION_MINOR* = 1
  ENET_VERSION_PATCH* = 3
  ENET_TIME_OVERFLOW* = 86400000
  ENET_BUFFER_MAXIMUM = 65
  ENET_IPV6* = 1
  ENET_PORT_ANY* = 0
  ENET_PROTOCOL_MINIMUM_MTU* = (576).Enum_librgh1
  ENET_PROTOCOL_MAXIMUM_MTU* = (4096).Enum_librgh1
  ENET_PROTOCOL_MAXIMUM_PACKET_COMMANDS* = (32).Enum_librgh1
  ENET_PROTOCOL_MINIMUM_WINDOW_SIZE* = (4096).Enum_librgh1
  ENET_PROTOCOL_MAXIMUM_WINDOW_SIZE* = (65536).Enum_librgh1
  ENET_PROTOCOL_MINIMUM_CHANNEL_COUNT* = (1).Enum_librgh1
  ENET_PROTOCOL_MAXIMUM_CHANNEL_COUNT* = (255).Enum_librgh1
  ENET_PROTOCOL_MAXIMUM_PEER_ID* = (0xFFF).Enum_librgh1
  ENET_PROTOCOL_MAXIMUM_FRAGMENT_COUNT* = (1024 * 1024).Enum_librgh1
  ENET_PROTOCOL_COMMAND_NONE* = (0).ENetProtocolCommand
  ENET_PROTOCOL_COMMAND_ACKNOWLEDGE* = (1).ENetProtocolCommand
  ENET_PROTOCOL_COMMAND_CONNECT* = (2).ENetProtocolCommand
  ENET_PROTOCOL_COMMAND_VERIFY_CONNECT* = (3).ENetProtocolCommand
  ENET_PROTOCOL_COMMAND_DISCONNECT* = (4).ENetProtocolCommand
  ENET_PROTOCOL_COMMAND_PING* = (5).ENetProtocolCommand
  ENET_PROTOCOL_COMMAND_SEND_RELIABLE* = (6).ENetProtocolCommand
  ENET_PROTOCOL_COMMAND_SEND_UNRELIABLE* = (7).ENetProtocolCommand
  ENET_PROTOCOL_COMMAND_SEND_FRAGMENT* = (8).ENetProtocolCommand
  ENET_PROTOCOL_COMMAND_SEND_UNSEQUENCED* = (9).ENetProtocolCommand
  ENET_PROTOCOL_COMMAND_BANDWIDTH_LIMIT* = (10).ENetProtocolCommand
  ENET_PROTOCOL_COMMAND_THROTTLE_CONFIGURE* = (11).ENetProtocolCommand
  ENET_PROTOCOL_COMMAND_SEND_UNRELIABLE_FRAGMENT* = (12).ENetProtocolCommand
  ENET_PROTOCOL_COMMAND_COUNT* = (13).ENetProtocolCommand
  ENET_PROTOCOL_COMMAND_MASK* = (0x0F).ENetProtocolCommand
  ENET_PROTOCOL_COMMAND_FLAG_ACKNOWLEDGE* = ((1  shl  7)).ENetProtocolFlag
  ENET_PROTOCOL_COMMAND_FLAG_UNSEQUENCED* = ((1  shl  6)).ENetProtocolFlag
  ENET_PROTOCOL_HEADER_FLAG_COMPRESSED* = ((1  shl  14)).ENetProtocolFlag
  ENET_PROTOCOL_HEADER_FLAG_SENT_TIME* = ((1  shl  15)).ENetProtocolFlag
  ENET_PROTOCOL_HEADER_FLAG_MASK* = (ENET_PROTOCOL_HEADER_FLAG_COMPRESSED  or  ENET_PROTOCOL_HEADER_FLAG_SENT_TIME).ENetProtocolFlag
  ENET_PROTOCOL_HEADER_SESSION_MASK* = ((3  shl  12)).ENetProtocolFlag
  ENET_PROTOCOL_HEADER_SESSION_SHIFT* = (12).ENetProtocolFlag
  ENET_SOCKET_TYPE_STREAM* = (1).ENetSocketType
  ENET_SOCKET_TYPE_DATAGRAM* = (2).ENetSocketType
  ENET_SOCKET_WAIT_NONE* = (0).ENetSocketWait
  ENET_SOCKET_WAIT_SEND* = ((1  shl  0)).ENetSocketWait
  ENET_SOCKET_WAIT_RECEIVE* = ((1  shl  1)).ENetSocketWait
  ENET_SOCKET_WAIT_INTERRUPT* = ((1  shl  2)).ENetSocketWait
  ENET_SOCKOPT_NONBLOCK* = (1).ENetSocketOption
  ENET_SOCKOPT_BROADCAST* = (2).ENetSocketOption
  ENET_SOCKOPT_RCVBUF* = (3).ENetSocketOption
  ENET_SOCKOPT_SNDBUF* = (4).ENetSocketOption
  ENET_SOCKOPT_REUSEADDR* = (5).ENetSocketOption
  ENET_SOCKOPT_RCVTIMEO* = (6).ENetSocketOption
  ENET_SOCKOPT_SNDTIMEO* = (7).ENetSocketOption
  ENET_SOCKOPT_ERROR* = (8).ENetSocketOption
  ENET_SOCKOPT_NODELAY* = (9).ENetSocketOption
  ENET_SOCKOPT_IPV6_V6ONLY* = (10).ENetSocketOption
  ENET_SOCKET_SHUTDOWN_READ* = (0).ENetSocketShutdown
  ENET_SOCKET_SHUTDOWN_WRITE* = (1).ENetSocketShutdown
  ENET_SOCKET_SHUTDOWN_READ_WRITE* = (2).ENetSocketShutdown
  ENET_PACKET_FLAG_RELIABLE* = ((1  shl  0)).ENetPacketFlag
  ENET_PACKET_FLAG_UNSEQUENCED* = ((1  shl  1)).ENetPacketFlag
  ENET_PACKET_FLAG_NO_ALLOCATE* = ((1  shl  2)).ENetPacketFlag
  ENET_PACKET_FLAG_UNRELIABLE_FRAGMENT* = ((1  shl  3)).ENetPacketFlag
  ENET_PACKET_FLAG_SENT* = ((1  shl  8)).ENetPacketFlag
  ENET_PEER_STATE_DISCONNECTED* = (0).ENetPeerState
  ENET_PEER_STATE_CONNECTING* = (1).ENetPeerState
  ENET_PEER_STATE_ACKNOWLEDGING_CONNECT* = (2).ENetPeerState
  ENET_PEER_STATE_CONNECTION_PENDING* = (3).ENetPeerState
  ENET_PEER_STATE_CONNECTION_SUCCEEDED* = (4).ENetPeerState
  ENET_PEER_STATE_CONNECTED* = (5).ENetPeerState
  ENET_PEER_STATE_DISCONNECT_LATER* = (6).ENetPeerState
  ENET_PEER_STATE_DISCONNECTING* = (7).ENetPeerState
  ENET_PEER_STATE_ACKNOWLEDGING_DISCONNECT* = (8).ENetPeerState
  ENET_PEER_STATE_ZOMBIE* = (9).ENetPeerState
  ENET_HOST_RECEIVE_BUFFER_SIZE* = (256 * 1024).Enum_librgh2
  ENET_HOST_SEND_BUFFER_SIZE* = (256 * 1024).Enum_librgh2
  ENET_HOST_BANDWIDTH_THROTTLE_INTERVAL* = (1000).Enum_librgh2
  ENET_HOST_DEFAULT_MTU* = (1400).Enum_librgh2
  ENET_HOST_DEFAULT_MAXIMUM_PACKET_SIZE* = (32 * 1024 * 1024).Enum_librgh2
  ENET_HOST_DEFAULT_MAXIMUM_WAITING_DATA* = (32 * 1024 * 1024).Enum_librgh2
  ENET_PEER_DEFAULT_ROUND_TRIP_TIME* = (500).Enum_librgh2
  ENET_PEER_DEFAULT_PACKET_THROTTLE* = (32).Enum_librgh2
  ENET_PEER_PACKET_THROTTLE_SCALE* = (32).Enum_librgh2
  ENET_PEER_PACKET_THROTTLE_COUNTER* = (7).Enum_librgh2
  ENET_PEER_PACKET_THROTTLE_ACCELERATION* = (2).Enum_librgh2
  ENET_PEER_PACKET_THROTTLE_DECELERATION* = (2).Enum_librgh2
  ENET_PEER_PACKET_THROTTLE_INTERVAL* = (5000).Enum_librgh2
  ENET_PEER_PACKET_LOSS_SCALE* = ((1  shl  16)).Enum_librgh2
  ENET_PEER_PACKET_LOSS_INTERVAL* = (10000).Enum_librgh2
  ENET_PEER_WINDOW_SIZE_SCALE* = (64 * 1024).Enum_librgh2
  ENET_PEER_TIMEOUT_LIMIT* = (32).Enum_librgh2
  ENET_PEER_TIMEOUT_MINIMUM* = (5000).Enum_librgh2
  ENET_PEER_TIMEOUT_MAXIMUM* = (30000).Enum_librgh2
  ENET_PEER_PING_INTERVAL* = (500).Enum_librgh2
  ENET_PEER_UNSEQUENCED_WINDOWS* = (64).Enum_librgh2
  ENET_PEER_UNSEQUENCED_WINDOW_SIZE* = (1024).Enum_librgh2
  ENET_PEER_FREE_UNSEQUENCED_WINDOWS* = (32).Enum_librgh2
  ENET_PEER_RELIABLE_WINDOWS* = (16).Enum_librgh2
  ENET_PEER_RELIABLE_WINDOW_SIZE* = (0x1000).Enum_librgh2
  ENET_PEER_FREE_RELIABLE_WINDOWS* = (8).Enum_librgh2
  ENET_EVENT_TYPE_NONE* = (0).ENetEventType
  ENET_EVENT_TYPE_CONNECT* = (1).ENetEventType
  ENET_EVENT_TYPE_DISCONNECT* = (2).ENetEventType
  ENET_EVENT_TYPE_RECEIVE* = (3).ENetEventType
  ENET_EVENT_TYPE_DISCONNECT_TIMEOUT* = (4).ENetEventType

  #  library mode (stastic or dynamic)
  #  default constants/methods used
  #  keeping enabled by default depecrecated
  #  naming support till next major release
  #  =======================================================================
  #  !
  #  ! Library compile-time configure features
  #  !
  #  =======================================================================
  LIBRG_FEATURE_VIRTUAL_WORLDS* = 1
  LIBRG_FEATURE_ENTITY_VISIBILITY* = 1
  LIBRG_FEATURE_OCTREE_CULLER* = 1
  LIBRG_MODE_SERVER* = 0.librg_mode
  LIBRG_MODE_CLIENT* = 1.librg_mode
  LIBRG_SPACE_2D* = (2).librg_space_type
  LIBRG_SPACE_3D* = (3).librg_space_type
  LIBRG_POINTER_CTX* = 0.librg_pointer_type
  LIBRG_POINTER_DATA* = 1.librg_pointer_type
  LIBRG_POINTER_EVENT* = 2.librg_pointer_type
  LIBRG_THREAD_IDLE* = 0.librg_thread_state
  LIBRG_THREAD_WORK* = 1.librg_thread_state
  LIBRG_THREAD_EXIT* = 2.librg_thread_state
  LIBRG_ENTITY_NONE* = (0).librg_entity_flag
  LIBRG_ENTITY_ALIVE* = ((1  shl  0)).librg_entity_flag
  LIBRG_ENTITY_CLIENT* = ((1  shl  1)).librg_entity_flag
  LIBRG_ENTITY_VISIBILITY* = ((1  shl  2)).librg_entity_flag
  LIBRG_ENTITY_QUERIED* = ((1  shl  3)).librg_entity_flag
  LIBRG_ENTITY_CONTROLLED* = ((1  shl  4)).librg_entity_flag
  LIBRG_ENTITY_CONTROL_REQUESTED* = ((1  shl  5)).librg_entity_flag
  LIBRG_ENTITY_MARKED_REMOVAL* = ((1  shl  6)).librg_entity_flag
  LIBRG_ENTITY_UNUSED* = ((1  shl  7)).librg_entity_flag
  LIBRG_ENTITY_FLAG_LAST* = ((1  shl  8)).librg_entity_flag
  LIBRG_DEFAULT_VISIBILITY* = 0.librg_visibility
  LIBRG_ALWAYS_VISIBLE* = 1.librg_visibility
  LIBRG_ALWAYS_INVISIBLE* = 2.librg_visibility
  LIBRG_CONNECTION_INIT* = 0.librg_events
  LIBRG_CONNECTION_REQUEST* = 1.librg_events
  LIBRG_CONNECTION_REFUSE* = 2.librg_events
  LIBRG_CONNECTION_ACCEPT* = 3.librg_events
  LIBRG_CONNECTION_DISCONNECT* = 4.librg_events
  LIBRG_CONNECTION_TIMEOUT* = 5.librg_events
  LIBRG_CONNECTION_TIMESYNC* = 6.librg_events
  LIBRG_ENTITY_CREATE* = 7.librg_events
  LIBRG_ENTITY_UPDATE* = 8.librg_events
  LIBRG_ENTITY_REMOVE* = 9.librg_events
  LIBRG_CLIENT_STREAMER_ADD* = 10.librg_events
  LIBRG_CLIENT_STREAMER_REMOVE* = 11.librg_events
  LIBRG_CLIENT_STREAMER_UPDATE* = 12.librg_events
  LIBRG_EVENT_LAST* = 13.librg_events
  LIBRG_EVENT_NONE* = (0).librg_event_flags
  LIBRG_EVENT_REJECTED* = ((1  shl  0)).librg_event_flags
  LIBRG_EVENT_REJECTABLE* = ((1  shl  1)).librg_event_flags
  LIBRG_EVENT_REMOTE* = ((1  shl  2)).librg_event_flags
  LIBRG_EVENT_LOCAL* = ((1  shl  3)).librg_event_flags
  LIBRG_PLATFORM_ID* = 0.librg_options
  LIBRG_PLATFORM_PROTOCOL* = 1.librg_options
  LIBRG_PLATFORM_BUILD* = 2.librg_options
  LIBRG_DEFAULT_CLIENT_TYPE* = 3.librg_options
  LIBRG_DEFAULT_STREAM_RANGE* = 4.librg_options
  LIBRG_DEFAULT_DATA_SIZE* = 5.librg_options
  LIBRG_NETWORK_CAPACITY* = 6.librg_options
  LIBRG_NETWORK_CHANNELS* = 7.librg_options
  LIBRG_NETWORK_PRIMARY_CHANNEL* = 8.librg_options
  LIBRG_NETWORK_SECONDARY_CHANNEL* = 9.librg_options
  LIBRG_NETWORK_MESSAGE_CHANNEL* = 10.librg_options
  LIBRG_NETWORK_BUFFER_SIZE* = 11.librg_options
  LIBRG_MAX_ENTITIES_PER_BRANCH* = 12.librg_options
  LIBRG_USE_RADIUS_CULLING* = 13.librg_options
  LIBRG_OPTIONS_SIZE* = 14.librg_options
  LIBRG_TIMESYNC_SIZE* = 5
  LIBRG_DATA_STREAMS_AMOUNT* = 4

{.pragma: implibrg, importc.}
{.pragma: implibrgC, implibrg, cdecl, dynlib: dynlibFile.}

type
  zpl_vec3_mat3* = object  
    x*: zpl_vec3  
    y*: zpl_vec3  
    z*: zpl_vec3  

  zpl_xy_zw_vec4* = object  
    xy*: zpl_vec2  
    zw*: zpl_vec2  

  zpl_json_object_real* = object  
    real*: zpl_f64  
    base*: zpl_i32  
    base2*: zpl_i32  
    exp*: zpl_i32  
    exp_neg* {.bitsize: 1.}: zpl_u8  
    lead_digit* {.bitsize: 1.}: zpl_u8  

  zpl_rgba* = object  
    r*: zpl_f32  
    g*: zpl_f32  
    b*: zpl_f32  
    a*: zpl_f32  

  zpl_xyzw* = object  
    x*: zpl_f32  
    y*: zpl_f32  
    z*: zpl_f32  
    w*: zpl_f32  

  zpl_rgb* = object  
    r*: zpl_f32  
    g*: zpl_f32  
    b*: zpl_f32  

  zpl_vec4_mat4* = object  
    x*: zpl_vec4  
    y*: zpl_vec4  
    z*: zpl_vec4  
    w*: zpl_vec4  

  zpl_xy* = object  
    x*: zpl_f32  
    y*: zpl_f32  

  in6_addr* {.pure.} = object  
    Byte*: array[16, uint8]  

  zpl_xyz* = object  
    x*: zpl_f32  
    y*: zpl_f32  
    z*: zpl_f32  

  zpl_json_object_union* {.union.} = object  
    nodes*: ptr UncheckedArray[zpl_json_object]  
    integer*: zpl_i64  
    str*: cstring  
    real*: zpl_json_object_real  
    constant*: zpl_u8  

  zpl_vec2_mat2* = object  
    x*: zpl_vec2  
    y*: zpl_vec2  

  zpl_opts_entry_sub* {.union.} = object  
    text*: zpl_string  
    integer*: zpl_i64  
    real*: zpl_f64  


  #  include errno.h for MinGW
  # 
  # 
  #  Base Types
  # 
  # 
  zpl_u8* = uint8
  zpl_i8* = int8
  zpl_u16* = uint16
  zpl_i16* = int16
  zpl_u32* = uint32
  zpl_i32* = int32
  zpl_u64* = uint64
  zpl_i64* = int64
  # Type 'static_assertion_static_assertion_at_line_598' skipped
  # Type 'static_assertion_static_assertion_at_line_599' skipped
  # Type 'static_assertion_static_assertion_at_line_600' skipped
  # Type 'static_assertion_static_assertion_at_line_601' skipped
  # Type 'static_assertion_static_assertion_at_line_603' skipped
  # Type 'static_assertion_static_assertion_at_line_604' skipped
  # Type 'static_assertion_static_assertion_at_line_605' skipped
  # Type 'static_assertion_static_assertion_at_line_606' skipped
  zpl_usize* = uint
  zpl_isize* = ptrdiff_t
  # Type 'static_assertion_static_assertion_at_line_611' skipped

  #  NOTE: (u)zpl_intptr is only here for semantic reasons really as this library will only support 32/64 bit OSes.
  zpl_intptr* = clonglong
  zpl_uintptr* = culonglong
  # Type 'static_assertion_static_assertion_at_line_633' skipped
  zpl_f32* = cfloat
  zpl_f64* = cdouble
  # Type 'static_assertion_static_assertion_at_line_638' skipped
  # Type 'static_assertion_static_assertion_at_line_639' skipped
  zpl_rune* = zpl_i32

  #  NOTE: Unicode codepoint
  zpl_char32* = zpl_i32
  zpl_b8* = zpl_i8
  zpl_b16* = zpl_i16
  zpl_b32* = zpl_i32

  # ! Copy non-overlapping array.
  # ! Copy an array.
  # 
  # 
  #  Virtual Memory
  # 
  # 
  zpl_virtual_memory* {.bycopy.} = object
    data*: pointer
    size*: zpl_isize

  #  NOTE: This is useful so you can define an allocator of the same type and parameters
  zpl_allocator_proc* = proc(allocator_data: pointer, `type`: zplAllocationType, size: zpl_isize, alignment: zpl_isize, old_memory: pointer, old_size: zpl_isize, flags: zpl_u64): pointer {.cdecl.}
  zpl_allocator* {.bycopy.} = object
    `proc`*: ptr zpl_allocator_proc
    data*: pointer

  # ! Helper to allocate memory using heap allocator.
  # ! Helper to free memory allocated by heap allocator.
  # ! Alias to heap allocator.
  # 
  #  Arena Allocator
  # 
  zpl_arena* {.bycopy.} = object
    backing*: zpl_allocator
    physical_start*: pointer
    total_size*: zpl_isize
    total_allocated*: zpl_isize
    temp_count*: zpl_isize
  zpl_temp_arena_memory* {.bycopy.} = object
    arena*: ptr zpl_arena
    original_count*: zpl_isize

  # 
  #  Pool Allocator
  # 
  zpl_pool* {.bycopy.} = object
    backing*: zpl_allocator
    physical_start*: pointer
    free_list*: pointer
    block_size*: zpl_isize
    block_align*: zpl_isize
    total_size*: zpl_isize
  zpl_allocation_header_ev* {.bycopy.} = object
    size*: zpl_isize

  # 
  #  Scratch Memory Allocator - Ring Buffer Based Arena
  # 
  zpl_scratch_memory* {.bycopy.} = object
    physical_start*: pointer
    total_size*: zpl_isize
    alloc_point*: pointer
    free_point*: pointer

  # 
  #  Stack Memory Allocator
  # 
  zpl_stack_memory* {.bycopy.} = object
    backing*: zpl_allocator
    physical_start*: pointer
    total_size*: zpl_usize
    allocated*: zpl_usize
  zpl_atomic32* {.bycopy.} = object
    value*: zpl_i32
  zpl_atomic64* {.bycopy.} = object
    value*: zpl_i64
  zpl_atomic_ptr* = object  
    value*: ptr cint  

  zpl_semaphore* {.bycopy.} = object
    win32_handle*: pointer

  #  Mutex
  zpl_mutex* {.bycopy.} = object
    win32_critical_section*: CRITICAL_SECTION
  zpl_thread* {.bycopy.} = object
  zpl_thread_proc* = proc(thread: ptr zpl_thread): zpl_isize {.cdecl.}

  #  NOTE: Thread Merge Operation
  #  Based on Sean Barrett's stb_sync
  zpl_sync* {.bycopy.} = object
    target*: zpl_i32
    current*: zpl_i32
    waiting*: zpl_i32
    start*: zpl_mutex
    mutex*: zpl_mutex
    release*: zpl_semaphore
  zpl_affinity* = object  
    is_accurate*: zpl_b32  
    core_count*: zpl_isize  
    thread_count*: zpl_isize  
    core_masks*: array[0 .. (8 * sizeof(size_t) - 1), zpl_usize]  


  # ! @}
  # * @file sort.c
  # @brief Sorting and searching methods.
  # @defgroup sort Sorting and searching
  # Methods for sorting arrays using either Quick/Merge-sort combo or Radix sort. It also contains simple implementation of binary search, as well as an easy to use API to define your own comparators.
  # @{
  # 
  zpl_compare_proc* = proc(a: pointer, b: pointer): cint {.cdecl.}

  # 
  # 
  #  zpl_string - C Read-Only-Compatible
  # 
  # 
  # 
  #     Reasoning:
  #     By default, strings in C are null terminated which means you have to count
  #     the number of character up to the null character to calculate the length.
  #     Many "better" C string libraries will create a struct for a string.
  #     i.e.
  #     struct String {
  #     Allocator allocator;
  #     size_t    length;
  #     size_t    capacity;
  #     char *    cstring;
  #     };
  #     This library tries to augment normal C strings in a better way that is still
  #     compatible with C-style strings.
  #     +--------+-----------------------+-----------------+
  #     | Header | Binary C-style String | Null Terminator |
  #     +--------+-----------------------+-----------------+
  #     |
  #     +-> Pointer returned by functions
  #     Due to the meta-data being stored before the string pointer and every zpl string
  #     having an implicit null terminator, zpl strings are full compatible with c-style
  #     strings and read-only functions.
  #     Advantages:
  #     * zpl strings can be passed to C-style string functions without accessing a struct
  #     member of calling a function, i.e.
  #     zpl_printf("%s\n", zpl_str);
  #     Many other libraries do either of these:
  #     zpl_printf("%s\n", string->cstr);
  #     zpl_printf("%s\n", get_cstring(string));
  #     * You can access each character just like a C-style string:
  #     zpl_printf("%c %c\n", str[0], str[13]);
  #     * zpl strings are singularly allocated. The meta-data is next to the character
  #     array which is better for the cache.
  #     Disadvantages:
  #     * In the C version of these functions, many return the new string. i.e.
  #     str = zpl_string_appendc(str, "another string");
  #     This could be changed to zpl_string_appendc(&str, "another string"); but I'm still not sure.
  # 
  zpl_string* = cstring
  zpl_string_header* {.bycopy.} = object
    allocator*: zpl_allocator
    length*: zpl_isize
    capacity*: zpl_isize

  # ! @}
  # * @file regex.c
  # @brief Regular expressions parser.
  # @defgroup regex Regex processor
  # Port of gb_regex with several bugfixes applied. This is a simple regex library and is fast to perform.
  # Supported Matching:
  #     @n ^       - Beginning of string
  #     @n $       - End of string
  #     @n .       - Match one (anything)
  #     @n |       - Branch (or)
  #     @n ()      - Capturing group
  #     @n []      - Any character included in set
  #     @n [^]     - Any character excluded from set
  #     @n +       - One or more  (greedy)
  #     @n +?      - One or more  (non-greedy)
  #     @n *       - Zero or more (greedy)
  #     @n *?      - Zero or more (non-greedy)
  #     @n ?       - Zero or once
  #     @n [BACKSLASH]XX     - Hex decimal digit (must be 2 digits)
  #     @n [BACKSLASH]meta   - Meta character
  #     @n [BACKSLASH]s      - Whitespace
  #     @n [BACKSLASH]S      - Not whitespace
  #     @n [BACKSLASH]d      - Digit
  #     @n [BACKSLASH]D      - Not digit
  #     @n [BACKSLASH]a      - Alphabetic character
  #     @n [BACKSLASH]l      - Lower case letter
  #     @n [BACKSLASH]u      - Upper case letter
  #     @n [BACKSLASH]w      - Word
  #     @n [BACKSLASH]W      - Not word
  #     @n [BACKSLASH]x      - Hex Digit
  #     @n [BACKSLASH]p      - Printable ASCII character
  #     @n --Whitespace--
  #     @n [BACKSLASH]t      - Tab
  #     @n [BACKSLASH]n      - New line
  #     @n [BACKSLASH]r      - Return carriage
  #     @n [BACKSLASH]v      - Vertical Tab
  #     @n [BACKSLASH]f      - Form feed
  #     @{
  # 
  zpl_re* {.bycopy.} = object
    backing*: zpl_allocator
    capture_count*: zpl_isize
    buf*: cstring
    buf_len*: zpl_isize
    buf_cap*: zpl_isize
    can_realloc*: zpl_b32
  zpl_re_capture* {.bycopy.} = object
    str*: cstring
    len*: zpl_isize

  # ! @}
  # * @file containers.c
  # @brief Memory containers
  # @defgroup containers Memory containers
  # Memory containers in various types: buffers, arrays, linked lists, ring buffers, ....
  # @{
  # 
  # 
  # 
  #  Fixed Capacity Buffer (POD Types)
  # 
  # 
  #  zpl_buffer(Type) works like zpl_string or zpl_array where the actual type is just a pointer to the first
  #  element.
  # 
  #  Available Procedures for zpl_buffer(Type)
  #  zpl_buffer_init
  #  zpl_buffer_free
  #  zpl_buffer_append
  #  zpl_buffer_appendv
  #  zpl_buffer_pop
  #  zpl_buffer_clear
  zpl_buffer_header* {.bycopy.} = object
    backing*: zpl_allocator
    count*: zpl_isize
    capacity*: zpl_isize

  #  DEPRECATED(zpl_buffer_free): Use zpl_buffer_free2 instead
  # 
  # 
  #  Linked List
  # 
  #  zpl_list encapsulates pointer to data and points to the next and the previous element in the list.
  # 
  #  Available Procedures for zpl_list
  #  zpl_list_init
  #  zpl_list_add
  #  zpl_list_remove
  zpl_list* {.bycopy.} = object
    `ptr`*: pointer
    next*: ptr zpl_list
    prev*: ptr zpl_list

  # 
  # 
  #  Dynamic Array (POD Types)
  # 
  #  zpl_array(Type) works like zpl_string or zpl_buffer where the actual type is just a pointer to the first
  #  element.
  # 
  #  Available Procedures for zpl_array(Type)
  #  zpl_array_init
  #  zpl_array_free
  #  zpl_array_set_capacity
  #  zpl_array_grow
  #  zpl_array_append
  #  zpl_array_appendv
  #  zpl_array_pop
  #  zpl_array_clear
  #  zpl_array_back
  #  zpl_array_front
  #  zpl_array_resize
  #  zpl_array_reserve
  # 
  zpl_array_header* {.bycopy.} = object
    data*: cstring
    count*: zpl_isize
    capacity*: zpl_isize
    allocator*: zpl_allocator
  # Type 'static_assertion_static_assertion_at_line_2152' skipped

  # ! @}
  # * @file hashtable.c
  # @brief Instantiated hash table
  # @defgroup hashtable Instantiated hash table
  # @n
  # @n This is an attempt to implement a templated hash table
  # @n NOTE: The key is always a zpl_u64 for simplicity and you will _probably_ _never_ need anything bigger.
  # @n
  # @n Hash table type and function declaration, call: ZPL_TABLE_DECLARE(PREFIX, NAME, N, VALUE)
  # @n Hash table function definitions, call: ZPL_TABLE_DEFINE(NAME, N, VALUE)
  # @n
  # @n     PREFIX  - a prefix for function prototypes e.g. extern, static, etc.
  # @n     NAME    - Name of the Hash Table
  # @n     FUNC    - the name will prefix function names
  # @n     VALUE   - the type of the value to be stored
  # @n
  # @n tablename_init(NAME * h, zpl_allocator a);
  # @n tablename_destroy(NAME * h);
  # @n tablename_get(NAME * h, zpl_u64 key);
  # @n tablename_set(NAME * h, zpl_u64 key, VALUE value);
  # @n tablename_grow(NAME * h);
  # @n tablename_rehash(NAME * h, zpl_isize new_count);
  # @n tablename_remove(NAME * h, zpl_u64 key);
  #  @{
  # 
  zpl_hash_table_find_result* {.bycopy.} = object
    hash_index*: zpl_isize
    entry_prev*: zpl_isize
    entry_index*: zpl_isize

  # ! @}
  # * @file file.c
  # @brief File handling
  # @defgroup fileio File handling
  # File I/O operations as well as path and folder structure manipulation methods. With threading enabled, it also offers async read/write methods.
  # @{
  # 
  zpl_file_mode* = zpl_u32
  zpl_file_descriptor* {.bycopy, union.} = object
    p*: pointer
    i*: zpl_intptr
    u*: zpl_uintptr
  zpl_file_operations* {.incompleteStruct.} = object
  zpl_file_open_proc* = proc(fd: ptr zpl_file_descriptor, ops: ptr zpl_file_operations, mode: zpl_file_mode, filename: cstring): zpl_file_error {.cdecl.}
  zpl_file_read_proc* = proc(fd: zpl_file_descriptor, buffer: pointer, size: zpl_isize, offset: zpl_i64, bytes_read: ptr zpl_isize, stop_at_newline: zpl_b32): zpl_b32 {.cdecl.}
  zpl_file_write_proc* = proc(fd: zpl_file_descriptor, buffer: pointer, size: zpl_isize, offset: zpl_i64, bytes_written: ptr zpl_isize): zpl_b32 {.cdecl.}
  zpl_file_seek_proc* = proc(fd: zpl_file_descriptor, offset: zpl_i64, whence: zpl_seek_whence_type, new_offset: ptr zpl_i64): zpl_b32 {.cdecl.}
  zpl_file_close_proc* = proc(fd: zpl_file_descriptor) {.cdecl.}
  zpl_file_time* = zpl_u64
  zpl_dir_info* {.bycopy.} = object
  zpl_dir_entry* {.bycopy.} = object
    filename*: cstring
    dir_info*: ptr zpl_dir_info
    `type`*: zpl_u8
  zpl_file* {.bycopy.} = object
    ops*: zpl_file_operations
    fd*: zpl_file_descriptor
    filename*: cstring
    last_write_time*: zpl_file_time
    dir*: ptr zpl_dir_entry
  zpl_file_contents* {.bycopy.} = object
    allocator*: zpl_allocator
    data*: pointer
    size*: zpl_isize

  # ! @}
  # * @file dll.c
  # @brief DLL Handling
  # @defgroup dll DLL handling
  # @{
  # 
  zpl_dll_handle* = pointer
  zpl_dll_proc* = proc() {.cdecl.}
  zpl_timer_cb* = proc(data: pointer) {.cdecl.}

  # ! Timer data structure
  zpl_timer* {.bycopy.} = object
    callback*: zpl_timer_cb
    enabled*: zpl_b32
    remaining_calls*: zpl_i32
    initial_calls*: zpl_i32
    next_call_ts*: zpl_f64
    duration*: zpl_f64
    user_data*: pointer
  zpl_timer_pool* = ptr zpl_timer

  # ! @}
  # * @file event.c
  # @brief Event handler
  # @n
  # @n Event handler
  # @n
  # @n Uses hash table to store array of callbacks per
  # @n each valid event type.
  # @n
  # @n Each event callback receives an anonymous pointer
  # @n which has to be casted to proper base type.
  # @n
  # @n Usage:
  # @n - Initialize event pool.
  # @n - Declare your event callbacks and any data layout
  # @n   used by the events.
  # @n - Add event callbacks to the pool. (Returns callback ID.)
  # @n - Trigger arbitrary event in pool with specified dataset.
  # @n - (Optional) Remove arbitrary event callback
  # @n   by refering to it through event type and its callback ID.
  # @n
  # @{
  # 
  zpl_event_data* = pointer
  zpl_event_cb* = proc(evt: zpl_event_data) {.cdecl.}
  zpl_event_block* = ptr zpl_event_cb

  # /< zpl_array
  zpl_event_poolEntry* {.bycopy.} = object
    key*: zpl_u64
    next*: zpl_isize
    value*: zpl_event_block
  zpl_event_pool* {.bycopy.} = object
    hashes*: ptr zpl_isize
    entries*: ptr zpl_event_poolEntry

  # ! @}
  # * @file misc.c
  # @brief Various other stuff
  # @defgroup misc Various other stuff
  #  Methods that don't belong anywhere but are still very useful in many occasions.
  #  @{
  # 
  zpl_random* {.bycopy.} = object
    offsets*: array[8, zpl_u32]
    value*: zpl_u32

  # ! JSON object definition.
  zpl_json_object* = object  
    backing*: zpl_allocator  
    name*: cstring  
    typ* {.bitsize: 6.}: zpl_u8  
    name_style* {.bitsize: 2.}: zpl_u8  
    props* {.bitsize: 7.}: zpl_u8  
    cfg_mode* {.bitsize: 1.}: zpl_u8  
    assign_style* {.bitsize: 4.}: zpl_u8  
    delim_style* {.bitsize: 4.}: zpl_u8  
    delim_line_width*: zpl_u8  
    zpl_json_object_u*: zpl_json_object_union  

  # Type 'zpl_u8' skipped
  # Type 'zpl_b32' skipped
  # Type 'zpl_u8' skipped
  # Type 'zpl_b32' skipped
  # Type 'zpl_string' skipped
  # Type 'zpl_i64' skipped
  # Type 'zpl_f64' skipped
  zpl_opts_entry* = object  
    name*: cstring  
    lname*: cstring  
    desc*: cstring  
    `type`*: zpl_u8  
    met*: zpl_b32  
    pos*: zpl_b32  
    opts*: zpl_opts_entry_sub  

  zpl_opts_err* {.bycopy.} = object
    val*: cstring
    `type`*: zpl_u8
  zpl_opts* {.bycopy.} = object
    alloc*: zpl_allocator
    entries*: ptr zpl_opts_entry
    errors*: ptr zpl_opts_err
    positioned*: ptr ptr zpl_opts_entry
    appname*: cstring
  zpl_pr* {.bycopy.} = object
    `in`*: zpl_file
    `out`*: zpl_file
    err*: zpl_file
    f_stdin*: pointer
    f_stdout*: pointer
    f_stderr*: pointer
    win32_handle*: HANDLE
  zpl_pr_si* {.bycopy.} = object
    con_title*: cstring
    workdir*: cstring
    env_count*: zpl_isize
    env*: ptr cstring
    posx*: zpl_u32
    posy*: zpl_u32
    resx*: zpl_u32
    resy*: zpl_u32
    bufx*: zpl_u32
    bufy*: zpl_u32
    fill_attr*: zpl_u32
    flags*: zpl_u32
    show_window*: zpl_b32

  # ! @}
  # * @file threadpool.c
  # @brief Job system
  # @defgroup jobs Job system
  #  This job system follows thread pool pattern to minimize the costs of thread initialization.
  #  It reuses fixed number of threads to process variable number of jobs.
  #  @{
  # 
  zpl_jobs_proc* = proc(data: pointer) {.cdecl.}
  zpl_thread_job* {.bycopy.} = object
    `proc`*: zpl_jobs_proc
    data*: pointer
    priority*: zpl_i32
  zpl_thread_worker* {.bycopy.} = object
    thread*: zpl_thread
    status*: zpl_atomic32
    jobid*: zpl_u32
    pool*: pointer
  zpl_thread_pool* {.bycopy.} = object
    alloc*: zpl_allocator
    max_threads*: zpl_u32
    job_spawn_treshold*: zpl_f32
    access*: zpl_mutex
    workers*: ptr zpl_thread_worker
    jobs*: ptr zpl_thread_job
    queue*: ptr zpl_u32
    available*: ptr zpl_u32
  zpl_co* {.bycopy.} = object
  zpl_co_proc* = proc(co: ptr zpl_co) {.cdecl.}

  # ! @}
  # * @file math.c
  # @brief Math operations
  # @defgroup math Math operations
  # OpenGL gamedev friendly library for math.
  # @{
  # 
  zpl_vec2* {.union.} = object  
    xy*: zpl_xy  
    e*: array[0 .. 1, zpl_f32]  

  zpl_vec3* {.union.} = object  
    xyz*: zpl_xyz  
    rgb*: zpl_rgb  
    xy*: zpl_vec2  
    e*: array[0 .. 2, zpl_f32]  

  zpl_vec4* {.union.} = object  
    xyzw*: zpl_xyzw  
    rgba*: zpl_rgba  
    xy_zw_vec4*: zpl_xy_zw_vec4  
    xyz*: zpl_vec3  
    rgb*: zpl_vec3  
    e*: array[0 .. 3, zpl_f32]  

  zpl_mat2* {.union.} = object  
    zpl_vec2_mat2*: zpl_vec2_mat2  
    col*: array[0 .. 1, zpl_vec2]  
    e*: array[0 .. 3, zpl_f32]  

  zpl_mat3* {.union.} = object  
    zpl_vec3_mat3*: zpl_vec3_mat3  
    col*: array[0 .. 2, zpl_vec3]  
    e*: array[0 .. 8, zpl_f32]  

  zpl_mat4* {.union.} = object  
    zpl_vec4_mat4*: zpl_vec4_mat4  
    col*: array[0 .. 3, zpl_vec4]  
    e*: array[0 .. 15, zpl_f32]  

  zpl_quat* {.union.} = object  
    zpl_xyzw*: zpl_xyzw  
    xyzw*: zpl_vec4  
    xyz*: zpl_vec3  
    e*: array[0 .. 3, zpl_f32]  

  zpl_float2* = array[2, zpl_f32]
  zpl_float3* = array[3, zpl_f32]
  zpl_float4* = array[4, zpl_f32]
  zpl_rect2* {.bycopy.} = object
    pos*: zpl_vec2
    dim*: zpl_vec2
  zpl_rect3* {.bycopy.} = object
    pos*: zpl_vec3
    dim*: zpl_vec3
  zpl_aabb2* {.bycopy.} = object
    centre*: zpl_vec2
    half_size*: zpl_vec2
  zpl_aabb3* {.bycopy.} = object
    centre*: zpl_vec3
    half_size*: zpl_vec3
  zpl_half* = cshort

  # ! @}
  # * @file platform.c
  # @brief Platform layer
  # @defgroup platform Platform layer
  # @n NOTE(bill):
  # @n Coordinate system - +ve x - left to right
  # @n                  - +ve y - bottom to top
  # @n                  - Relative to window
  # @n TODO(bill): Proper documentation for this with code examples
  # @n
  # @n Window Support - Complete
  # @n OS X Support (Not ported yet) - Missing:
  # @n     * Software framebuffer
  # @n     * (show|hide) window
  # @n     * show_cursor
  # @n     * toggle (fullscreen|borderless)
  # @n     * set window position
  # @n     * Clipboard
  # @n     * GameControllers
  # @n Linux Support - None
  # @n Other OS Support - None
  # @n
  # @n Ported from https:github.com/gingerBill/gb/
  # @{
  # 
  # ! @}
  # * @file opengl.c
  # @brief OpenGL helpers
  # @defgroup opengl OpenGL helpers
  #  Offers OpenGL helper methods as well as basic 2D graphics and font rendering.
  #  @{
  # 
  # ! @}
  # <<header.c>>
  # <<mem.c>>
  # <<threads.c>>
  # <<sort.c>>
  # <<string.c>>
  # <<regex.c>>
  # <<containers.c>>
  # <<hashing.c>>
  # <<hashtable.c>>
  # <<file.c>>
  # <<print.c>>
  # <<dll.c>>
  # <<time.c>>
  # <<event.c>>
  # <<misc.c>>
  # <<json.c>>
  # <<opts.c>>
  # <<process.c>>
  # <<threadpool.c>>
  # <<coroutines.c>>
  # <<math.c>>
  # <<platform.c>>
  # <<opengl.c>>
  u8* = zpl_u8
  i8* = zpl_i8
  u16* = zpl_u16
  i16* = zpl_i16
  u32* = zpl_u32
  i32* = zpl_i32
  u64* = zpl_u64
  i64* = zpl_i64
  b8* = zpl_b8
  b16* = zpl_b16
  b32* = zpl_b32
  f32* = zpl_f32
  f64* = zpl_f64
  rune* = zpl_rune
  usize* = zpl_usize
  isize* = zpl_isize
  uintptr* = zpl_uintptr
  intptr* = zpl_intptr

  #  =======================================================================
  #  !
  #  ! System differences
  #  !
  #  =======================================================================
  ENetSocket* = SOCKET
  ENetBuffer* {.bycopy.} = object
    dataLength*: uint
    data*: pointer
  ENetSocketSet* = fd_set

  #  =======================================================================
  #  !
  #  ! Basic stuff
  #  !
  #  =======================================================================
  enet_uint8* = uint8

  # *< unsigned 8-bit type
  enet_uint16* = uint16

  # *< unsigned 16-bit type
  enet_uint32* = uint32

  # *< unsigned 32-bit type
  enet_uint64* = uint64

  # *< unsigned 64-bit type
  ENetVersion* = enet_uint32
  ENetCallbacks* {.bycopy.} = object
    malloc*: proc(size: uint): pointer {.cdecl.}
    free*: proc(memory: pointer) {.cdecl.}
    no_memory*: proc() {.cdecl.}

  #  =======================================================================
  #  !
  #  ! List
  #  !
  #  =======================================================================
  ENetListNode* {.bycopy.} = object
    next*: ptr ENetListNode
    previous*: ptr ENetListNode
  ENetListIterator* = ptr ENetListNode
  ENetList* {.bycopy.} = object
    sentinel*: ENetListNode
  ENetProtocolHeader* {.bycopy.} = object
    peerID*: enet_uint16
    sentTime*: enet_uint16
  ENetProtocolCommandHeader* {.bycopy.} = object
    command*: enet_uint8
    channelID*: enet_uint8
    reliableSequenceNumber*: enet_uint16
  ENetProtocolAcknowledge* {.bycopy.} = object
    header*: ENetProtocolCommandHeader
    receivedReliableSequenceNumber*: enet_uint16
    receivedSentTime*: enet_uint16
  ENetProtocolConnect* {.bycopy.} = object
    header*: ENetProtocolCommandHeader
    outgoingPeerID*: enet_uint16
    incomingSessionID*: enet_uint8
    outgoingSessionID*: enet_uint8
    mtu*: enet_uint32
    windowSize*: enet_uint32
    channelCount*: enet_uint32
    incomingBandwidth*: enet_uint32
    outgoingBandwidth*: enet_uint32
    packetThrottleInterval*: enet_uint32
    packetThrottleAcceleration*: enet_uint32
    packetThrottleDeceleration*: enet_uint32
    connectID*: enet_uint32
    data*: enet_uint32
  ENetProtocolVerifyConnect* {.bycopy.} = object
    header*: ENetProtocolCommandHeader
    outgoingPeerID*: enet_uint16
    incomingSessionID*: enet_uint8
    outgoingSessionID*: enet_uint8
    mtu*: enet_uint32
    windowSize*: enet_uint32
    channelCount*: enet_uint32
    incomingBandwidth*: enet_uint32
    outgoingBandwidth*: enet_uint32
    packetThrottleInterval*: enet_uint32
    packetThrottleAcceleration*: enet_uint32
    packetThrottleDeceleration*: enet_uint32
    connectID*: enet_uint32
  ENetProtocolBandwidthLimit* {.bycopy.} = object
    header*: ENetProtocolCommandHeader
    incomingBandwidth*: enet_uint32
    outgoingBandwidth*: enet_uint32
  ENetProtocolThrottleConfigure* {.bycopy.} = object
    header*: ENetProtocolCommandHeader
    packetThrottleInterval*: enet_uint32
    packetThrottleAcceleration*: enet_uint32
    packetThrottleDeceleration*: enet_uint32
  ENetProtocolDisconnect* {.bycopy.} = object
    header*: ENetProtocolCommandHeader
    data*: enet_uint32
  ENetProtocolPing* {.bycopy.} = object
    header*: ENetProtocolCommandHeader
  ENetProtocolSendReliable* {.bycopy.} = object
    header*: ENetProtocolCommandHeader
    dataLength*: enet_uint16
  ENetProtocolSendUnreliable* {.bycopy.} = object
    header*: ENetProtocolCommandHeader
    unreliableSequenceNumber*: enet_uint16
    dataLength*: enet_uint16
  ENetProtocolSendUnsequenced* {.bycopy.} = object
    header*: ENetProtocolCommandHeader
    unsequencedGroup*: enet_uint16
    dataLength*: enet_uint16
  ENetProtocolSendFragment* {.bycopy.} = object
    header*: ENetProtocolCommandHeader
    startSequenceNumber*: enet_uint16
    dataLength*: enet_uint16
    fragmentCount*: enet_uint32
    fragmentNumber*: enet_uint32
    totalLength*: enet_uint32
    fragmentOffset*: enet_uint32
  ENetProtocol* {.bycopy, union.} = object
    header*: ENetProtocolCommandHeader
    acknowledge*: ENetProtocolAcknowledge
    connect*: ENetProtocolConnect
    verifyConnect*: ENetProtocolVerifyConnect
    disconnect*: ENetProtocolDisconnect
    ping*: ENetProtocolPing
    sendReliable*: ENetProtocolSendReliable
    sendUnreliable*: ENetProtocolSendUnreliable
    sendUnsequenced*: ENetProtocolSendUnsequenced
    sendFragment*: ENetProtocolSendFragment
    bandwidthLimit*: ENetProtocolBandwidthLimit
    throttleConfigure*: ENetProtocolThrottleConfigure

  # *
  #      * Portable internet address structure.
  #      *
  #      * The host must be specified in network byte-order, and the port must be in host
  #      * byte-order. The constant ENET_HOST_ANY may be used to specify the default
  #      * server host. The constant ENET_HOST_BROADCAST may be used to specify the
  #      * broadcast address (255.255.255.255).  This makes sense for enet_host_connect,
  #      * but not for enet_host_create.  Once a server responds to a broadcast, the
  #      * address is updated from ENET_HOST_BROADCAST to the server's actual IP address.
  # 
  ENetAddress* {.bycopy.} = object
    host*: in6_addr
    port*: enet_uint16
    sin6_scope_id*: enet_uint16
  ENetPacketFreeCallback* = proc(a1: pointer) {.cdecl.}

  # *
  #      * ENet packet structure.
  #      *
  #      * An ENet data packet that may be sent to or received from a peer. The shown
  #      * fields should only be read and never modified. The data field contains the
  #      * allocated data for the packet. The dataLength fields specifies the length
  #      * of the allocated data.  The flags field is either 0 (specifying no flags),
  #      * or a bitwise-or of any combination of the following flags:
  #      *
  #      *    ENET_PACKET_FLAG_RELIABLE - packet must be received by the target peer and resend attempts should be made until the packet is delivered
  #      *    ENET_PACKET_FLAG_UNSEQUENCED - packet will not be sequenced with other packets (not supported for reliable packets)
  #      *    ENET_PACKET_FLAG_NO_ALLOCATE - packet will not allocate data, and user must supply it instead
  #      *    ENET_PACKET_FLAG_UNRELIABLE_FRAGMENT - packet will be fragmented using unreliable (instead of reliable) sends if it exceeds the MTU
  #      *    ENET_PACKET_FLAG_SENT - whether the packet has been sent from all queues it has been entered into
  #      * @sa ENetPacketFlag
  # 
  ENetPacket* {.bycopy.} = object
    referenceCount*: uint
    flags*: enet_uint32
    data*: ptr enet_uint8
    dataLength*: uint
    freeCallback*: ENetPacketFreeCallback
    userData*: pointer
  ENetAcknowledgement* {.bycopy.} = object
    acknowledgementList*: ENetListNode
    sentTime*: enet_uint32
    command*: ENetProtocol
  ENetOutgoingCommand* {.bycopy.} = object
    outgoingCommandList*: ENetListNode
    reliableSequenceNumber*: enet_uint16
    unreliableSequenceNumber*: enet_uint16
    sentTime*: enet_uint32
    roundTripTimeout*: enet_uint32
    roundTripTimeoutLimit*: enet_uint32
    fragmentOffset*: enet_uint32
    fragmentLength*: enet_uint16
    sendAttempts*: enet_uint16
    command*: ENetProtocol
    packet*: ptr ENetPacket
  ENetIncomingCommand* {.bycopy.} = object
    incomingCommandList*: ENetListNode
    reliableSequenceNumber*: enet_uint16
    unreliableSequenceNumber*: enet_uint16
    command*: ENetProtocol
    fragmentCount*: enet_uint32
    fragmentsRemaining*: enet_uint32
    fragments*: ptr enet_uint32
    packet*: ptr ENetPacket
  ENetChannel* {.bycopy.} = object
    outgoingReliableSequenceNumber*: enet_uint16
    outgoingUnreliableSequenceNumber*: enet_uint16
    usedReliableWindows*: enet_uint16
    reliableWindows*: array[ENET_PEER_RELIABLE_WINDOWS, enet_uint16]
    incomingReliableSequenceNumber*: enet_uint16
    incomingUnreliableSequenceNumber*: enet_uint16
    incomingReliableCommands*: ENetList
    incomingUnreliableCommands*: ENetList

  # *
  #      * An ENet peer which data packets may be sent or received from.
  #      *
  #      * No fields should be modified unless otherwise specified.
  # 
  ENetPeer* {.bycopy.} = object
    dispatchList*: ENetListNode
    host*: ptr ENetHost
    outgoingPeerID*: enet_uint16
    incomingPeerID*: enet_uint16
    connectID*: enet_uint32
    outgoingSessionID*: enet_uint8
    incomingSessionID*: enet_uint8
    address*: ENetAddress
    data*: pointer
    state*: ENetPeerState
    channels*: ptr ENetChannel
    channelCount*: uint
    incomingBandwidth*: enet_uint32
    outgoingBandwidth*: enet_uint32
    incomingBandwidthThrottleEpoch*: enet_uint32
    outgoingBandwidthThrottleEpoch*: enet_uint32
    incomingDataTotal*: enet_uint32
    totalDataReceived*: enet_uint64
    outgoingDataTotal*: enet_uint32
    totalDataSent*: enet_uint64
    lastSendTime*: enet_uint32
    lastReceiveTime*: enet_uint32
    nextTimeout*: enet_uint32
    earliestTimeout*: enet_uint32
    packetLossEpoch*: enet_uint32
    packetsSent*: enet_uint32
    totalPacketsSent*: enet_uint64
    packetsLost*: enet_uint32
    totalPacketsLost*: enet_uint32
    packetLoss*: enet_uint32
    packetLossVariance*: enet_uint32
    packetThrottle*: enet_uint32
    packetThrottleLimit*: enet_uint32
    packetThrottleCounter*: enet_uint32
    packetThrottleEpoch*: enet_uint32
    packetThrottleAcceleration*: enet_uint32
    packetThrottleDeceleration*: enet_uint32
    packetThrottleInterval*: enet_uint32
    pingInterval*: enet_uint32
    timeoutLimit*: enet_uint32
    timeoutMinimum*: enet_uint32
    timeoutMaximum*: enet_uint32
    lastRoundTripTime*: enet_uint32
    lowestRoundTripTime*: enet_uint32
    lastRoundTripTimeVariance*: enet_uint32
    highestRoundTripTimeVariance*: enet_uint32
    roundTripTime*: enet_uint32
    roundTripTimeVariance*: enet_uint32
    mtu*: enet_uint32
    windowSize*: enet_uint32
    reliableDataInTransit*: enet_uint32
    outgoingReliableSequenceNumber*: enet_uint16
    acknowledgements*: ENetList
    sentReliableCommands*: ENetList
    sentUnreliableCommands*: ENetList
    outgoingReliableCommands*: ENetList
    outgoingUnreliableCommands*: ENetList
    dispatchedCommands*: ENetList
    needsDispatch*: cint
    incomingUnsequencedGroup*: enet_uint16
    outgoingUnsequencedGroup*: enet_uint16
    unsequencedWindow*: array[(ENET_PEER_UNSEQUENCED_WINDOW_SIZE / 32).int, enet_uint32]
    eventData*: enet_uint32
    totalWaitingData*: uint

  # * An ENet packet compressor for compressing UDP packets before socket sends or receives.
  ENetCompressor* {.bycopy.} = object
    context*: pointer
    compress*: proc(context: pointer, inBuffers: ptr ENetBuffer, inBufferCount: uint, inLimit: uint, outData: ptr enet_uint8, outLimit: uint): uint {.cdecl.}
    decompress*: proc(context: pointer, inData: ptr enet_uint8, inLimit: uint, outData: ptr enet_uint8, outLimit: uint): uint {.cdecl.}
    destroy*: proc(context: pointer) {.cdecl.}

  # * Callback that computes the checksum of the data held in buffers[0:bufferCount-1]
  ENetChecksumCallback* = proc(buffers: ptr ENetBuffer, bufferCount: uint): enet_uint32 {.cdecl.}

  # * Callback for intercepting received raw UDP packets. Should return 1 to intercept, 0 to ignore, or -1 to propagate an error.
  ENetInterceptCallback* = proc(host: ptr ENetHost, event: pointer): cint {.cdecl.}

  # * An ENet host for communicating with peers.
  #      *
  #      * No fields should be modified unless otherwise stated.
  #      *
  #      *  @sa enet_host_create()
  #      *  @sa enet_host_destroy()
  #      *  @sa enet_host_connect()
  #      *  @sa enet_host_service()
  #      *  @sa enet_host_flush()
  #      *  @sa enet_host_broadcast()
  #      *  @sa enet_host_compress()
  #      *  @sa enet_host_channel_limit()
  #      *  @sa enet_host_bandwidth_limit()
  #      *  @sa enet_host_bandwidth_throttle()
  # 
  ENetHost* = object  
    socket*: ENetSocket  
    address*: ENetAddress  
    incomingBandwidth*: enet_uint32  
    outgoingBandwidth*: enet_uint32  
    bandwidthThrottleEpoch*: enet_uint32  
    mtu*: enet_uint32  
    randomSeed*: enet_uint32  
    recalculateBandwidthLimits*: cint  
    peers*: UncheckedArray[ENetPeer]  
    peerCount*: size_t  
    channelLimit*: size_t  
    serviceTime*: enet_uint32  
    dispatchQueue*: ENetList  
    continueSending*: cint  
    packetSize*: size_t  
    headerFlags*: enet_uint16  
    commands*: UncheckedArray[array[0 ..  
        (ENET_PROTOCOL_MAXIMUM_PACKET_COMMANDS - 1).int, ENetProtocol]]  
    commandCount*: size_t  
    buffers*: UncheckedArray[array[0 .. (ENET_BUFFER_MAXIMUM - 1).int, ENetBuffer]]  
    bufferCount*: size_t  
    checksum*: ENetChecksumCallback  
    compressor*: ENetCompressor  
    packetData*: array[0 .. 1,  
                      array[0 .. (ENET_PROTOCOL_MAXIMUM_MTU - 1).int, enet_uint8]]  
    receivedAddress*: ENetAddress  
    receivedData*: UncheckedArray[enet_uint8]  
    receivedDataLength*: size_t  
    totalSentData*: enet_uint32  
    totalSentPackets*: enet_uint32  
    totalReceivedData*: enet_uint32  
    totalReceivedPackets*: enet_uint32  
    intercept*: ENetInterceptCallback  
    connectedPeers*: size_t  
    bandwidthLimitedPeers*: size_t  
    duplicatePeers*: size_t  
    maximumPacketSize*: size_t  
    maximumWaitingData*: size_t  


  # *
  #      * An ENet event as returned by enet_host_service().
  #      *
  #      * @sa enet_host_service
  # 
  ENetEvent* {.bycopy.} = object
    `type`*: ENetEventType
    peer*: ptr ENetPeer
    channelID*: enet_uint8
    data*: enet_uint32
    packet*: ptr ENetPacket

  #  =======================================================================
  #  !
  #  ! Public API
  #  !
  #  =======================================================================
  librg_ctx* {.bycopy.} = object
  librg_data* {.bycopy.} = object
  librg_entity* {.bycopy.} = object
  librg_address* {.bycopy.} = object
  librg_message* {.bycopy.} = object
  librg_event* {.bycopy.} = object
  librg_peer* = ENetPeer
  librg_packet* = ENetPacket

  # *
  #  * Simple host address
  #  * used to configure network on start
  # 
  librg_entity_cb* = proc(ctx: ptr librg_ctx, entity: ptr librg_entity) {.cdecl.}
  librg_message_cb* = proc(msg: ptr librg_message) {.cdecl.}
  librg_event_cb* = proc(event: ptr librg_event) {.cdecl.}
  librg_event_block* = ptr ptr librg_event_cb

  # /< zpl_array
  librg_tableEntry* {.bycopy.} = object
    key*: zpl_u64
    next*: zpl_isize
    value*: u32
  librg_table* {.bycopy.} = object
    hashes*: ptr zpl_isize
    entries*: ptr librg_tableEntry
  librg_event_poolEntry* {.bycopy.} = object
    key*: zpl_u64
    next*: zpl_isize
    value*: librg_event_block
  librg_event_pool* {.bycopy.} = object
    hashes*: ptr zpl_isize
    entries*: ptr librg_event_poolEntry

  #  =======================================================================
  #  !
  #  ! Main context definition
  #  !
  #  =======================================================================
  librg_snapshot* {.bycopy.} = object
    time*: f64
    data*: pointer
    size*: usize
    peer*: ptr librg_peer
  zpl_ring_librg_snapshot* {.bycopy.} = object
    backing*: zpl_allocator
    buf*: ptr librg_snapshot
    head*: zpl_usize
    tail*: zpl_usize
    capacity*: zpl_usize
  librg_space_node* {.bycopy.} = object
    blob*: ptr librg_entity
    unused*: b32
  librg_space* {.bycopy.} = object
    allocator*: zpl_allocator
    max_nodes*: u32
    dimensions*: isize
    boundary*: zpl_aabb3
    min_bounds*: zpl_vec3
    use_min_bounds*: b32
    free_nodes*: ptr usize
    spaces*: ptr librg_space
    nodes*: ptr librg_space_node

  # *
  #  * Context + config struct
  # 
  # Type 'librg_ctx' skipped
  # Type 'u16' skipped
  # Type 'f64' skipped
  # Type 'u16' skipped
  # Type 'u32' skipped
  # Type 'zpl_vec3' skipped
  # Type 'zpl_vec3' skipped
  # Type 'f64' skipped
  # Type 'librg_ctx' skipped
  # Type 'u16' skipped
  # Type 'f64' skipped
  # Type 'u16' skipped
  # Type 'u32' skipped
  # Type 'zpl_vec3' skipped
  # Type 'zpl_vec3' skipped
  # Type 'f64' skipped
  # Type 'librg_peer' skipped
  # Type 'librg_host' skipped
  # Type 'librg_table' skipped
  # Type 'librg_address' skipped
  # Type 'b32' skipped
  # Type 'b32' skipped
  # Type 'u32' skipped
  # Type 'u32' skipped
  # Type 'librg_table' skipped
  # Type 'librg_entity' skipped
  # Type 'u32' skipped
  # Type 'librg_message' skipped
  # Type 'librg_data' skipped
  # Type 'librg_data' skipped
  # Type 'librg_data' skipped
  # Type 'librg_data' skipped
  # Type 'librg_data' skipped
  # Type 'f64' skipped
  # Type 'f64' skipped
  # Type 'f64' skipped
  # Type 'f64' skipped
  # Type 'f32' skipped
  # Type 'zpl_timer' skipped
  # Type 'usize' skipped
  # Type 'zpl_timer' skipped
  # Type 'zpl_ring_librg_snapshot' skipped
  # Type 'librg_message_cb' skipped
  # Type 'zpl_allocator' skipped
  # Type 'zpl_timer_pool' skipped
  # Type 'librg_event_pool' skipped
  # Type 'librg_space' skipped
  # Type 'librg_ctx' skipped



# 
# 
#  Macro Fun!
# 
# 
#  WARN(ZaKlaus): Supported only on GCC via GNU extensions!!!
#  NOTE: Very useful bit setting
#  NOTE: Some compilers support applying printf-style warnings to user functions.
#  Multiline string literals in C99!
# 
# 
#  Debug
# 
# 
#  NOTE: Things that shouldn't happen with a message!
proc zpl_assert_handler*(condition: cstring, file: cstring, line: zpl_i32, msg: cstring) {.implibrgC.}
proc zpl_assert_crash*(condition: cstring): zpl_i32 {.implibrgC.}

# ! @}
# * @file mem.c
# @brief Memory manipulation and helpers.
# @defgroup memman Memory management
#  Consists of pointer arithmetic methods, virtual memory management and custom memory allocators.
#  @{
# 
# ! Checks if value is power of 2.
proc zpl_is_power_of_two*(x: zpl_isize): zpl_b32 {.implibrgC.}

# ! Aligns address to specified alignment.
proc zpl_align_forward*(`ptr`: pointer, alignment: zpl_isize): pointer {.implibrgC.}

# ! Moves pointer forward by bytes.
proc zpl_pointer_add*(`ptr`: pointer, bytes: zpl_isize): pointer {.implibrgC.}

# ! Moves pointer backward by bytes.
proc zpl_pointer_sub*(`ptr`: pointer, bytes: zpl_isize): pointer {.implibrgC.}

# ! Moves pointer forward by bytes.
proc zpl_pointer_add_const*(`ptr`: pointer, bytes: zpl_isize): pointer {.implibrgC.}

# ! Moves pointer backward by bytes.
proc zpl_pointer_sub_const*(`ptr`: pointer, bytes: zpl_isize): pointer {.implibrgC.}

# ! Calculates difference between two addresses.
proc zpl_pointer_diff*(begin: pointer, `end`: pointer): zpl_isize {.implibrgC.}

# ! Clears up memory at location by specified size.
# ! @param ptr Memory location to clear up.
# ! @param size The size to clear up with.
proc zpl_zero_size*(`ptr`: pointer, size: zpl_isize) {.implibrgC.}

# ! Clears up an item.
# ! Clears up an array.
# ! Copy non-overlapping memory from source to destination.
proc zpl_memcopy*(dest: pointer, source: pointer, size: zpl_isize): pointer {.implibrgC.}

# ! Copy memory from source to destination.
proc zpl_memmove*(dest: pointer, source: pointer, size: zpl_isize): pointer {.implibrgC.}

# ! Set constant value at memory location with specified size.
proc zpl_memset*(data: pointer, byte_value: zpl_u8, size: zpl_isize): pointer {.implibrgC.}

# ! Compare two memory locations with specified size.
proc zpl_memcompare*(s1: pointer, s2: pointer, size: zpl_isize): zpl_i32 {.implibrgC.}

# ! Swap memory contents between 2 locations with size.
proc zpl_memswap*(i: pointer, j: pointer, size: zpl_isize) {.implibrgC.}

# ! Search for a constant value within the size limit at memory location.
proc zpl_memchr*(data: pointer, byte_value: zpl_u8, size: zpl_isize): pointer {.implibrgC.}

# ! Search for a constant value within the size limit at memory location in backwards.
proc zpl_memrchr*(data: pointer, byte_value: zpl_u8, size: zpl_isize): pointer {.implibrgC.}

# ! Initialize virtual memory from existing data.
proc zpl_vm*(data: pointer, size: zpl_isize): zpl_virtual_memory {.implibrgC.}

# ! Allocate virtual memory at address with size.
# ! @param addr The starting address of the region to reserve. If NULL, it lets operating system to decide where to allocate it.
# ! @param size The size to server.
proc zpl_vm_alloc*(`addr`: pointer, size: zpl_isize): zpl_virtual_memory {.implibrgC.}

# ! Release the virtual memory.
proc zpl_vm_free*(vm: zpl_virtual_memory): zpl_b32 {.implibrgC.}

# ! Trim virtual memory.
proc zpl_vm_trim*(vm: zpl_virtual_memory, lead_size: zpl_isize, size: zpl_isize): zpl_virtual_memory {.implibrgC.}

# ! Purge virtual memory.
proc zpl_vm_purge*(vm: zpl_virtual_memory): zpl_b32 {.implibrgC.}

# ! Retrieve VM's page size and alignment.
proc zpl_virtual_memory_page_size*(alignment_out: ptr zpl_isize): zpl_isize {.implibrgC.}

# ! Allocate memory with specified alignment.
proc zpl_alloc_align*(a: zpl_allocator, size: zpl_isize, alignment: zpl_isize): pointer {.implibrgC.}

# ! Allocate memory with default alignment.
proc zpl_alloc*(a: zpl_allocator, size: zpl_isize): pointer {.implibrgC.}

# ! Free allocated memory.
proc zpl_free*(a: zpl_allocator, `ptr`: pointer) {.implibrgC.}

# ! Free all memory allocated by an allocator.
proc zpl_free_all*(a: zpl_allocator) {.implibrgC.}

# ! Resize an allocated memory.
proc zpl_resize*(a: zpl_allocator, `ptr`: pointer, old_size: zpl_isize, new_size: zpl_isize): pointer {.implibrgC.}

# ! Resize an allocated memory with specified alignment.
proc zpl_resize_align*(a: zpl_allocator, `ptr`: pointer, old_size: zpl_isize, new_size: zpl_isize, alignment: zpl_isize): pointer {.implibrgC.}

# ! Allocate memory and copy data into it.
proc zpl_alloc_copy*(a: zpl_allocator, src: pointer, size: zpl_isize): pointer {.implibrgC.}

# ! Allocate memory with specified alignment and copy data into it.
proc zpl_alloc_copy_align*(a: zpl_allocator, src: pointer, size: zpl_isize, alignment: zpl_isize): pointer {.implibrgC.}

# ! Allocate memory for null-terminated C-String.
proc zpl_alloc_str*(a: zpl_allocator, str: cstring): cstring {.implibrgC.}

# ! Allocate memory for C-String with specified size.
proc zpl_alloc_str_len*(a: zpl_allocator, str: cstring, len: zpl_isize): cstring {.implibrgC.}

# ! Allocate memory for an item.
# ! Allocate memory for an array of items.
# ! Allocate/Resize memory using default options.
# ! Use this if you don't need a "fancy" resize allocation
proc zpl_default_resize_align*(a: zpl_allocator, `ptr`: pointer, old_size: zpl_isize, new_size: zpl_isize, alignment: zpl_isize): pointer {.implibrgC.}

# ! The heap allocator backed by operating system's memory manager.
proc zpl_heap_allocator*(): zpl_allocator {.implibrgC.}
proc zpl_heap_allocator_proc*(allocator_data: pointer, `type`: zplAllocationType, size: zpl_isize, alignment: zpl_isize, old_memory: pointer, old_size: zpl_isize, flags: zpl_u64): pointer {.implibrgC.}

# ! Initialize memory arena from existing memory region.
proc zpl_arena_init_from_memory*(arena: ptr zpl_arena, start: pointer, size: zpl_isize) {.implibrgC.}

# ! Initialize memory arena using existing memory allocator.
proc zpl_arena_init_from_allocator*(arena: ptr zpl_arena, backing: zpl_allocator, size: zpl_isize) {.implibrgC.}

# ! Initialize memory arena within an existing parent memory arena.
proc zpl_arena_init_sub*(arena: ptr zpl_arena, parent_arena: ptr zpl_arena, size: zpl_isize) {.implibrgC.}

# ! Release the memory used by memory arena.
proc zpl_arena_free*(arena: ptr zpl_arena) {.implibrgC.}

# ! Retrieve memory arena's aligned allocation address.
proc zpl_arena_alignment_of*(arena: ptr zpl_arena, alignment: zpl_isize): zpl_isize {.implibrgC.}

# ! Retrieve memory arena's remaining size.
proc zpl_arena_size_remaining*(arena: ptr zpl_arena, alignment: zpl_isize): zpl_isize {.implibrgC.}

# ! Check whether memory arena has any temporary snapshots.
proc zpl_arena_check*(arena: ptr zpl_arena) {.implibrgC.}

# ! Allocation Types: alloc, free_all, resize
proc zpl_arena_allocator*(arena: ptr zpl_arena): zpl_allocator {.implibrgC.}
proc zpl_arena_allocator_proc*(allocator_data: pointer, `type`: zplAllocationType, size: zpl_isize, alignment: zpl_isize, old_memory: pointer, old_size: zpl_isize, flags: zpl_u64): pointer {.implibrgC.}

# ! Capture a snapshot of used memory in a memory arena.
proc zpl_temp_arena_memory_begin*(arena: ptr zpl_arena): zpl_temp_arena_memory {.implibrgC.}

# ! Reset memory arena's usage by a captured snapshot.
proc zpl_temp_arena_memory_end*(tmp_mem: zpl_temp_arena_memory) {.implibrgC.}

# ! Initialize pool allocator.
proc zpl_pool_init*(pool: ptr zpl_pool, backing: zpl_allocator, num_blocks: zpl_isize, block_size: zpl_isize) {.implibrgC.}

# ! Initialize pool allocator with specific block alignment.
proc zpl_pool_init_align*(pool: ptr zpl_pool, backing: zpl_allocator, num_blocks: zpl_isize, block_size: zpl_isize, block_align: zpl_isize) {.implibrgC.}

# ! Release the resources used by pool allocator.
proc zpl_pool_free*(pool: ptr zpl_pool) {.implibrgC.}

# ! Allocation Types: alloc, free
proc zpl_pool_allocator*(pool: ptr zpl_pool): zpl_allocator {.implibrgC.}
proc zpl_pool_allocator_proc*(allocator_data: pointer, `type`: zplAllocationType, size: zpl_isize, alignment: zpl_isize, old_memory: pointer, old_size: zpl_isize, flags: zpl_u64): pointer {.implibrgC.}
proc zpl_allocation_header*(data: pointer): ptr zpl_allocation_header_ev {.implibrgC.}
proc zpl_allocation_header_fill*(header: ptr zpl_allocation_header_ev, data: pointer, size: zpl_isize) {.implibrgC.}

# ! Initialize ring buffer arena.
proc zpl_scratch_memory_init*(s: ptr zpl_scratch_memory, start: pointer, size: zpl_isize) {.implibrgC.}

# ! Check whether ring buffer arena is in use.
proc zpl_scratch_memory_is_in_use*(s: ptr zpl_scratch_memory, `ptr`: pointer): zpl_b32 {.implibrgC.}

# ! Allocation Types: alloc, free, free_all, resize
proc zpl_scratch_allocator*(s: ptr zpl_scratch_memory): zpl_allocator {.implibrgC.}
proc zpl_scratch_allocator_proc*(allocator_data: pointer, `type`: zplAllocationType, size: zpl_isize, alignment: zpl_isize, old_memory: pointer, old_size: zpl_isize, flags: zpl_u64): pointer {.implibrgC.}

# ! Initialize stack allocator from existing memory.
proc zpl_stack_memory_init_from_memory*(s: ptr zpl_stack_memory, start: pointer, size: zpl_isize) {.implibrgC.}

# ! Initialize stack allocator using existing memory allocator.
proc zpl_stack_memory_init*(s: ptr zpl_stack_memory, backing: zpl_allocator, size: zpl_isize) {.implibrgC.}

# ! Check whether stack allocator is in use.
proc zpl_stack_memory_is_in_use*(s: ptr zpl_stack_memory, `ptr`: pointer): zpl_b32 {.implibrgC.}

# ! Release the resources used by stack allocator.
proc zpl_stack_memory_free*(s: ptr zpl_stack_memory) {.implibrgC.}

# ! Allocation Types: alloc, free, free_all
proc zpl_stack_allocator*(s: ptr zpl_stack_memory): zpl_allocator {.implibrgC.}
proc zpl_stack_allocator_proc*(allocator_data: pointer, `type`: zplAllocationType, size: zpl_isize, alignment: zpl_isize, old_memory: pointer, old_size: zpl_isize, flags: zpl_u64): pointer {.implibrgC.}
# Declaration 'zpl_atomic32_load' skipped
# Declaration 'zpl_atomic32_load' skipped
# Declaration 'a' skipped
proc zpl_atomic32_store*(a: ptr zpl_atomic32, value: zpl_i32) {.implibrgC.}
proc zpl_atomic32_compare_exchange*(a: ptr zpl_atomic32, expected: zpl_i32, desired: zpl_i32): zpl_i32 {.implibrgC.}
proc zpl_atomic32_exchange*(a: ptr zpl_atomic32, desired: zpl_i32): zpl_i32 {.implibrgC.}
proc zpl_atomic32_fetch_add*(a: ptr zpl_atomic32, operand: zpl_i32): zpl_i32 {.implibrgC.}
proc zpl_atomic32_fetch_and*(a: ptr zpl_atomic32, operand: zpl_i32): zpl_i32 {.implibrgC.}
proc zpl_atomic32_fetch_or*(a: ptr zpl_atomic32, operand: zpl_i32): zpl_i32 {.implibrgC.}
proc zpl_atomic32_spin_lock*(a: ptr zpl_atomic32, time_out: zpl_isize): zpl_b32 {.implibrgC.}

#  NOTE: time_out = -1 as default
proc zpl_atomic32_spin_unlock*(a: ptr zpl_atomic32) {.implibrgC.}
proc zpl_atomic32_try_acquire_lock*(a: ptr zpl_atomic32): zpl_b32 {.implibrgC.}
# Declaration 'zpl_atomic64_load' skipped
# Declaration 'zpl_atomic64_load' skipped
# Declaration 'a' skipped
proc zpl_atomic64_store*(a: ptr zpl_atomic64, value: zpl_i64) {.implibrgC.}
proc zpl_atomic64_compare_exchange*(a: ptr zpl_atomic64, expected: zpl_i64, desired: zpl_i64): zpl_i64 {.implibrgC.}
proc zpl_atomic64_exchange*(a: ptr zpl_atomic64, desired: zpl_i64): zpl_i64 {.implibrgC.}
proc zpl_atomic64_fetch_add*(a: ptr zpl_atomic64, operand: zpl_i64): zpl_i64 {.implibrgC.}
proc zpl_atomic64_fetch_and*(a: ptr zpl_atomic64, operand: zpl_i64): zpl_i64 {.implibrgC.}
proc zpl_atomic64_fetch_or*(a: ptr zpl_atomic64, operand: zpl_i64): zpl_i64 {.implibrgC.}
proc zpl_atomic64_spin_lock*(a: ptr zpl_atomic64, time_out: zpl_isize): zpl_b32 {.implibrgC.}

#  NOTE: time_out = -1 as default
proc zpl_atomic64_spin_unlock*(a: ptr zpl_atomic64) {.implibrgC.}
proc zpl_atomic64_try_acquire_lock*(a: ptr zpl_atomic64): zpl_b32 {.implibrgC.}
# Declaration 'zpl_atomic_ptr_load' skipped
# Declaration 'zpl_atomic_ptr_load' skipped
# Declaration 'a' skipped
proc zpl_atomic_ptr_store*(a: ptr zpl_atomic_ptr, value: pointer) {.implibrgC.}
proc zpl_atomic_ptr_compare_exchange*(a: ptr zpl_atomic_ptr, expected: pointer, desired: pointer): pointer {.implibrgC.}
proc zpl_atomic_ptr_exchange*(a: ptr zpl_atomic_ptr, desired: pointer): pointer {.implibrgC.}
proc zpl_atomic_ptr_fetch_add*(a: ptr zpl_atomic_ptr, operand: pointer): pointer {.implibrgC.}
proc zpl_atomic_ptr_fetch_and*(a: ptr zpl_atomic_ptr, operand: pointer): pointer {.implibrgC.}
proc zpl_atomic_ptr_fetch_or*(a: ptr zpl_atomic_ptr, operand: pointer): pointer {.implibrgC.}
proc zpl_atomic_ptr_spin_lock*(a: ptr zpl_atomic_ptr, time_out: zpl_isize): zpl_b32 {.implibrgC.}

#  NOTE: time_out = -1 as default
proc zpl_atomic_ptr_spin_unlock*(a: ptr zpl_atomic_ptr) {.implibrgC.}
proc zpl_atomic_ptr_try_acquire_lock*(a: ptr zpl_atomic_ptr): zpl_b32 {.implibrgC.}

#  Fences
proc zpl_yield_thread*() {.implibrgC.}
proc zpl_mfence*() {.implibrgC.}
proc zpl_sfence*() {.implibrgC.}
proc zpl_lfence*() {.implibrgC.}
proc zpl_semaphore_init*(s: ptr zpl_semaphore) {.implibrgC.}
proc zpl_semaphore_destroy*(s: ptr zpl_semaphore) {.implibrgC.}
proc zpl_semaphore_post*(s: ptr zpl_semaphore, count: zpl_i32) {.implibrgC.}
proc zpl_semaphore_release*(s: ptr zpl_semaphore) {.implibrgC.}

#  NOTE: zpl_semaphore_post(s, 1)
proc zpl_semaphore_wait*(s: ptr zpl_semaphore) {.implibrgC.}
proc zpl_mutex_init*(m: ptr zpl_mutex) {.implibrgC.}
proc zpl_mutex_destroy*(m: ptr zpl_mutex) {.implibrgC.}
proc zpl_mutex_lock*(m: ptr zpl_mutex) {.implibrgC.}
proc zpl_mutex_try_lock*(m: ptr zpl_mutex): zpl_b32 {.implibrgC.}
proc zpl_mutex_unlock*(m: ptr zpl_mutex) {.implibrgC.}
proc zpl_async_handler*(thread: ptr zpl_thread): zpl_isize {.importc: "zpl__async_handler", cdecl, dynlib: dynlibFile.}
proc zpl_thread_init*(t: ptr zpl_thread) {.implibrgC.}
proc zpl_thread_destroy*(t: ptr zpl_thread) {.implibrgC.}
proc zpl_thread_start*(t: ptr zpl_thread, `proc`: zpl_thread_proc, data: pointer) {.implibrgC.}
proc zpl_thread_start_with_stack*(t: ptr zpl_thread, `proc`: zpl_thread_proc, data: pointer, stack_size: zpl_isize) {.implibrgC.}
proc zpl_thread_join*(t: ptr zpl_thread) {.implibrgC.}
proc zpl_thread_is_running*(t: ptr zpl_thread): zpl_b32 {.implibrgC.}
proc zpl_thread_current_id*(): zpl_u32 {.implibrgC.}
proc zpl_thread_set_name*(t: ptr zpl_thread, name: cstring) {.implibrgC.}
proc zpl_sync_init*(s: ptr zpl_sync) {.implibrgC.}
proc zpl_sync_destroy*(s: ptr zpl_sync) {.implibrgC.}
proc zpl_sync_set_target*(s: ptr zpl_sync, count: zpl_i32) {.implibrgC.}
proc zpl_sync_release*(s: ptr zpl_sync) {.implibrgC.}
proc zpl_sync_reach*(s: ptr zpl_sync): zpl_i32 {.implibrgC.}
proc zpl_sync_reach_and_wait*(s: ptr zpl_sync) {.implibrgC.}
proc zpl_affinity_init*(a: ptr zpl_affinity) {.implibrgC.}
proc zpl_affinity_destroy*(a: ptr zpl_affinity) {.implibrgC.}
proc zpl_affinity_set*(a: ptr zpl_affinity, core: zpl_isize, thread: zpl_isize): zpl_b32 {.implibrgC.}
proc zpl_affinity_thread_count_for_core*(a: ptr zpl_affinity, core: zpl_isize): zpl_isize {.implibrgC.}

#  Procedure pointers
#  NOTE: The offset parameter specifies the offset in the structure
#  e.g. zpl_i32_cmp(zpl_offset_of(Thing, value))
#  Use 0 if it's just the type instead.
# Declaration 'i16_cmp' skipped
# Declaration 'offset' skipped
# Declaration 'a' skipped
# Declaration 'b' skipped
# Declaration 'u8_cmp' skipped
# Declaration 'offset' skipped
# Declaration 'a' skipped
# Declaration 'b' skipped
# Declaration 'i32_cmp' skipped
# Declaration 'offset' skipped
# Declaration 'a' skipped
# Declaration 'b' skipped
# Declaration 'i64_cmp' skipped
# Declaration 'offset' skipped
# Declaration 'a' skipped
# Declaration 'b' skipped
# Declaration 'isize_cmp' skipped
# Declaration 'offset' skipped
# Declaration 'a' skipped
# Declaration 'b' skipped
# Declaration 'str_cmp' skipped
# Declaration 'offset' skipped
# Declaration 'a' skipped
# Declaration 'b' skipped
# Declaration 'f32_cmp' skipped
# Declaration 'offset' skipped
# Declaration 'a' skipped
# Declaration 'b' skipped
# Declaration 'f64_cmp' skipped
# Declaration 'offset' skipped
# Declaration 'a' skipped
# Declaration 'b' skipped

#  TODO: Better sorting algorithms
# ! Sorts an array.
# ! Uses quick sort for large arrays but insertion sort for small ones.
# ! Perform sorting operation on a memory location with a specified item count and size.
proc zpl_sort*(base: pointer, count: zpl_isize, size: zpl_isize, compare_proc: zpl_compare_proc) {.implibrgC.}

#  NOTE: the count of temp == count of items
proc zpl_radix_sort_u8*(items: ptr zpl_u8, temp: ptr zpl_u8, count: zpl_isize) {.implibrgC.}
proc zpl_radix_sort_u16*(items: ptr zpl_u16, temp: ptr zpl_u16, count: zpl_isize) {.implibrgC.}
proc zpl_radix_sort_u32*(items: ptr zpl_u32, temp: ptr zpl_u32, count: zpl_isize) {.implibrgC.}
proc zpl_radix_sort_u64*(items: ptr zpl_u64, temp: ptr zpl_u64, count: zpl_isize) {.implibrgC.}

# ! Performs binary search on an array.
# ! Returns index or -1 if not found
# ! Performs binary search on a memory location with specified item count and size.
proc zpl_binary_search*(base: pointer, count: zpl_isize, size: zpl_isize, key: pointer, compare_proc: zpl_compare_proc): zpl_isize {.implibrgC.}

# ! Shuffles a memory.
proc zpl_shuffle*(base: pointer, count: zpl_isize, size: zpl_isize) {.implibrgC.}

# ! Reverses memory's contents
proc zpl_reverse*(base: pointer, count: zpl_isize, size: zpl_isize) {.implibrgC.}

# ! @}
# * @file string.c
# @brief String operations and library
# @defgroup string String library
# Offers methods for c-string manipulation, but also a string library based on gb_string, which is c-string friendly.
# @{
# 
# 
# 
#  Char Functions
# 
# 
proc zpl_char_to_lower*(c: cchar): cchar {.implibrgC.}
proc zpl_char_to_upper*(c: cchar): cchar {.implibrgC.}
proc zpl_char_is_space*(c: cchar): zpl_b32 {.implibrgC.}
proc zpl_char_is_digit*(c: cchar): zpl_b32 {.implibrgC.}
proc zpl_char_is_hex_digit*(c: cchar): zpl_b32 {.implibrgC.}
proc zpl_char_is_alpha*(c: cchar): zpl_b32 {.implibrgC.}
proc zpl_char_is_alphanumeric*(c: cchar): zpl_b32 {.implibrgC.}
proc zpl_digit_to_int*(c: cchar): zpl_i32 {.implibrgC.}
proc zpl_hex_digit_to_int*(c: cchar): zpl_i32 {.implibrgC.}
proc zpl_char_to_hex_digit*(c: cchar): zpl_u8 {.implibrgC.}

#  NOTE: ASCII only
proc zpl_str_to_lower*(str: cstring) {.implibrgC.}
proc zpl_str_to_upper*(str: cstring) {.implibrgC.}
proc zpl_str_trim*(str: cstring, skip_newline: zpl_b32): cstring {.implibrgC.}
proc zpl_str_skip*(str: cstring, c: cchar): cstring {.implibrgC.}
proc zpl_strlen*(str: cstring): zpl_isize {.implibrgC.}
proc zpl_strnlen*(str: cstring, max_len: zpl_isize): zpl_isize {.implibrgC.}
proc zpl_strcmp*(s1: cstring, s2: cstring): zpl_i32 {.implibrgC.}
proc zpl_strncmp*(s1: cstring, s2: cstring, len: zpl_isize): zpl_i32 {.implibrgC.}
proc zpl_strcpy*(dest: cstring, source: cstring): cstring {.implibrgC.}
proc zpl_strdup*(a: zpl_allocator, src: cstring, max_len: zpl_isize): cstring {.implibrgC.}
proc zpl_strncpy*(dest: cstring, source: cstring, len: zpl_isize): cstring {.implibrgC.}
proc zpl_strlcpy*(dest: cstring, source: cstring, len: zpl_isize): zpl_isize {.implibrgC.}
proc zpl_strrev*(str: cstring): cstring {.implibrgC.}

#  NOTE: ASCII only
proc zpl_strtok*(output: cstring, src: cstring, delimit: cstring): cstring {.implibrgC.}

#  NOTE: This edits *source* string.
#  Returns: zpl_array
proc zpl_str_split_lines*(alloc: zpl_allocator, source: cstring, strip_whitespace: zpl_b32): ptr cstring {.implibrgC.}
proc zpl_str_has_prefix*(str: cstring, prefix: cstring): zpl_b32 {.implibrgC.}
proc zpl_str_has_suffix*(str: cstring, suffix: cstring): zpl_b32 {.implibrgC.}
proc zpl_char_first_occurence*(str: cstring, c: cchar): cstring {.implibrgC.}
proc zpl_char_last_occurence*(str: cstring, c: cchar): cstring {.implibrgC.}
proc zpl_str_concat*(dest: cstring, dest_len: zpl_isize, src_a: cstring, src_a_len: zpl_isize, src_b: cstring, src_b_len: zpl_isize) {.implibrgC.}
proc zpl_str_to_u64*(str: cstring, end_ptr: ptr cstring, base: zpl_i32): zpl_u64 {.implibrgC.}

#  TODO: Support more than just decimal and hexadecimal
proc zpl_str_to_i64*(str: cstring, end_ptr: ptr cstring, base: zpl_i32): zpl_i64 {.implibrgC.}

#  TODO: Support more than just decimal and hexadecimal
proc zpl_str_to_f32*(str: cstring, end_ptr: ptr cstring): zpl_f32 {.implibrgC.}
proc zpl_str_to_f64*(str: cstring, end_ptr: ptr cstring): zpl_f64 {.implibrgC.}
proc zpl_i64_to_str*(value: zpl_i64, string: cstring, base: zpl_i32) {.implibrgC.}
proc zpl_u64_to_str*(value: zpl_u64, string: cstring, base: zpl_i32) {.implibrgC.}

# 
# 
#  UTF-8 Handling
# 
# 
#  NOTE: Does not check if utf-8 string is valid
proc zpl_utf8_strlen*(str: ptr zpl_u8): zpl_isize {.implibrgC.}
proc zpl_utf8_strnlen*(str: ptr zpl_u8, max_len: zpl_isize): zpl_isize {.implibrgC.}

#  NOTE: Windows doesn't handle 8 bit filenames well
proc zpl_utf8_to_ucs2*(buffer: ptr zpl_u16, len: zpl_isize, str: ptr zpl_u8): ptr zpl_u16 {.implibrgC.}
proc zpl_ucs2_to_utf8*(buffer: ptr zpl_u8, len: zpl_isize, str: ptr zpl_u16): ptr zpl_u8 {.implibrgC.}
proc zpl_utf8_to_ucs2_buf*(str: ptr zpl_u8): ptr zpl_u16 {.implibrgC.}

#  NOTE: Uses locally persisting buffer
proc zpl_ucs2_to_utf8_buf*(str: ptr zpl_u16): ptr zpl_u8 {.implibrgC.}

#  NOTE: Uses locally persisting buffer
#  NOTE: Returns size of codepoint in bytes
proc zpl_utf8_decode*(str: ptr zpl_u8, str_len: zpl_isize, codepoint: ptr zpl_rune): zpl_isize {.implibrgC.}
proc zpl_utf8_codepoint_size*(str: ptr zpl_u8, str_len: zpl_isize): zpl_isize {.implibrgC.}
proc zpl_utf8_encode_rune*(buf: array[4, zpl_u8], r: zpl_rune): zpl_isize {.implibrgC.}
proc zpl_string_make_reserve*(a: zpl_allocator, capacity: zpl_isize): zpl_string {.implibrgC.}
proc zpl_string_make*(a: zpl_allocator, str: cstring): zpl_string {.implibrgC.}
proc zpl_string_make_length*(a: zpl_allocator, str: pointer, num_bytes: zpl_isize): zpl_string {.implibrgC.}
proc zpl_string_sprintf*(a: zpl_allocator, buf: cstring, num_bytes: zpl_isize, fmt: cstring): zpl_string {.implibrgC.}
proc zpl_string_sprintf_buf*(a: zpl_allocator, fmt: cstring): zpl_string {.implibrgC.}

#  NOTE: Uses locally persistent buffer
proc zpl_string_free*(str: zpl_string) {.implibrgC.}
proc zpl_string_duplicate*(a: zpl_allocator, str: zpl_string): zpl_string {.implibrgC.}
proc zpl_string_length*(str: zpl_string): zpl_isize {.implibrgC.}
proc zpl_string_capacity*(str: zpl_string): zpl_isize {.implibrgC.}
proc zpl_string_available_space*(str: zpl_string): zpl_isize {.implibrgC.}
proc zpl_string_clear*(str: zpl_string) {.implibrgC.}
proc zpl_string_append*(str: zpl_string, other: zpl_string): zpl_string {.implibrgC.}
proc zpl_string_append_length*(str: zpl_string, other: pointer, num_bytes: zpl_isize): zpl_string {.implibrgC.}
proc zpl_string_appendc*(str: zpl_string, other: cstring): zpl_string {.implibrgC.}
proc zpl_string_join*(a: zpl_allocator, parts: ptr cstring, count: zpl_isize, glue: cstring): zpl_string {.implibrgC.}
proc zpl_string_set*(str: zpl_string, cstr: cstring): zpl_string {.implibrgC.}
proc zpl_string_make_space_for*(str: zpl_string, add_len: zpl_isize): zpl_string {.implibrgC.}
proc zpl_string_allocation_size*(str: zpl_string): zpl_isize {.implibrgC.}
proc zpl_string_are_equal*(lhs: zpl_string, rhs: zpl_string): zpl_b32 {.implibrgC.}
proc zpl_string_trim*(str: zpl_string, cut_set: cstring): zpl_string {.implibrgC.}
proc zpl_string_trim_space*(str: zpl_string): zpl_string {.implibrgC.}

#  Whitespace ` \t\r\n\v\f`
proc zpl_string_append_rune*(str: zpl_string, r: zpl_rune): zpl_string {.implibrgC.}
proc zpl_string_append_fmt*(str: zpl_string, fmt: cstring): zpl_string {.implibrgC.}

# ! Compile regex pattern.
proc zpl_re_compile*(re: ptr zpl_re, backing: zpl_allocator, pattern: cstring, pattern_len: zpl_isize): zpl_regex_error {.implibrgC.}

# ! Compile regex pattern using a buffer.
proc zpl_re_compile_from_buffer*(re: ptr zpl_re, pattern: cstring, pattern_len: zpl_isize, buffer: pointer, buffer_len: zpl_isize): zpl_regex_error {.implibrgC.}

# ! Destroy regex object.
proc zpl_re_destroy*(re: ptr zpl_re) {.implibrgC.}

# ! Retrieve number of retrievable captures.
proc zpl_re_capture_count*(re: ptr zpl_re): zpl_isize {.implibrgC.}

# ! Match input string and output captures of the occurence.
proc zpl_re_match*(re: ptr zpl_re, str: cstring, str_len: zpl_isize, captures: ptr zpl_re_capture, max_capture_count: zpl_isize, offset: ptr zpl_isize): zpl_b32 {.implibrgC.}

# ! Match all occurences in an input string and output them into captures. Array of captures is allocated on the heap and needs to be freed afterwards.
proc zpl_re_match_all*(re: ptr zpl_re, str: cstring, str_len: zpl_isize, max_capture_count: zpl_isize, out_captures: ptr ptr zpl_re_capture): zpl_b32 {.implibrgC.}
proc zpl_list_init*(list: ptr zpl_list, `ptr`: pointer) {.implibrgC.}
proc zpl_list_add*(list: ptr zpl_list, item: ptr zpl_list): ptr zpl_list {.implibrgC.}

#  NOTE(zaklaus): Returns a pointer to the next node (or NULL if the removed node has no trailing node.)
proc zpl_list_remove*(list: ptr zpl_list): ptr zpl_list {.implibrgC.}

#  NOTE: Give it an initial default capacity
#  NOTE: Do not use the thing below directly, use the macro
proc zpl_array_set_capacity*(array: pointer, capacity: zpl_isize, element_size: zpl_isize): pointer {.importc: "zpl__array_set_capacity", cdecl, dynlib: dynlibFile.}

# 
# 
#  Instantiated Circular buffer
# 
# 
# int main()
# {
#     zpl_ring_zpl_u32 pad={0};
#     zpl_ring_zpl_u32_init(&pad, zpl_heap(), 3);
#     zpl_ring_zpl_u32_append(&pad, 1);
#     zpl_ring_zpl_u32_append(&pad, 2);
#     zpl_ring_zpl_u32_append(&pad, 3);
#     while (!zpl_ring_zpl_u32_empty(&pad)) {
#         zpl_printf("Result is %d\n", *zpl_ring_zpl_u32_get(&pad));
#     }
#     zpl_ring_zpl_u32_free(&pad);
#     return 0;
# }
# 
# ! @}
# * @file hashing.c
# @brief Hashing and Checksum Functions
# @defgroup hashing Hashing and Checksum Functions
# Several hashing methods used by ZPL internally but possibly useful outside of it. Contains: adler32, crc32/64, fnv32/64/a and murmur32/64
# @{
# 
proc zpl_adler32*(data: pointer, len: zpl_isize): zpl_u32 {.implibrgC.}
proc zpl_crc32*(data: pointer, len: zpl_isize): zpl_u32 {.implibrgC.}
proc zpl_crc64*(data: pointer, len: zpl_isize): zpl_u64 {.implibrgC.}
proc zpl_fnv32*(data: pointer, len: zpl_isize): zpl_u32 {.implibrgC.}
proc zpl_fnv64*(data: pointer, len: zpl_isize): zpl_u64 {.implibrgC.}
proc zpl_fnv32a*(data: pointer, len: zpl_isize): zpl_u32 {.implibrgC.}
proc zpl_fnv64a*(data: pointer, len: zpl_isize): zpl_u64 {.implibrgC.}
proc zpl_base64_encode*(a: zpl_allocator, data: pointer, len: zpl_isize): ptr zpl_u8 {.implibrgC.}
proc zpl_base64_decode*(a: zpl_allocator, data: pointer, len: zpl_isize): ptr zpl_u8 {.implibrgC.}

# ! Default seed of 0x9747b28c
proc zpl_murmur32*(data: pointer, len: zpl_isize): zpl_u32 {.implibrgC.}

# ! Default seed of 0x9747b28c
proc zpl_murmur64*(data: pointer, len: zpl_isize): zpl_u64 {.implibrgC.}
proc zpl_murmur32_seed*(data: pointer, len: zpl_isize, seed: zpl_u32): zpl_u32 {.implibrgC.}
proc zpl_murmur64_seed*(data: pointer, len: zpl_isize, seed: zpl_u64): zpl_u64 {.implibrgC.}
# Declaration 'zpl_default_file_operations' skipped
proc zpl_file_get_standard*(std: zpl_file_standard_type): ptr zpl_file {.implibrgC.}
proc zpl_file_connect_handle*(file: ptr zpl_file, handle: pointer) {.implibrgC.}
proc zpl_file_create*(file: ptr zpl_file, filename: cstring): zpl_file_error {.implibrgC.}
proc zpl_file_open*(file: ptr zpl_file, filename: cstring): zpl_file_error {.implibrgC.}
proc zpl_file_open_mode*(file: ptr zpl_file, mode: zpl_file_mode, filename: cstring): zpl_file_error {.implibrgC.}
proc zpl_file_new*(file: ptr zpl_file, fd: zpl_file_descriptor, ops: zpl_file_operations, filename: cstring): zpl_file_error {.implibrgC.}
proc zpl_file_read_at_check*(file: ptr zpl_file, buffer: pointer, size: zpl_isize, offset: zpl_i64, bytes_read: ptr zpl_isize): zpl_b32 {.implibrgC.}
proc zpl_file_write_at_check*(file: ptr zpl_file, buffer: pointer, size: zpl_isize, offset: zpl_i64, bytes_written: ptr zpl_isize): zpl_b32 {.implibrgC.}
proc zpl_file_read_at*(file: ptr zpl_file, buffer: pointer, size: zpl_isize, offset: zpl_i64): zpl_b32 {.implibrgC.}
proc zpl_file_write_at*(file: ptr zpl_file, buffer: pointer, size: zpl_isize, offset: zpl_i64): zpl_b32 {.implibrgC.}
proc zpl_file_seek*(file: ptr zpl_file, offset: zpl_i64): zpl_i64 {.implibrgC.}
proc zpl_file_seek_to_end*(file: ptr zpl_file): zpl_i64 {.implibrgC.}
proc zpl_file_skip*(file: ptr zpl_file, bytes: zpl_i64): zpl_i64 {.implibrgC.}

#  NOTE: Skips a certain amount of bytes
proc zpl_file_tell*(file: ptr zpl_file): zpl_i64 {.implibrgC.}
proc zpl_file_close*(file: ptr zpl_file): zpl_file_error {.implibrgC.}
proc zpl_file_read*(file: ptr zpl_file, buffer: pointer, size: zpl_isize): zpl_b32 {.implibrgC.}
proc zpl_file_write*(file: ptr zpl_file, buffer: pointer, size: zpl_isize): zpl_b32 {.implibrgC.}
proc zpl_file_size*(file: ptr zpl_file): zpl_i64 {.implibrgC.}
proc zpl_file_name*(file: ptr zpl_file): cstring {.implibrgC.}
proc zpl_file_truncate*(file: ptr zpl_file, size: zpl_i64): zpl_file_error {.implibrgC.}

#  NOTE: Changed since lasted checked
proc zpl_file_has_changed*(file: ptr zpl_file): zpl_b32 {.implibrgC.}

# ! Refresh dirinfo of specified file
proc zpl_file_dirinfo_refresh*(file: ptr zpl_file) {.implibrgC.}
proc zpl_file_temp*(file: ptr zpl_file): zpl_file_error {.implibrgC.}
proc zpl_file_read_contents*(a: zpl_allocator, zero_terminate: zpl_b32, filepath: cstring): zpl_file_contents {.implibrgC.}
proc zpl_file_free_contents*(fc: ptr zpl_file_contents) {.implibrgC.}

# ! Make sure you free both the returned buffer and the lines (zpl_array)
# Declaration 'zpl_file_read_lines' skipped
# Declaration 'alloc' skipped
# Declaration 'zpl_file_read_lines' skipped
# Declaration 'alloc' skipped
# Declaration 'lines' skipped
# Declaration 'filename' skipped
# Declaration 'strip_whitespace' skipped
proc zpl_fs_exists*(filepath: cstring): zpl_b32 {.implibrgC.}
proc zpl_fs_get_type*(path: cstring): zpl_u8 {.implibrgC.}
proc zpl_fs_last_write_time*(filepath: cstring): zpl_file_time {.implibrgC.}
proc zpl_fs_copy*(existing_filename: cstring, new_filename: cstring, fail_if_exists: zpl_b32): zpl_b32 {.implibrgC.}
proc zpl_fs_move*(existing_filename: cstring, new_filename: cstring): zpl_b32 {.implibrgC.}
proc zpl_fs_remove*(filename: cstring): zpl_b32 {.implibrgC.}
proc zpl_path_is_absolute*(path: cstring): zpl_b32 {.implibrgC.}
proc zpl_path_is_relative*(path: cstring): zpl_b32 {.implibrgC.}
proc zpl_path_is_root*(path: cstring): zpl_b32 {.implibrgC.}
proc zpl_path_fix_slashes*(path: cstring) {.implibrgC.}
proc zpl_path_base_name*(path: cstring): cstring {.implibrgC.}
proc zpl_path_extension*(path: cstring): cstring {.implibrgC.}
proc zpl_path_get_full_name*(a: zpl_allocator, path: cstring): cstring {.implibrgC.}
proc zpl_path_mkdir*(path: cstring, mode: zpl_i32): zpl_file_error {.implibrgC.}
proc zpl_path_rmdir*(path: cstring): zpl_file_error {.implibrgC.}

# ! Returns file paths terminated by newline (\n)
proc zpl_path_dirlist*(alloc: zpl_allocator, dirname: cstring, recurse: zpl_b32): zpl_string {.implibrgC.}

# ! Initialize dirinfo from specified path
proc zpl_dirinfo_init*(dir: ptr zpl_dir_info, path: cstring) {.implibrgC.}
proc zpl_dirinfo_free*(dir: ptr zpl_dir_info) {.implibrgC.}

# ! Analyze the entry's dirinfo
proc zpl_dirinfo_step*(dir_entry: ptr zpl_dir_entry) {.implibrgC.}

# ! @}
# * @file print.c
# @brief Printing methods
# @defgroup print Printing methods
# Various printing methods.
# @{
# 
proc zpl_printf*(fmt: cstring): zpl_isize {.implibrgC.}
proc zpl_printf_va*(fmt: cstring, va: types.va_list): zpl_isize {.implibrgC.}
proc zpl_printf_err*(fmt: cstring): zpl_isize {.implibrgC.}
proc zpl_printf_err_va*(fmt: cstring, va: types.va_list): zpl_isize {.implibrgC.}
proc zpl_fprintf*(f: ptr zpl_file, fmt: cstring): zpl_isize {.implibrgC.}
proc zpl_fprintf_va*(f: ptr zpl_file, fmt: cstring, va: types.va_list): zpl_isize {.implibrgC.}

#  NOTE: A locally persisting buffer is used internally
proc zpl_bprintf*(fmt: cstring): cstring {.implibrgC.}

#  NOTE: A locally persisting buffer is used internally
proc zpl_bprintf_va*(fmt: cstring, va: types.va_list): cstring {.implibrgC.}
proc zpl_snprintf*(str: cstring, n: zpl_isize, fmt: cstring): zpl_isize {.implibrgC.}
proc zpl_snprintf_va*(str: cstring, n: zpl_isize, fmt: cstring, va: types.va_list): zpl_isize {.implibrgC.}
proc zpl_dll_load*(filepath: cstring): zpl_dll_handle {.implibrgC.}
proc zpl_dll_unload*(dll: zpl_dll_handle) {.implibrgC.}
proc zpl_dll_proc_address*(dll: zpl_dll_handle, proc_name: cstring): zpl_dll_proc {.implibrgC.}

# ! @}
# * @file time.c
# @brief Time helper methods.
# @defgroup time Time helpers
#  Helper methods for retrieving the current time in many forms under different precisions. It also offers a simple to use timer library.
#  @{
# 
# ! Return CPU timestamp.
proc zpl_rdtsc*(): zpl_u64 {.implibrgC.}

# ! Return relative time since the application start.
proc zpl_time_now*(): zpl_f64 {.implibrgC.}

# ! Return time since 1601-01-01 UTC.
proc zpl_utc_time_now*(): zpl_f64 {.implibrgC.}

# ! Sleep for specified number of milliseconds.
proc zpl_sleep_ms*(ms: zpl_u32) {.implibrgC.}

# /< zpl_array
# ! Initialize timer pool.
# ! Add new timer to pool and return it.
proc zpl_timer_add*(pool: zpl_timer_pool): ptr zpl_timer {.implibrgC.}

# ! Perform timer pool update.
# ! Traverse over all timers and update them accordingly. Should be called by Main Thread in a tight loop.
proc zpl_timer_update*(pool: zpl_timer_pool) {.implibrgC.}

# ! Set up timer.
# ! Set up timer with specific options.
# ! @param timer
# ! @param duration How long/often to fire a timer.
# ! @param count How many times we fire a timer. Use -1 for infinity.
# ! @param callback A method to execute once a timer triggers.
proc zpl_timer_set*(timer: ptr zpl_timer, duration: zpl_f64, count: zpl_i32, callback: zpl_timer_cb) {.implibrgC.}

# ! Start timer with specified delay.
proc zpl_timer_start*(timer: ptr zpl_timer, delay_start: zpl_f64) {.implibrgC.}

# ! Stop timer and prevent it from triggering.
proc zpl_timer_stop*(timer: ptr zpl_timer) {.implibrgC.}
proc zpl_event_pool_init*(h: ptr zpl_event_pool, a: zpl_allocator) {.implibrgC.}
proc zpl_event_pool_destroy*(h: ptr zpl_event_pool) {.implibrgC.}
proc zpl_event_pool_get*(h: ptr zpl_event_pool, key: zpl_u64): ptr zpl_event_block {.implibrgC.}
proc zpl_event_pool_set*(h: ptr zpl_event_pool, key: zpl_u64, value: zpl_event_block) {.implibrgC.}
proc zpl_event_pool_grow*(h: ptr zpl_event_pool) {.implibrgC.}
proc zpl_event_pool_rehash*(h: ptr zpl_event_pool, new_count: zpl_isize) {.implibrgC.}
proc zpl_event_pool_remove*(h: ptr zpl_event_pool, key: zpl_u64) {.implibrgC.}
proc zpl_event_init*(pool: ptr zpl_event_pool, alloc: zpl_allocator) {.implibrgC.}
proc zpl_event_destroy*(pool: ptr zpl_event_pool) {.implibrgC.}
proc zpl_event_add*(pool: ptr zpl_event_pool, slot: zpl_u64, cb: zpl_event_cb): zpl_u64 {.implibrgC.}
proc zpl_event_remove*(pool: ptr zpl_event_pool, slot: zpl_u64, index: zpl_u64) {.implibrgC.}
proc zpl_event_trigger*(pool: ptr zpl_event_pool, slot: zpl_u64, evt: zpl_event_data) {.implibrgC.}

#  NOTE: Generates from numerous sources to produce a decent pseudo-random seed
proc zpl_random_init*(r: ptr zpl_random) {.implibrgC.}
proc zpl_random_gen_u32*(r: ptr zpl_random): zpl_u32 {.implibrgC.}
proc zpl_random_gen_u32_unique*(r: ptr zpl_random): zpl_u32 {.implibrgC.}
proc zpl_random_gen_u64*(r: ptr zpl_random): zpl_u64 {.implibrgC.}

#  NOTE: (zpl_random_gen_u32() << 32) | zpl_random_gen_u32()
proc zpl_random_gen_isize*(r: ptr zpl_random): zpl_isize {.implibrgC.}
proc zpl_random_range_i64*(r: ptr zpl_random, lower_inc: zpl_i64, higher_inc: zpl_i64): zpl_i64 {.implibrgC.}
proc zpl_random_range_isize*(r: ptr zpl_random, lower_inc: zpl_isize, higher_inc: zpl_isize): zpl_isize {.implibrgC.}
proc zpl_random_range_f64*(r: ptr zpl_random, lower_inc: zpl_f64, higher_inc: zpl_f64): zpl_f64 {.implibrgC.}
proc zpl_exit*(code: zpl_u32) {.implibrgC.}
proc zpl_yield*() {.implibrgC.}

# ! Returns allocated buffer
proc zpl_get_env*(name: cstring): cstring {.implibrgC.}
proc zpl_get_env_buf*(name: cstring): cstring {.implibrgC.}
proc zpl_get_env_str*(name: cstring): zpl_string {.implibrgC.}
proc zpl_set_env*(name: cstring, value: cstring) {.implibrgC.}
proc zpl_unset_env*(name: cstring) {.implibrgC.}
proc zpl_endian_swap16*(i: zpl_u16): zpl_u16 {.implibrgC.}
proc zpl_endian_swap32*(i: zpl_u32): zpl_u32 {.implibrgC.}
proc zpl_endian_swap64*(i: zpl_u64): zpl_u64 {.implibrgC.}
proc zpl_count_set_bits*(mask: zpl_u64): zpl_isize {.implibrgC.}
proc zpl_system_command*(command: cstring, buffer_len: zpl_usize, buffer: cstring): zpl_u32 {.implibrgC.}
proc zpl_system_command_str*(command: cstring, backing: zpl_allocator): zpl_string {.implibrgC.}

# ! Parses JSON5/SJSON text.
# ! This method takes text form of JSON document as a source and parses its contents into JSON object structure we can work with. It also optionally handles comments that usually appear in documents used for configuration.
# ! @param root JSON object we store data to.
# ! @param len Text length. (reserved)
# ! @param source Text to be processed.
# ! @param allocator Memory allocator to use. (ex. zpl_heap())
# ! @param handle_comments Whether to handle possible comments or not. Note that if we won't handle comments in a document containing them, the parser will error out. See remark in source code.
# ! @param err_code Variable we will store error code in.
proc zpl_json_parse*(root: ptr zpl_json_object, len: zpl_usize, source: cstring, allocator: zpl_allocator, handle_comments: zpl_b32, err_code: ptr zpl_u8) {.implibrgC.}

# ! Exports JSON5 document into text form and outputs it into a file.
# ! This method takes JSON object tree and exports it into valid JSON5 form with the support of various styles that were preserved during import or set up programatically.
# ! @param file File we write text to.
# ! @param obj JSON object we export data from.
# ! @param indent Text indentation used during export. Use 0 for root objects.
proc zpl_json_write*(file: ptr zpl_file, obj: ptr zpl_json_object, indent: zpl_isize) {.implibrgC.}

# ! Releases used resources by a JSON object.
# ! @param obj JSON object to free.
proc zpl_json_free*(obj: ptr zpl_json_object) {.implibrgC.}

# ! Searches for a JSON node within a document by its name.
# ! @param obj JSON object to search in.
# ! @param name JSON node's name to search for.
# ! @param deep_search Perform the search recursively.
proc zpl_json_find*(obj: ptr zpl_json_object, name: cstring, deep_search: zpl_b32): ptr zpl_json_object {.implibrgC.}

# ! Initializes a JSON node.
# ! @param obj JSON node to initialize.
# ! @param backing Memory allocator to use (ex. zpl_heap())
# ! @param name JSON node's name.
# ! @param type JSON node's type. (See zpl_json_type)
# ! @see zpl_json_type
proc zpl_json_init_node*(obj: ptr zpl_json_object, backing: zpl_allocator, name: cstring, `type`: zpl_u8) {.implibrgC.}

# ! Adds object into JSON document at a specific index.
# ! Initializes and adds a JSON object into a JSON document at a specific index.
# ! @param obj Root node to add to.
# ! @param index Index to store at.
# ! @param name JSON node's name.
# ! @param type JSON node's type. (See zpl_json_type)
# ! @see zpl_json_type
proc zpl_json_add_at*(obj: ptr zpl_json_object, index: zpl_isize, name: cstring, `type`: zpl_u8): ptr zpl_json_object {.implibrgC.}

# ! Appends object into JSON document.
# ! Initializes and appends a JSON object into a JSON document.
# ! @param obj Root node to add to.
# ! @param name JSON node's name.
# ! @param type JSON node's type. (See zpl_json_type)
# ! @see zpl_json_type
proc zpl_json_add*(obj: ptr zpl_json_object, name: cstring, `type`: zpl_u8): ptr zpl_json_object {.implibrgC.}
proc zpl_json_parse_object*(obj: ptr zpl_json_object, base: cstring, a: zpl_allocator, err_code: ptr zpl_u8): cstring {.importc: "zpl__json_parse_object", cdecl, dynlib: dynlibFile.}
proc zpl_json_parse_value*(obj: ptr zpl_json_object, base: cstring, a: zpl_allocator, err_code: ptr zpl_u8): cstring {.importc: "zpl__json_parse_value", cdecl, dynlib: dynlibFile.}
proc zpl_json_parse_array*(obj: ptr zpl_json_object, base: cstring, a: zpl_allocator, err_code: ptr zpl_u8): cstring {.importc: "zpl__json_parse_array", cdecl, dynlib: dynlibFile.}
proc zpl_json_skip*(str: cstring, c: cchar): cstring {.importc: "zpl__json_skip", cdecl, dynlib: dynlibFile.}
proc zpl_json_validate_name*(str: cstring, err: cstring): zpl_b32 {.importc: "zpl__json_validate_name", cdecl, dynlib: dynlibFile.}

# ! Initializes options parser.
# ! Initializes CLI options parser using specified memory allocator and provided application name.
# ! @param opts Options parser to initialize.
# ! @param allocator Memory allocator to use. (ex. zpl_heap())
# ! @param app Application name displayed in help screen.
proc zpl_opts_init*(opts: ptr zpl_opts, allocator: zpl_allocator, app: cstring) {.implibrgC.}

# ! Releases the resources used by options parser.
proc zpl_opts_free*(opts: ptr zpl_opts) {.implibrgC.}

# ! Registers an option.
# ! Registers an option with its short and long name, specifies option's type and its description.
# ! @param opts Options parser to add to.
# ! @param lname Shorter name of option. (ex. "f")
# ! @param name Full name of option. (ex. "foo") Note that rest of the module uses longer names to manipulate opts.
# ! @param desc Description shown in the help screen.
# ! @param type Option's type (see zpl_opts_types)
# ! @see zpl_opts_types
proc zpl_opts_add*(opts: ptr zpl_opts, name: cstring, lname: cstring, desc: cstring, `type`: zpl_u8) {.implibrgC.}

# ! Registers option as positional.
# ! Registers added option as positional, so that we can pass it anonymously. Arguments are expected on the command input in the same order they were registered as.
# ! @param opts
# ! @param name Name of already registered option.
proc zpl_opts_positional_add*(opts: ptr zpl_opts, name: cstring) {.implibrgC.}

# ! Compiles CLI arguments.
#  This method takes CLI arguments as input and processes them based on rules that were set up.
# ! @param opts
# ! @param argc Argument count in an array.
# ! @param argv Array of arguments.
proc zpl_opts_compile*(opts: ptr zpl_opts, argc: cint, argv: ptr cstring): zpl_b32 {.implibrgC.}

# ! Prints out help screen.
# ! Prints out help screen with example usage of application as well as with all the flags available.
proc zpl_opts_print_help*(opts: ptr zpl_opts) {.implibrgC.}

# ! Prints out parsing errors.
# ! Prints out possible errors caused by CLI input.
proc zpl_opts_print_errors*(opts: ptr zpl_opts) {.implibrgC.}

# ! Fetches a string from an option.
# ! @param opts
# ! @param name Name of an option.
# ! @param fallback Fallback string we return if option wasn't found.
proc zpl_opts_string*(opts: ptr zpl_opts, name: cstring, fallback: cstring): zpl_string {.implibrgC.}

# ! Fetches a real number from an option.
# ! @param opts
# ! @param name Name of an option.
# ! @param fallback Fallback real number we return if option was not found.
proc zpl_opts_real*(opts: ptr zpl_opts, name: cstring, fallback: zpl_f64): zpl_f64 {.implibrgC.}

# ! Fetches an integer number from an option.
# ! @param opts
# ! @param name Name of an option.
# ! @param fallback Fallback integer number we return if option was not found.
proc zpl_opts_integer*(opts: ptr zpl_opts, name: cstring, fallback: zpl_i64): zpl_i64 {.implibrgC.}

# ! Checks whether an option was used.
# ! @param opts
# ! @param name Name of an option.
proc zpl_opts_has_arg*(opts: ptr zpl_opts, name: cstring): zpl_b32 {.implibrgC.}

# ! Checks whether all positionals have been passed in.
proc zpl_opts_positionals_filled*(opts: ptr zpl_opts): zpl_b32 {.implibrgC.}
proc zpl_pr_create*(process: ptr zpl_pr, args: ptr cstring, argc: zpl_isize, si: zpl_pr_si, options: zpl_pr_opts): zpl_i32 {.implibrgC.}
proc zpl_pr_destroy*(process: ptr zpl_pr) {.implibrgC.}
proc zpl_pr_terminate*(process: ptr zpl_pr, err_code: zpl_i32) {.implibrgC.}
proc zpl_pr_join*(process: ptr zpl_pr): zpl_i32 {.implibrgC.}

# ! Initialize thread pool with specified amount of fixed threads.
proc zpl_jobs_init*(pool: ptr zpl_thread_pool, a: zpl_allocator, max_threads: zpl_u32) {.implibrgC.}

# ! Release the resources use by thread pool.
proc zpl_jobs_free*(pool: ptr zpl_thread_pool) {.implibrgC.}

# ! Enqueue a job with specified data.
proc zpl_jobs_enqueue*(pool: ptr zpl_thread_pool, `proc`: zpl_jobs_proc, data: pointer) {.implibrgC.}

# ! Enqueue a job with specific priority with specified data.
proc zpl_jobs_enqueue_with_priority*(pool: ptr zpl_thread_pool, `proc`: zpl_jobs_proc, data: pointer, priority: zpl_i32) {.implibrgC.}

# ! Process all jobs and check all threads. Should be called by Main Thread in a tight loop.
proc zpl_jobs_process*(pool: ptr zpl_thread_pool): zpl_b32 {.implibrgC.}

#  These methods are used to initialize the co-routine subsystem
# *
#  * Initializes the coroutines subsystem
#  * @param  a           Memory allocator to be used
#  * @param  max_threads Maximum amount of threads to use for coroutines execution
# 
proc zpl_co_init*(a: zpl_allocator, max_threads: zpl_u32) {.implibrgC.}

# *
#  * Destroys the coroutines subsystem
#  *
#  * IMPORTANT: This is a blocking method that waits until all the coroutines are finished.
#  * Please, make sure your coroutines are correctly designed, so that you
#  * won't end up in an infinite loop.
# 
proc zpl_co_destroy*() {.implibrgC.}

#  These methods are used by the host to create and run/resume co-routines
#  Make sure the co-routine subsystem is initialized first!
# *
#  * Create a paused coroutine
#  * @param  f Coroutine method
#  * @return   Paused coroutine
# 
proc zpl_co_make*(f: zpl_co_proc): zpl_co {.implibrgC.}

# *
#  * Starts/Resumes a coroutine execution.
#  *
#  * IMPORTANT: Data you pass is stored in a stack of up to ZPL_CO_ARG_STACK_CAPACITY.
#  * This means that you can cause stack corruption if you
#  * call 'zpl_co_resume' with data passed (ZPL_CO_ARG_STACK_CAPACITY+1) times.
#  * Raise the number by defining ZPL_CO_ARG_STACK_CAPACITY if required.
#  *
#  * @param  co   Coroutine
#  * @param  data Data we want to pass (or NULL)
# 
proc zpl_co_resume*(co: ptr zpl_co, data: pointer) {.implibrgC.}

# *
#  * Is a coroutine running at the moment?
#  * @param  co Coroutine
#  * @return
# 
proc zpl_co_running*(co: ptr zpl_co): zpl_b32 {.implibrgC.}

# *
#  * Is a coroutine already finished?
#  * @param  co Coroutine
#  * @return
# 
proc zpl_co_finished*(co: ptr zpl_co): zpl_b32 {.implibrgC.}

# *
#  * Is coroutine waiting? (in yield state)
#  * @param  co Coroutine
#  * @return
# 
proc zpl_co_waiting*(co: ptr zpl_co): zpl_b32 {.implibrgC.}

#  This method is used by the co-routine to await execution
# 
# *
#  * Yield the coroutine.
#  *
#  * IMPORTANT: Only to be used by the coroutine!!
#  * @param  co Coroutine
# 
proc zpl_co_yield*(co: ptr zpl_co) {.implibrgC.}
proc zpl_to_radians*(degrees: zpl_f32): zpl_f32 {.implibrgC.}
proc zpl_to_degrees*(radians: zpl_f32): zpl_f32 {.implibrgC.}

#  NOTE: Because to interpolate angles
proc zpl_angle_diff*(radians_a: zpl_f32, radians_b: zpl_f32): zpl_f32 {.implibrgC.}
proc zpl_copy_sign*(x: zpl_f32, y: zpl_f32): zpl_f32 {.implibrgC.}
proc zpl_remainder*(x: zpl_f32, y: zpl_f32): zpl_f32 {.implibrgC.}
proc zpl_mod*(x: zpl_f32, y: zpl_f32): zpl_f32 {.implibrgC.}
proc zpl_copy_sign64*(x: zpl_f64, y: zpl_f64): zpl_f64 {.implibrgC.}
proc zpl_floor64*(x: zpl_f64): zpl_f64 {.implibrgC.}
proc zpl_ceil64*(x: zpl_f64): zpl_f64 {.implibrgC.}
proc zpl_round64*(x: zpl_f64): zpl_f64 {.implibrgC.}
proc zpl_remainder64*(x: zpl_f64, y: zpl_f64): zpl_f64 {.implibrgC.}
proc zpl_abs64*(x: zpl_f64): zpl_f64 {.implibrgC.}
proc zpl_sign64*(x: zpl_f64): zpl_f64 {.implibrgC.}
proc zpl_mod64*(x: zpl_f64, y: zpl_f64): zpl_f64 {.implibrgC.}
proc zpl_sqrt*(a: zpl_f32): zpl_f32 {.implibrgC.}
proc zpl_rsqrt*(a: zpl_f32): zpl_f32 {.implibrgC.}
proc zpl_quake_rsqrt*(a: zpl_f32): zpl_f32 {.implibrgC.}

#  NOTE: It's probably better to use 1.0f/zpl_sqrt(a)
#                                      * And for simd, there is usually isqrt functions too!
# 
proc zpl_sin*(radians: zpl_f32): zpl_f32 {.implibrgC.}
proc zpl_cos*(radians: zpl_f32): zpl_f32 {.implibrgC.}
proc zpl_tan*(radians: zpl_f32): zpl_f32 {.implibrgC.}
proc zpl_arcsin*(a: zpl_f32): zpl_f32 {.implibrgC.}
proc zpl_arccos*(a: zpl_f32): zpl_f32 {.implibrgC.}
proc zpl_arctan*(a: zpl_f32): zpl_f32 {.implibrgC.}
proc zpl_arctan2*(y: zpl_f32, x: zpl_f32): zpl_f32 {.implibrgC.}
proc zpl_exp*(x: zpl_f32): zpl_f32 {.implibrgC.}
proc zpl_exp2*(x: zpl_f32): zpl_f32 {.implibrgC.}
proc zpl_log*(x: zpl_f32): zpl_f32 {.implibrgC.}
proc zpl_log2*(x: zpl_f32): zpl_f32 {.implibrgC.}
proc zpl_fast_exp*(x: zpl_f32): zpl_f32 {.implibrgC.}

#  NOTE: Only valid from -1 <= x <= +1
proc zpl_fast_exp2*(x: zpl_f32): zpl_f32 {.implibrgC.}

#  NOTE: Only valid from -1 <= x <= +1
proc zpl_pow*(x: zpl_f32, y: zpl_f32): zpl_f32 {.implibrgC.}

#  x^y
proc zpl_round*(x: zpl_f32): zpl_f32 {.implibrgC.}
proc zpl_floor*(x: zpl_f32): zpl_f32 {.implibrgC.}
proc zpl_ceil*(x: zpl_f32): zpl_f32 {.implibrgC.}
proc zpl_half_to_float*(value: zpl_half): zpl_f32 {.implibrgC.}
proc zpl_float_to_half*(value: zpl_f32): zpl_half {.implibrgC.}
proc zpl_vec2f_zero*(): zpl_vec2 {.implibrgC.}
proc zpl_vec2f*(x: zpl_f32, y: zpl_f32): zpl_vec2 {.implibrgC.}
proc zpl_vec2fv*(x: array[2, zpl_f32]): zpl_vec2 {.implibrgC.}
proc zpl_vec3f_zero*(): zpl_vec3 {.implibrgC.}
proc zpl_vec3f*(x: zpl_f32, y: zpl_f32, z: zpl_f32): zpl_vec3 {.implibrgC.}
proc zpl_vec3fv*(x: array[3, zpl_f32]): zpl_vec3 {.implibrgC.}
proc zpl_vec4f_zero*(): zpl_vec4 {.implibrgC.}
proc zpl_vec4f*(x: zpl_f32, y: zpl_f32, z: zpl_f32, w: zpl_f32): zpl_vec4 {.implibrgC.}
proc zpl_vec4fv*(x: array[4, zpl_f32]): zpl_vec4 {.implibrgC.}
proc zpl_vec2_add*(d: ptr zpl_vec2, v0: zpl_vec2, v1: zpl_vec2) {.implibrgC.}
proc zpl_vec2_sub*(d: ptr zpl_vec2, v0: zpl_vec2, v1: zpl_vec2) {.implibrgC.}
proc zpl_vec2_mul*(d: ptr zpl_vec2, v: zpl_vec2, s: zpl_f32) {.implibrgC.}
proc zpl_vec2_div*(d: ptr zpl_vec2, v: zpl_vec2, s: zpl_f32) {.implibrgC.}
proc zpl_vec3_add*(d: ptr zpl_vec3, v0: zpl_vec3, v1: zpl_vec3) {.implibrgC.}
proc zpl_vec3_sub*(d: ptr zpl_vec3, v0: zpl_vec3, v1: zpl_vec3) {.implibrgC.}
proc zpl_vec3_mul*(d: ptr zpl_vec3, v: zpl_vec3, s: zpl_f32) {.implibrgC.}
proc zpl_vec3_div*(d: ptr zpl_vec3, v: zpl_vec3, s: zpl_f32) {.implibrgC.}
proc zpl_vec4_add*(d: ptr zpl_vec4, v0: zpl_vec4, v1: zpl_vec4) {.implibrgC.}
proc zpl_vec4_sub*(d: ptr zpl_vec4, v0: zpl_vec4, v1: zpl_vec4) {.implibrgC.}
proc zpl_vec4_mul*(d: ptr zpl_vec4, v: zpl_vec4, s: zpl_f32) {.implibrgC.}
proc zpl_vec4_div*(d: ptr zpl_vec4, v: zpl_vec4, s: zpl_f32) {.implibrgC.}
proc zpl_vec2_addeq*(d: ptr zpl_vec2, v: zpl_vec2) {.implibrgC.}
proc zpl_vec2_subeq*(d: ptr zpl_vec2, v: zpl_vec2) {.implibrgC.}
proc zpl_vec2_muleq*(d: ptr zpl_vec2, s: zpl_f32) {.implibrgC.}
proc zpl_vec2_diveq*(d: ptr zpl_vec2, s: zpl_f32) {.implibrgC.}
proc zpl_vec3_addeq*(d: ptr zpl_vec3, v: zpl_vec3) {.implibrgC.}
proc zpl_vec3_subeq*(d: ptr zpl_vec3, v: zpl_vec3) {.implibrgC.}
proc zpl_vec3_muleq*(d: ptr zpl_vec3, s: zpl_f32) {.implibrgC.}
proc zpl_vec3_diveq*(d: ptr zpl_vec3, s: zpl_f32) {.implibrgC.}
proc zpl_vec4_addeq*(d: ptr zpl_vec4, v: zpl_vec4) {.implibrgC.}
proc zpl_vec4_subeq*(d: ptr zpl_vec4, v: zpl_vec4) {.implibrgC.}
proc zpl_vec4_muleq*(d: ptr zpl_vec4, s: zpl_f32) {.implibrgC.}
proc zpl_vec4_diveq*(d: ptr zpl_vec4, s: zpl_f32) {.implibrgC.}
proc zpl_vec2_dot*(v0: zpl_vec2, v1: zpl_vec2): zpl_f32 {.implibrgC.}
proc zpl_vec3_dot*(v0: zpl_vec3, v1: zpl_vec3): zpl_f32 {.implibrgC.}
proc zpl_vec4_dot*(v0: zpl_vec4, v1: zpl_vec4): zpl_f32 {.implibrgC.}
proc zpl_vec2_cross*(d: ptr zpl_f32, v0: zpl_vec2, v1: zpl_vec2) {.implibrgC.}
proc zpl_vec3_cross*(d: ptr zpl_vec3, v0: zpl_vec3, v1: zpl_vec3) {.implibrgC.}
proc zpl_vec2_mag2*(v: zpl_vec2): zpl_f32 {.implibrgC.}
proc zpl_vec3_mag2*(v: zpl_vec3): zpl_f32 {.implibrgC.}
proc zpl_vec4_mag2*(v: zpl_vec4): zpl_f32 {.implibrgC.}
proc zpl_vec2_mag*(v: zpl_vec2): zpl_f32 {.implibrgC.}
proc zpl_vec3_mag*(v: zpl_vec3): zpl_f32 {.implibrgC.}
proc zpl_vec4_mag*(v: zpl_vec4): zpl_f32 {.implibrgC.}
proc zpl_vec2_norm*(d: ptr zpl_vec2, v: zpl_vec2) {.implibrgC.}
proc zpl_vec3_norm*(d: ptr zpl_vec3, v: zpl_vec3) {.implibrgC.}
proc zpl_vec4_norm*(d: ptr zpl_vec4, v: zpl_vec4) {.implibrgC.}
proc zpl_vec2_norm0*(d: ptr zpl_vec2, v: zpl_vec2) {.implibrgC.}
proc zpl_vec3_norm0*(d: ptr zpl_vec3, v: zpl_vec3) {.implibrgC.}
proc zpl_vec4_norm0*(d: ptr zpl_vec4, v: zpl_vec4) {.implibrgC.}
proc zpl_vec2_reflect*(d: ptr zpl_vec2, i: zpl_vec2, n: zpl_vec2) {.implibrgC.}
proc zpl_vec3_reflect*(d: ptr zpl_vec3, i: zpl_vec3, n: zpl_vec3) {.implibrgC.}
proc zpl_vec2_refract*(d: ptr zpl_vec2, i: zpl_vec2, n: zpl_vec2, eta: zpl_f32) {.implibrgC.}
proc zpl_vec3_refract*(d: ptr zpl_vec3, i: zpl_vec3, n: zpl_vec3, eta: zpl_f32) {.implibrgC.}
proc zpl_vec2_aspect_ratio*(v: zpl_vec2): zpl_f32 {.implibrgC.}
proc zpl_mat2_identity*(m: ptr zpl_mat2) {.implibrgC.}
# Declaration 'zpl_float22_identity' skipped
# Declaration 'zpl_float22_identity' skipped
# Declaration 'm' skipped
proc zpl_mat2_transpose*(m: ptr zpl_mat2) {.implibrgC.}
proc zpl_mat2_mul*(`out`: ptr zpl_mat2, m1: ptr zpl_mat2, m2: ptr zpl_mat2) {.implibrgC.}
proc zpl_mat2_mul_vec2*(`out`: ptr zpl_vec2, m: ptr zpl_mat2, `in`: zpl_vec2) {.implibrgC.}
proc zpl_mat2_inverse*(`out`: ptr zpl_mat2, `in`: ptr zpl_mat2) {.implibrgC.}
proc zpl_mat2_determinate*(m: ptr zpl_mat2): zpl_f32 {.implibrgC.}
proc zpl_mat2_v*(m: array[2, zpl_vec2]): ptr zpl_mat2 {.implibrgC.}
# Declaration 'zpl_mat2_f' skipped
# Declaration 'zpl_mat2_f' skipped
# Declaration 'm' skipped
proc zpl_float22_m*(m: ptr zpl_mat2): ptr zpl_float2 {.implibrgC.}
proc zpl_float22_v*(m: array[2, zpl_vec2]): ptr zpl_float2 {.implibrgC.}
proc zpl_float22_4*(m: array[4, zpl_f32]): ptr zpl_float2 {.implibrgC.}
# Declaration 'zpl_float22_transpose' skipped
# Declaration 'zpl_f32' skipped
# Declaration 'vec' skipped
# Declaration 'zpl_float22_mul' skipped
# Declaration 'zpl_f32' skipped
# Declaration 'out' skipped
# Declaration 'zpl_f32' skipped
# Declaration 'mat1' skipped
# Declaration 'zpl_f32' skipped
# Declaration 'mat2' skipped
# Declaration 'zpl_float22_mul_vec2' skipped
# Declaration 'out' skipped
# Declaration 'zpl_float22_mul_vec2' skipped
# Declaration 'out' skipped
# Declaration 'm' skipped
# Declaration 'in' skipped
proc zpl_mat3_identity*(m: ptr zpl_mat3) {.implibrgC.}
# Declaration 'zpl_float33_identity' skipped
# Declaration 'zpl_float33_identity' skipped
# Declaration 'm' skipped
proc zpl_mat3_transpose*(m: ptr zpl_mat3) {.implibrgC.}
proc zpl_mat3_mul*(`out`: ptr zpl_mat3, m1: ptr zpl_mat3, m2: ptr zpl_mat3) {.implibrgC.}
proc zpl_mat3_mul_vec3*(`out`: ptr zpl_vec3, m: ptr zpl_mat3, `in`: zpl_vec3) {.implibrgC.}
proc zpl_mat3_inverse*(`out`: ptr zpl_mat3, `in`: ptr zpl_mat3) {.implibrgC.}
proc zpl_mat3_determinate*(m: ptr zpl_mat3): zpl_f32 {.implibrgC.}
proc zpl_mat3_v*(m: array[3, zpl_vec3]): ptr zpl_mat3 {.implibrgC.}
# Declaration 'zpl_mat3_f' skipped
# Declaration 'zpl_mat3_f' skipped
# Declaration 'm' skipped
proc zpl_float33_m*(m: ptr zpl_mat3): ptr zpl_float3 {.implibrgC.}
proc zpl_float33_v*(m: array[3, zpl_vec3]): ptr zpl_float3 {.implibrgC.}
proc zpl_float33_9*(m: array[9, zpl_f32]): ptr zpl_float3 {.implibrgC.}
# Declaration 'zpl_float33_transpose' skipped
# Declaration 'zpl_f32' skipped
# Declaration 'vec' skipped
# Declaration 'zpl_float33_mul' skipped
# Declaration 'zpl_f32' skipped
# Declaration 'out' skipped
# Declaration 'zpl_f32' skipped
# Declaration 'mat1' skipped
# Declaration 'zpl_f32' skipped
# Declaration 'mat2' skipped
# Declaration 'zpl_float33_mul_vec3' skipped
# Declaration 'out' skipped
# Declaration 'zpl_float33_mul_vec3' skipped
# Declaration 'out' skipped
# Declaration 'm' skipped
# Declaration 'in' skipped
proc zpl_mat4_identity*(m: ptr zpl_mat4) {.implibrgC.}
# Declaration 'zpl_float44_identity' skipped
# Declaration 'zpl_float44_identity' skipped
# Declaration 'm' skipped
proc zpl_mat4_transpose*(m: ptr zpl_mat4) {.implibrgC.}
proc zpl_mat4_mul*(`out`: ptr zpl_mat4, m1: ptr zpl_mat4, m2: ptr zpl_mat4) {.implibrgC.}
proc zpl_mat4_mul_vec4*(`out`: ptr zpl_vec4, m: ptr zpl_mat4, `in`: zpl_vec4) {.implibrgC.}
proc zpl_mat4_inverse*(`out`: ptr zpl_mat4, `in`: ptr zpl_mat4) {.implibrgC.}
proc zpl_mat4_v*(m: array[4, zpl_vec4]): ptr zpl_mat4 {.implibrgC.}
# Declaration 'zpl_mat4_f' skipped
# Declaration 'zpl_mat4_f' skipped
# Declaration 'm' skipped
proc zpl_float44_m*(m: ptr zpl_mat4): ptr zpl_float4 {.implibrgC.}
proc zpl_float44_v*(m: array[4, zpl_vec4]): ptr zpl_float4 {.implibrgC.}
proc zpl_float44_16*(m: array[16, zpl_f32]): ptr zpl_float4 {.implibrgC.}
# Declaration 'zpl_float44_transpose' skipped
# Declaration 'zpl_f32' skipped
# Declaration 'vec' skipped
# Declaration 'zpl_float44_mul' skipped
# Declaration 'zpl_f32' skipped
# Declaration 'out' skipped
# Declaration 'zpl_f32' skipped
# Declaration 'mat1' skipped
# Declaration 'zpl_f32' skipped
# Declaration 'mat2' skipped
# Declaration 'zpl_float44_mul_vec4' skipped
# Declaration 'out' skipped
# Declaration 'zpl_float44_mul_vec4' skipped
# Declaration 'out' skipped
# Declaration 'm' skipped
# Declaration 'in' skipped
proc zpl_mat4_translate*(`out`: ptr zpl_mat4, v: zpl_vec3) {.implibrgC.}
proc zpl_mat4_rotate*(`out`: ptr zpl_mat4, v: zpl_vec3, angle_radians: zpl_f32) {.implibrgC.}
proc zpl_mat4_scale*(`out`: ptr zpl_mat4, v: zpl_vec3) {.implibrgC.}
proc zpl_mat4_scalef*(`out`: ptr zpl_mat4, s: zpl_f32) {.implibrgC.}
proc zpl_mat4_ortho2d*(`out`: ptr zpl_mat4, left: zpl_f32, right: zpl_f32, bottom: zpl_f32, top: zpl_f32) {.implibrgC.}
proc zpl_mat4_ortho3d*(`out`: ptr zpl_mat4, left: zpl_f32, right: zpl_f32, bottom: zpl_f32, top: zpl_f32, z_near: zpl_f32, z_far: zpl_f32) {.implibrgC.}
proc zpl_mat4_perspective*(`out`: ptr zpl_mat4, fovy: zpl_f32, aspect: zpl_f32, z_near: zpl_f32, z_far: zpl_f32) {.implibrgC.}
proc zpl_mat4_infinite_perspective*(`out`: ptr zpl_mat4, fovy: zpl_f32, aspect: zpl_f32, z_near: zpl_f32) {.implibrgC.}
proc zpl_mat4_look_at*(`out`: ptr zpl_mat4, eye: zpl_vec3, centre: zpl_vec3, up: zpl_vec3) {.implibrgC.}
proc zpl_quatf*(x: zpl_f32, y: zpl_f32, z: zpl_f32, w: zpl_f32): zpl_quat {.implibrgC.}
proc zpl_quatfv*(e: array[4, zpl_f32]): zpl_quat {.implibrgC.}
proc zpl_quat_axis_angle*(axis: zpl_vec3, angle_radians: zpl_f32): zpl_quat {.implibrgC.}
proc zpl_quat_euler_angles*(pitch: zpl_f32, yaw: zpl_f32, roll: zpl_f32): zpl_quat {.implibrgC.}
proc zpl_quat_identity*(): zpl_quat {.implibrgC.}
proc zpl_quat_add*(d: ptr zpl_quat, q0: zpl_quat, q1: zpl_quat) {.implibrgC.}
proc zpl_quat_sub*(d: ptr zpl_quat, q0: zpl_quat, q1: zpl_quat) {.implibrgC.}
proc zpl_quat_mul*(d: ptr zpl_quat, q0: zpl_quat, q1: zpl_quat) {.implibrgC.}
proc zpl_quat_div*(d: ptr zpl_quat, q0: zpl_quat, q1: zpl_quat) {.implibrgC.}
proc zpl_quat_mulf*(d: ptr zpl_quat, q: zpl_quat, s: zpl_f32) {.implibrgC.}
proc zpl_quat_divf*(d: ptr zpl_quat, q: zpl_quat, s: zpl_f32) {.implibrgC.}
proc zpl_quat_addeq*(d: ptr zpl_quat, q: zpl_quat) {.implibrgC.}
proc zpl_quat_subeq*(d: ptr zpl_quat, q: zpl_quat) {.implibrgC.}
proc zpl_quat_muleq*(d: ptr zpl_quat, q: zpl_quat) {.implibrgC.}
proc zpl_quat_diveq*(d: ptr zpl_quat, q: zpl_quat) {.implibrgC.}
proc zpl_quat_muleqf*(d: ptr zpl_quat, s: zpl_f32) {.implibrgC.}
proc zpl_quat_diveqf*(d: ptr zpl_quat, s: zpl_f32) {.implibrgC.}
proc zpl_quat_dot*(q0: zpl_quat, q1: zpl_quat): zpl_f32 {.implibrgC.}
proc zpl_quat_mag*(q: zpl_quat): zpl_f32 {.implibrgC.}
proc zpl_quat_norm*(d: ptr zpl_quat, q: zpl_quat) {.implibrgC.}
proc zpl_quat_conj*(d: ptr zpl_quat, q: zpl_quat) {.implibrgC.}
proc zpl_quat_inverse*(d: ptr zpl_quat, q: zpl_quat) {.implibrgC.}
proc zpl_quat_axis*(axis: ptr zpl_vec3, q: zpl_quat) {.implibrgC.}
proc zpl_quat_angle*(q: zpl_quat): zpl_f32 {.implibrgC.}
proc zpl_quat_pitch*(q: zpl_quat): zpl_f32 {.implibrgC.}
proc zpl_quat_yaw*(q: zpl_quat): zpl_f32 {.implibrgC.}
proc zpl_quat_roll*(q: zpl_quat): zpl_f32 {.implibrgC.}

#  NOTE: Rotate v by q
proc zpl_quat_rotate_vec3*(d: ptr zpl_vec3, q: zpl_quat, v: zpl_vec3) {.implibrgC.}
proc zpl_mat4_from_quat*(`out`: ptr zpl_mat4, q: zpl_quat) {.implibrgC.}
proc zpl_quat_from_mat4*(`out`: ptr zpl_quat, m: ptr zpl_mat4) {.implibrgC.}

#  Interpolations
proc zpl_lerp*(a: zpl_f32, b: zpl_f32, t: zpl_f32): zpl_f32 {.implibrgC.}
proc zpl_unlerp*(t: zpl_f32, a: zpl_f32, b: zpl_f32): zpl_f32 {.implibrgC.}
proc zpl_smooth_step*(a: zpl_f32, b: zpl_f32, t: zpl_f32): zpl_f32 {.implibrgC.}
proc zpl_smoother_step*(a: zpl_f32, b: zpl_f32, t: zpl_f32): zpl_f32 {.implibrgC.}
proc zpl_vec2_lerp*(d: ptr zpl_vec2, a: zpl_vec2, b: zpl_vec2, t: zpl_f32) {.implibrgC.}
proc zpl_vec3_lerp*(d: ptr zpl_vec3, a: zpl_vec3, b: zpl_vec3, t: zpl_f32) {.implibrgC.}
proc zpl_vec4_lerp*(d: ptr zpl_vec4, a: zpl_vec4, b: zpl_vec4, t: zpl_f32) {.implibrgC.}
proc zpl_vec2_cslerp*(d: ptr zpl_vec2, a: zpl_vec2, v0: zpl_vec2, b: zpl_vec2, v1: zpl_vec2, t: zpl_f32) {.implibrgC.}
proc zpl_vec3_cslerp*(d: ptr zpl_vec3, a: zpl_vec3, v0: zpl_vec3, b: zpl_vec3, v1: zpl_vec3, t: zpl_f32) {.implibrgC.}
proc zpl_vec2_dcslerp*(d: ptr zpl_vec2, a: zpl_vec2, v0: zpl_vec2, b: zpl_vec2, v1: zpl_vec2, t: zpl_f32) {.implibrgC.}
proc zpl_vec3_dcslerp*(d: ptr zpl_vec3, a: zpl_vec3, v0: zpl_vec3, b: zpl_vec3, v1: zpl_vec3, t: zpl_f32) {.implibrgC.}
proc zpl_quat_lerp*(d: ptr zpl_quat, a: zpl_quat, b: zpl_quat, t: zpl_f32) {.implibrgC.}
proc zpl_quat_nlerp*(d: ptr zpl_quat, a: zpl_quat, b: zpl_quat, t: zpl_f32) {.implibrgC.}
proc zpl_quat_slerp*(d: ptr zpl_quat, a: zpl_quat, b: zpl_quat, t: zpl_f32) {.implibrgC.}
proc zpl_quat_nquad*(d: ptr zpl_quat, p: zpl_quat, a: zpl_quat, b: zpl_quat, q: zpl_quat, t: zpl_f32) {.implibrgC.}
proc zpl_quat_squad*(d: ptr zpl_quat, p: zpl_quat, a: zpl_quat, b: zpl_quat, q: zpl_quat, t: zpl_f32) {.implibrgC.}
proc zpl_quat_slerp_approx*(d: ptr zpl_quat, a: zpl_quat, b: zpl_quat, t: zpl_f32) {.implibrgC.}
proc zpl_quat_squad_approx*(d: ptr zpl_quat, p: zpl_quat, a: zpl_quat, b: zpl_quat, q: zpl_quat, t: zpl_f32) {.implibrgC.}

#  Rects
proc zpl_rect2f*(pos: zpl_vec2, dim: zpl_vec2): zpl_rect2 {.implibrgC.}
proc zpl_rect3f*(pos: zpl_vec3, dim: zpl_vec3): zpl_rect3 {.implibrgC.}
proc zpl_rect2_contains*(a: zpl_rect2, x: zpl_f32, y: zpl_f32): cint {.implibrgC.}
proc zpl_rect2_contains_vec2*(a: zpl_rect2, p: zpl_vec2): cint {.implibrgC.}
proc zpl_rect2_intersects*(a: zpl_rect2, b: zpl_rect2): cint {.implibrgC.}
proc zpl_rect2_intersection_result*(a: zpl_rect2, b: zpl_rect2, intersection: ptr zpl_rect2): cint {.implibrgC.}
proc enet_malloc*(a1: uint): pointer {.implibrgC.}
proc enet_free*(a1: pointer) {.implibrgC.}
proc enet_list_insert*(a1: ENetListIterator, a2: pointer): ENetListIterator {.implibrgC.}
proc enet_list_move*(a1: ENetListIterator, a2: pointer, a3: pointer): ENetListIterator {.implibrgC.}
proc enet_list_remove*(a1: ENetListIterator): pointer {.implibrgC.}
proc enet_list_clear*(a1: ptr ENetList) {.implibrgC.}
proc enet_list_size*(a1: ptr ENetList): uint {.implibrgC.}

#  =======================================================================
#  !
#  ! Public API
#  !
#  =======================================================================
# *
#      * Initializes ENet globally.  Must be called prior to using any functions in ENet.
#      * @returns 0 on success, < 0 on failure
# 
proc enet_initialize*(): cint {.implibrgC.}

# *
#      * Initializes ENet globally and supplies user-overridden callbacks. Must be called prior to using any functions in ENet. Do not use enet_initialize() if you use this variant. Make sure the ENetCallbacks structure is zeroed out so that any additional callbacks added in future versions will be properly ignored.
#      *
#      * @param version the constant ENET_VERSION should be supplied so ENet knows which version of ENetCallbacks struct to use
#      * @param inits user-overridden callbacks where any NULL callbacks will use ENet's defaults
#      * @returns 0 on success, < 0 on failure
# 
proc enet_initialize_with_callbacks*(version: ENetVersion, inits: ptr ENetCallbacks): cint {.implibrgC.}

# *
#      * Shuts down ENet globally.  Should be called when a program that has initialized ENet exits.
# 
proc enet_deinitialize*() {.implibrgC.}

# *
#      * Gives the linked version of the ENet library.
#      * @returns the version number
# 
proc enet_linked_version*(): ENetVersion {.implibrgC.}

# * Returns the monotonic time in milliseconds. Its initial value is unspecified unless otherwise set.
proc enet_time_get*(): enet_uint32 {.implibrgC.}

# * ENet socket functions
proc enet_socket_create*(a1: ENetSocketType): ENetSocket {.implibrgC.}
proc enet_socket_bind*(a1: ENetSocket, a2: ptr ENetAddress): cint {.implibrgC.}
proc enet_socket_get_address*(a1: ENetSocket, a2: ptr ENetAddress): cint {.implibrgC.}
proc enet_socket_listen*(a1: ENetSocket, a2: cint): cint {.implibrgC.}
proc enet_socket_accept*(a1: ENetSocket, a2: ptr ENetAddress): ENetSocket {.implibrgC.}
proc enet_socket_connect*(a1: ENetSocket, a2: ptr ENetAddress): cint {.implibrgC.}
proc enet_socket_send*(a1: ENetSocket, a2: ptr ENetAddress, a3: ptr ENetBuffer, a4: uint): cint {.implibrgC.}
proc enet_socket_receive*(a1: ENetSocket, a2: ptr ENetAddress, a3: ptr ENetBuffer, a4: uint): cint {.implibrgC.}
proc enet_socket_wait*(a1: ENetSocket, a2: ptr enet_uint32, a3: enet_uint64): cint {.implibrgC.}
proc enet_socket_set_option*(a1: ENetSocket, a2: ENetSocketOption, a3: cint): cint {.implibrgC.}
proc enet_socket_get_option*(a1: ENetSocket, a2: ENetSocketOption, a3: ptr cint): cint {.implibrgC.}
proc enet_socket_shutdown*(a1: ENetSocket, a2: ENetSocketShutdown): cint {.implibrgC.}
proc enet_socket_destroy*(a1: ENetSocket) {.implibrgC.}
proc enet_socketset_select*(a1: ENetSocket, a2: ptr ENetSocketSet, a3: ptr ENetSocketSet, a4: enet_uint32): cint {.implibrgC.}

# * Attempts to parse the printable form of the IP address in the parameter hostName
#         and sets the host field in the address parameter if successful.
#         @param address destination to store the parsed IP address
#         @param hostName IP address to parse
#         @retval 0 on success
#         @retval < 0 on failure
#         @returns the address of the given hostName in address on success
# 
proc enet_address_set_host_ip*(address: ptr ENetAddress, hostName: cstring): cint {.implibrgC.}

# * Attempts to resolve the host named by the parameter hostName and sets
#         the host field in the address parameter if successful.
#         @param address destination to store resolved address
#         @param hostName host name to lookup
#         @retval 0 on success
#         @retval < 0 on failure
#         @returns the address of the given hostName in address on success
# 
proc enet_address_set_host*(address: ptr ENetAddress, hostName: cstring): cint {.implibrgC.}

# * Gives the printable form of the IP address specified in the address parameter.
#         @param address    address printed
#         @param hostName   destination for name, must not be NULL
#         @param nameLength maximum length of hostName.
#         @returns the null-terminated name of the host in hostName on success
#         @retval 0 on success
#         @retval < 0 on failure
# 
proc enet_address_get_host_ip*(address: ptr ENetAddress, hostName: cstring, nameLength: uint): cint {.implibrgC.}

# * Attempts to do a reverse lookup of the host field in the address parameter.
#         @param address    address used for reverse lookup
#         @param hostName   destination for name, must not be NULL
#         @param nameLength maximum length of hostName.
#         @returns the null-terminated name of the host in hostName on success
#         @retval 0 on success
#         @retval < 0 on failure
# 
proc enet_address_get_host*(address: ptr ENetAddress, hostName: cstring, nameLength: uint): cint {.implibrgC.}
proc enet_host_get_peers_count*(a1: ptr ENetHost): enet_uint32 {.implibrgC.}
proc enet_host_get_packets_sent*(a1: ptr ENetHost): enet_uint32 {.implibrgC.}
proc enet_host_get_packets_received*(a1: ptr ENetHost): enet_uint32 {.implibrgC.}
proc enet_host_get_bytes_sent*(a1: ptr ENetHost): enet_uint32 {.implibrgC.}
proc enet_host_get_bytes_received*(a1: ptr ENetHost): enet_uint32 {.implibrgC.}
proc enet_peer_get_id*(a1: ptr ENetPeer): enet_uint32 {.implibrgC.}
proc enet_peer_get_ip*(a1: ptr ENetPeer, ip: cstring, ipLength: uint): enet_uint32 {.implibrgC.}
proc enet_peer_get_port*(a1: ptr ENetPeer): enet_uint16 {.implibrgC.}
proc enet_peer_get_rtt*(a1: ptr ENetPeer): enet_uint32 {.implibrgC.}
proc enet_peer_get_packets_sent*(a1: ptr ENetPeer): enet_uint64 {.implibrgC.}
proc enet_peer_get_packets_lost*(a1: ptr ENetPeer): enet_uint32 {.implibrgC.}
proc enet_peer_get_bytes_sent*(a1: ptr ENetPeer): enet_uint64 {.implibrgC.}
proc enet_peer_get_bytes_received*(a1: ptr ENetPeer): enet_uint64 {.implibrgC.}
proc enet_peer_get_state*(a1: ptr ENetPeer): ENetPeerState {.implibrgC.}
proc enet_peer_get_data*(a1: ptr ENetPeer): pointer {.implibrgC.}
proc enet_peer_set_data*(a1: ptr ENetPeer, a2: pointer) {.implibrgC.}
proc enet_packet_get_data*(a1: ptr ENetPacket): pointer {.implibrgC.}
proc enet_packet_get_length*(a1: ptr ENetPacket): enet_uint32 {.implibrgC.}
proc enet_packet_set_free_callback*(a1: ptr ENetPacket, a2: pointer) {.implibrgC.}
proc enet_packet_create*(a1: pointer, a2: uint, a3: enet_uint32): ptr ENetPacket {.implibrgC.}
proc enet_packet_create_offset*(a1: pointer, a2: uint, a3: uint, a4: enet_uint32): ptr ENetPacket {.implibrgC.}
proc enet_packet_destroy*(a1: ptr ENetPacket) {.implibrgC.}
proc enet_crc32*(a1: ptr ENetBuffer, a2: uint): enet_uint32 {.implibrgC.}
proc enet_host_create*(a1: ptr ENetAddress, a2: uint, a3: uint, a4: enet_uint32, a5: enet_uint32): ptr ENetHost {.implibrgC.}
proc enet_host_destroy*(a1: ptr ENetHost) {.implibrgC.}
proc enet_host_connect*(a1: ptr ENetHost, a2: ptr ENetAddress, a3: uint, a4: enet_uint32): ptr ENetPeer {.implibrgC.}
proc enet_host_check_events*(a1: ptr ENetHost, a2: ptr ENetEvent): cint {.implibrgC.}
proc enet_host_service*(a1: ptr ENetHost, a2: ptr ENetEvent, a3: enet_uint32): cint {.implibrgC.}
proc enet_host_flush*(a1: ptr ENetHost) {.implibrgC.}
proc enet_host_broadcast*(a1: ptr ENetHost, a2: enet_uint8, a3: ptr ENetPacket) {.implibrgC.}
proc enet_host_compress*(a1: ptr ENetHost, a2: ptr ENetCompressor) {.implibrgC.}
proc enet_host_channel_limit*(a1: ptr ENetHost, a2: uint) {.implibrgC.}
proc enet_host_bandwidth_limit*(a1: ptr ENetHost, a2: enet_uint32, a3: enet_uint32) {.implibrgC.}
proc enet_host_bandwidth_throttle*(a1: ptr ENetHost) {.implibrgC.}
proc enet_host_random_seed*(): enet_uint64 {.implibrgC.}
proc enet_peer_send*(a1: ptr ENetPeer, a2: enet_uint8, a3: ptr ENetPacket): cint {.implibrgC.}
proc enet_peer_receive*(a1: ptr ENetPeer, channelID: ptr enet_uint8): ptr ENetPacket {.implibrgC.}
proc enet_peer_ping*(a1: ptr ENetPeer) {.implibrgC.}
proc enet_peer_ping_interval*(a1: ptr ENetPeer, a2: enet_uint32) {.implibrgC.}
proc enet_peer_timeout*(a1: ptr ENetPeer, a2: enet_uint32, a3: enet_uint32, a4: enet_uint32) {.implibrgC.}
proc enet_peer_reset*(a1: ptr ENetPeer) {.implibrgC.}
proc enet_peer_disconnect*(a1: ptr ENetPeer, a2: enet_uint32) {.implibrgC.}
proc enet_peer_disconnect_now*(a1: ptr ENetPeer, a2: enet_uint32) {.implibrgC.}
proc enet_peer_disconnect_later*(a1: ptr ENetPeer, a2: enet_uint32) {.implibrgC.}
proc enet_peer_throttle_configure*(a1: ptr ENetPeer, a2: enet_uint32, a3: enet_uint32, a4: enet_uint32) {.implibrgC.}
proc enet_peer_throttle*(a1: ptr ENetPeer, a2: enet_uint32): cint {.implibrgC.}
proc enet_peer_reset_queues*(a1: ptr ENetPeer) {.implibrgC.}
proc enet_peer_setup_outgoing_command*(a1: ptr ENetPeer, a2: ptr ENetOutgoingCommand) {.implibrgC.}
proc enet_peer_queue_outgoing_command*(a1: ptr ENetPeer, a2: ptr ENetProtocol, a3: ptr ENetPacket, a4: enet_uint32, a5: enet_uint16): ptr ENetOutgoingCommand {.implibrgC.}
proc enet_peer_queue_incoming_command*(a1: ptr ENetPeer, a2: ptr ENetProtocol, a3: pointer, a4: uint, a5: enet_uint32, a6: enet_uint32): ptr ENetIncomingCommand {.implibrgC.}
proc enet_peer_queue_acknowledgement*(a1: ptr ENetPeer, a2: ptr ENetProtocol, a3: enet_uint16): ptr ENetAcknowledgement {.implibrgC.}
proc enet_peer_dispatch_incoming_unreliable_commands*(a1: ptr ENetPeer, a2: ptr ENetChannel) {.implibrgC.}
proc enet_peer_dispatch_incoming_reliable_commands*(a1: ptr ENetPeer, a2: ptr ENetChannel) {.implibrgC.}
proc enet_peer_on_connect*(a1: ptr ENetPeer) {.implibrgC.}
proc enet_peer_on_disconnect*(a1: ptr ENetPeer) {.implibrgC.}
proc enet_protocol_command_size*(a1: enet_uint8): uint {.implibrgC.}
proc librg_table_init*(h: ptr librg_table, a: zpl_allocator) {.implibrgC.}
proc librg_table_destroy*(h: ptr librg_table) {.implibrgC.}
proc librg_table_get*(h: ptr librg_table, key: zpl_u64): ptr u32 {.implibrgC.}
proc librg_table_set*(h: ptr librg_table, key: zpl_u64, value: u32) {.implibrgC.}
proc librg_table_grow*(h: ptr librg_table) {.implibrgC.}
proc librg_table_rehash*(h: ptr librg_table, new_count: zpl_isize) {.implibrgC.}
proc librg_table_remove*(h: ptr librg_table, key: zpl_u64) {.implibrgC.}
proc librg_event_pool_init*(h: ptr librg_event_pool, a: zpl_allocator) {.implibrgC.}
proc librg_event_pool_destroy*(h: ptr librg_event_pool) {.implibrgC.}
proc librg_event_pool_get*(h: ptr librg_event_pool, key: zpl_u64): ptr librg_event_block {.implibrgC.}
proc librg_event_pool_set*(h: ptr librg_event_pool, key: zpl_u64, value: librg_event_block) {.implibrgC.}
proc librg_event_pool_grow*(h: ptr librg_event_pool) {.implibrgC.}
proc librg_event_pool_rehash*(h: ptr librg_event_pool, new_count: zpl_isize) {.implibrgC.}
proc librg_event_pool_remove*(h: ptr librg_event_pool, key: zpl_u64) {.implibrgC.}

# *
#  * Allocate librg ctx
#  * (to be used inside bindings)
# 
proc librg_allocate_ptr*(pointer_type: librg_pointer_type): pointer {.implibrgC.}

# *
#  * Frees a pointer allocated by library
#  * usually used in bindings.
# 
proc librg_release_ptr*(`ptr`: pointer) {.implibrgC.}

# *
#  * Main initialization method
#  * MUST BE called in the begging of your application
# 
proc librg_init*(ctx: ptr librg_ctx): u32 {.implibrgC.}

# *
#  * Should be called at the end of
#  * execution of the program
# 
proc librg_free*(ctx: ptr librg_ctx) {.implibrgC.}

# *
#  * Main tick method
#  * MUST BE called in your loop
#  * preferably w/o delays
#  *
#  * Calling tick ensures that packets will be sent and received,
#  * world state will be recalculated and new update data will be created/used
# 
proc librg_tick*(ctx: ptr librg_ctx) {.implibrgC.}

# *
#  * Create entity and return handle
# 
proc librg_entity_create*(ctx: ptr librg_ctx, `type`: u32): ptr librg_entity {.implibrgC.}

# *
#  * Try to find an existing entity by a provided id, or NULL
# 
proc librg_entity_fetch*(ctx: ptr librg_ctx, entity_id: u32): ptr librg_entity {.implibrgC.}

# *
#  * Reuqest an entity destruction (will be marked as destroyed, and send to be destroyed on clients on the next tick)
# 
proc librg_entity_destroy*(ctx: ptr librg_ctx, entity_id: u32) {.implibrgC.}

# *
#  * Query for entities that are in stream zone
#  * for current entity, and are visible to this entity
#  * an uninitialized pointer to the pointer of librg_entity_id should be provided:
#  *
#  * EXAMPLE:
#  * librg_entity_id *result;
#  * usize amount = librg_entity_query(&ctx, 15, &result);
# 
proc librg_entity_query*(ctx: ptr librg_ctx, entity_id: u32, result: ptr ptr u32): usize {.implibrgC.}

# *
#  * Check if provided entity is a valid entity
# 
proc librg_entity_valid*(ctx: ptr librg_ctx, id: u32): b32 {.implibrgC.}

# *
#  * Try to find an entity that is assossiated with a particular peer, or NULL
# 
proc librg_entity_find*(ctx: ptr librg_ctx, peer: ptr librg_peer): ptr librg_entity {.implibrgC.}

# *
#  * Concept of entity control describes an entity behavior, data of which is constantly sent to the server from a particular peer (controller).
#  * And, updates about that entity are later sent to other peers, except the original controller.
#  *
#  * New data will arrive, accordingly to configured client tick delay, and the LIBRG_CLIENT_STREAMER_UPDATE event will be triggered.
#  * librg_entity_control_set is an async method, it will send a message to the client, notifiyng him of becoming a controller.
#  * librg_entity_control_remove is a hybrid sync+async method. Behaves similar wat to _set, described above, however,
#  * even thought client will still be sending control messages for some time, server will reject them if method has been called.
#  *
#  * For occasions, where some sort of an authoritative pause is needed for the server, in the controlled data stream,
#  * for example you want to change some property data of the entity, authoritatively from the server, like position.
#  * Calling librg_entity_control_remove(ctx, id), making the neccessary changes, and calling librg_entity_control_set(ctx, id, peer)
#  * will make sure all current streamed data from the client will be ignored, and new data will be force-delivered to client
# 
proc librg_entity_control_get*(ctx: ptr librg_ctx, entity_id: u32): ptr librg_peer {.implibrgC.}
proc librg_entity_control_set*(ctx: ptr librg_ctx, entity_id: u32, peer: ptr librg_peer) {.implibrgC.}
proc librg_entity_control_remove*(ctx: ptr librg_ctx, entity_id: u32) {.implibrgC.}
proc librg_entity_control_ignore_next_update*(ctx: ptr librg_ctx, entity_id: u32) {.implibrgC.}

# *
#  * Iterate over all the entities with a flag
# 
proc librg_entity_iterate*(ctx: ptr librg_ctx, flags: u64, callback: librg_entity_cb) {.implibrgC.}

# *
#  * C based entity iteration macro
# 
# *
#  * Return entity type
# 
proc librg_entity_type*(ctx: ptr librg_ctx, entity_id: u32): u32 {.implibrgC.}

# *
#      * A complex api allowing to change visibility of the entities via specific set of rules.
#      *
#      * Calling librg_entity_visibility_set, will change global visibility of partiulcar entity for others.
#      * Meaning, the entity is still gonna be able to see other entities, while others won't be able to see it back.
#      *
#      * Calling librg_entity_visibility_set_for, has a similar effect to method mentioned above,
#      * however extends also to specify entity inivisibilty for other specific entity.
# 
proc librg_entity_visibility_set*(ctx: ptr librg_ctx, entity_id: u32, state: librg_visibility) {.implibrgC.}
proc librg_entity_visibility_get*(ctx: ptr librg_ctx, entity_id: u32): librg_visibility {.implibrgC.}
proc librg_entity_visibility_set_for*(ctx: ptr librg_ctx, entity_id: u32, target: u32, state: librg_visibility) {.implibrgC.}
proc librg_entity_visibility_get_for*(ctx: ptr librg_ctx, entity_id: u32, target: u32): librg_visibility {.implibrgC.}

# *
#      * A simple thin api allowing to change virtual world for specific entity
#      * "Virtual world" is a concept where entities can see other entities near
#      * each other only when they share the virtual world.
#      *
#      * If entity is being removed from the virtual world, it's, LIBRG_ENTITY_REMOVED
#      * will be called for all the clients that were sharing same world with target entity.
# 
proc librg_entity_world_get*(ctx: ptr librg_ctx, entity_id: u32): u32 {.implibrgC.}
proc librg_entity_world_set*(ctx: ptr librg_ctx, entity_id: u32, world: u32) {.implibrgC.}

# *
#  * Used to attach event handler
#  * You can bind as many event handlers onto
#  * single event, as you want
#  *
#  * In the callback you will need to cast event
#  * to type of structure that you've triggered this event with
#  *
#  * @param  id usually you define event ids inside enum
#  * @param  callback
#  * @return index of added event, can be used to remove particular event handler
# 
proc librg_event_add*(ctx: ptr librg_ctx, id: u64, callback: librg_event_cb): u64 {.implibrgC.}

# *
#  * Used to trigger execution of all attached
#  * event handlers for particlar event
#  *
#  * You can provide pointer to any data, which will be
#  * passed inside the event callback
#  *
#  * @param id usually you define event ids inside enum
#  * @param event pointer onto data or NULL
# 
proc librg_event_trigger*(ctx: ptr librg_ctx, id: u64, event: ptr librg_event) {.implibrgC.}

# *
#  * Used to remove particular callback from
#  * event chain, so it wont be called ever again
#  *
#  * @param id usually you define event ids inside enum
#  * @param index returned by librg_event_add
# 
proc librg_event_remove*(ctx: ptr librg_ctx, id: u64, index: u64) {.implibrgC.}

# *
#  * Used to reject some event from triggering from
#  * inside of executing callback
# 
proc librg_event_reject*(event: ptr librg_event) {.implibrgC.}

# *
#  * Used to check if some event can be rejected
# 
proc librg_event_rejectable*(event: ptr librg_event): b32 {.implibrgC.}

# *
#  * Checks if current event was not rejected
#  * inside any of the callbacks
# 
proc librg_event_succeeded*(event: ptr librg_event): b32 {.implibrgC.}

#  =======================================================================
#  !
#  ! Binary data (bitstream)
#  !
#  =======================================================================
# *
#  * Initialize new bitstream with default mem size
# 
proc librg_data_init*(data: ptr librg_data) {.implibrgC.}

# *
#  * Initialize new bitstream with custom mem size
# 
proc librg_data_init_size*(data: ptr librg_data, size: usize) {.implibrgC.}

# *
#  * Initialize new bitstream with custom mem size (bindings stuff)
# 
proc librg_data_init_new*(): ptr librg_data {.implibrgC.}

# *
#  * Free initialized bitstream
# 
proc librg_data_free*(data: ptr librg_data) {.implibrgC.}

# *
#  * Reset initialized bitstream
#  * NOTE: doesnt remove any data, just resets read and write pos to 0
# 
proc librg_data_reset*(data: ptr librg_data) {.implibrgC.}

# *
#  * Increase size of bitstream
# 
proc librg_data_grow*(data: ptr librg_data, min_size: usize) {.implibrgC.}

# *
#  * Methods for getting various parameters of bitstream
# 
proc librg_data_capacity*(data: ptr librg_data): usize {.implibrgC.}
proc librg_data_get_rpos*(data: ptr librg_data): usize {.implibrgC.}
proc librg_data_get_wpos*(data: ptr librg_data): usize {.implibrgC.}
proc librg_data_set_rpos*(data: ptr librg_data, position: usize) {.implibrgC.}
proc librg_data_set_wpos*(data: ptr librg_data, position: usize) {.implibrgC.}

# *
#  * Read and write methods for custom sized data
# 
proc librg_data_rptr*(data: ptr librg_data, `ptr`: pointer, size: usize): b32 {.implibrgC.}
proc librg_data_wptr*(data: ptr librg_data, `ptr`: pointer, size: usize) {.implibrgC.}

# *
#  * Read and write methods for custom sized data
#  * at particular position in memory
# 
proc librg_data_rptr_at*(data: ptr librg_data, `ptr`: pointer, size: usize, position: isize): b32 {.implibrgC.}
proc librg_data_wptr_at*(data: ptr librg_data, `ptr`: pointer, size: usize, position: isize) {.implibrgC.}

# *
#  * A helprer macro for onliner methods
# 
# *
#  * General one-line methods for reading/writing different types
# 
proc librg_data_ri8*(data: ptr librg_data): i8 {.implibrgC.}
proc librg_data_wi8*(data: ptr librg_data, value: i8) {.implibrgC.}
proc librg_data_ri8_at*(data: ptr librg_data, position: isize): i8 {.implibrgC.}
proc librg_data_wi8_at*(data: ptr librg_data, value: i8, position: isize) {.implibrgC.}
proc librg_data_ru8*(data: ptr librg_data): u8 {.implibrgC.}
proc librg_data_wu8*(data: ptr librg_data, value: u8) {.implibrgC.}
proc librg_data_ru8_at*(data: ptr librg_data, position: isize): u8 {.implibrgC.}
proc librg_data_wu8_at*(data: ptr librg_data, value: u8, position: isize) {.implibrgC.}
proc librg_data_ri16*(data: ptr librg_data): i16 {.implibrgC.}
proc librg_data_wi16*(data: ptr librg_data, value: i16) {.implibrgC.}
proc librg_data_ri16_at*(data: ptr librg_data, position: isize): i16 {.implibrgC.}
proc librg_data_wi16_at*(data: ptr librg_data, value: i16, position: isize) {.implibrgC.}
proc librg_data_ru16*(data: ptr librg_data): u16 {.implibrgC.}
proc librg_data_wu16*(data: ptr librg_data, value: u16) {.implibrgC.}
proc librg_data_ru16_at*(data: ptr librg_data, position: isize): u16 {.implibrgC.}
proc librg_data_wu16_at*(data: ptr librg_data, value: u16, position: isize) {.implibrgC.}
proc librg_data_ri32*(data: ptr librg_data): i32 {.implibrgC.}
proc librg_data_wi32*(data: ptr librg_data, value: i32) {.implibrgC.}
proc librg_data_ri32_at*(data: ptr librg_data, position: isize): i32 {.implibrgC.}
proc librg_data_wi32_at*(data: ptr librg_data, value: i32, position: isize) {.implibrgC.}
proc librg_data_ru32*(data: ptr librg_data): u32 {.implibrgC.}
proc librg_data_wu32*(data: ptr librg_data, value: u32) {.implibrgC.}
proc librg_data_ru32_at*(data: ptr librg_data, position: isize): u32 {.implibrgC.}
proc librg_data_wu32_at*(data: ptr librg_data, value: u32, position: isize) {.implibrgC.}
proc librg_data_ri64*(data: ptr librg_data): i64 {.implibrgC.}
proc librg_data_wi64*(data: ptr librg_data, value: i64) {.implibrgC.}
proc librg_data_ri64_at*(data: ptr librg_data, position: isize): i64 {.implibrgC.}
proc librg_data_wi64_at*(data: ptr librg_data, value: i64, position: isize) {.implibrgC.}
proc librg_data_ru64*(data: ptr librg_data): u64 {.implibrgC.}
proc librg_data_wu64*(data: ptr librg_data, value: u64) {.implibrgC.}
proc librg_data_ru64_at*(data: ptr librg_data, position: isize): u64 {.implibrgC.}
proc librg_data_wu64_at*(data: ptr librg_data, value: u64, position: isize) {.implibrgC.}
proc librg_data_rf32*(data: ptr librg_data): f32 {.implibrgC.}
proc librg_data_wf32*(data: ptr librg_data, value: f32) {.implibrgC.}
proc librg_data_rf32_at*(data: ptr librg_data, position: isize): f32 {.implibrgC.}
proc librg_data_wf32_at*(data: ptr librg_data, value: f32, position: isize) {.implibrgC.}
proc librg_data_rf64*(data: ptr librg_data): f64 {.implibrgC.}
proc librg_data_wf64*(data: ptr librg_data, value: f64) {.implibrgC.}
proc librg_data_rf64_at*(data: ptr librg_data, position: isize): f64 {.implibrgC.}
proc librg_data_wf64_at*(data: ptr librg_data, value: f64, position: isize) {.implibrgC.}
proc librg_data_rb8*(data: ptr librg_data): b8 {.implibrgC.}
proc librg_data_wb8*(data: ptr librg_data, value: b8) {.implibrgC.}
proc librg_data_rb8_at*(data: ptr librg_data, position: isize): b8 {.implibrgC.}
proc librg_data_wb8_at*(data: ptr librg_data, value: b8, position: isize) {.implibrgC.}
proc librg_data_rb16*(data: ptr librg_data): b16 {.implibrgC.}
proc librg_data_wb16*(data: ptr librg_data, value: b16) {.implibrgC.}
proc librg_data_rb16_at*(data: ptr librg_data, position: isize): b16 {.implibrgC.}
proc librg_data_wb16_at*(data: ptr librg_data, value: b16, position: isize) {.implibrgC.}
proc librg_data_rb32*(data: ptr librg_data): b32 {.implibrgC.}
proc librg_data_wb32*(data: ptr librg_data, value: b32) {.implibrgC.}
proc librg_data_rb32_at*(data: ptr librg_data, position: isize): b32 {.implibrgC.}
proc librg_data_wb32_at*(data: ptr librg_data, value: b32, position: isize) {.implibrgC.}

# *
#  * Read/write methods for entity (aliases for u32)
# 
#  =======================================================================
#  !
#  ! Network
#  !
#  =======================================================================
# *
#  * Message structure
#  * created inside network handler
#  * and injected to each incoming message
# 
# *
#  * Check are we connected
# 
proc librg_is_connected*(ctx: ptr librg_ctx): b32 {.implibrgC.}

# *
#  * Is librg instance is running
#  * in the server mode
# 
proc librg_is_server*(ctx: ptr librg_ctx): b32 {.implibrgC.}

# *
#  * Is librg instance is running
#  * in the client mode
# 
proc librg_is_client*(ctx: ptr librg_ctx): b32 {.implibrgC.}

# *
#  * Starts network connection
#  * Requires you to provide .port (if running as server)
#  * or both .port and .host (if running as client)
#  *
#  * For server mode - starts server
#  * For client mode - starts client, and connects to provided host & port
# 
proc librg_network_start*(ctx: ptr librg_ctx, address: librg_address): u32 {.implibrgC.}

# *
#  * Disconnects (if connected), stops network
#  * and releases resources
# 
proc librg_network_stop*(ctx: ptr librg_ctx) {.implibrgC.}

# *
#  * Forces disconnection for provided peer
#  * @param ctx
#  * @param peer
# 
proc librg_network_kick*(ctx: ptr librg_ctx, peer: ptr librg_peer) {.implibrgC.}

# *
#  * Can be used to add handler
#  * to a particular message id
# 
proc librg_network_add*(ctx: ptr librg_ctx, id: u16, callback: librg_message_cb) {.implibrgC.}

# *
#  * Can be used to remove a handler
#  * from particular message id
# 
proc librg_network_remove*(ctx: ptr librg_ctx, id: u16) {.implibrgC.}

# *
#  * General reliable message/data sending API
#  * (original message api version since v2.0.0, might be subject for changes in the future)
#  *
#  * Message id - is an identifier which librg will use to route the message from
#  * (to particlar handlers which are added via librg_network_add)
#  *
#  * method _all - sends the data to all connected peers
#  * method _to - sends the data to particular peer
#  * method _except - sends the data to all but particular peer
#  * method _instream - sends the data to all connected peers in the particular range (based off entity's stream_range)
#  * method _instream_except - similar to method above, however excludes particular peer
#  *
#  * The data which is provided inside the method will be copied inside packet struct, so it can be safely freed after you've called a method.
# 
proc librg_message_send_all*(ctx: ptr librg_ctx, id: u16, data: pointer, size: usize) {.implibrgC.}
proc librg_message_send_to*(ctx: ptr librg_ctx, id: u16, target: ptr librg_peer, data: pointer, size: usize) {.implibrgC.}
proc librg_message_send_except*(ctx: ptr librg_ctx, id: u16, target: ptr librg_peer, data: pointer, size: usize) {.implibrgC.}
proc librg_message_send_instream*(ctx: ptr librg_ctx, id: u16, entity_id: u32, data: pointer, size: usize) {.implibrgC.}
proc librg_message_send_instream_except*(ctx: ptr librg_ctx, id: u16, entity_id: u32, target: ptr librg_peer, data: pointer, size: usize) {.implibrgC.}

# *
#  * The basic message/data sending API
#  * Supports sending both reliable and unreliable messages, and ability to set which channel to use for message sending.
#  *
#  * All previous message sending methods are using these extended variants internally.
#  *
#  * method sendex:
#  *     if target is NULL - will send the data to all connected peers, else - to a particular peer
#  *     if except is NULL - argument does nothing, else - prevents data from being sent to a particlar peer
#  *     channel - provides which network channel is supposed to be used for message sending
#  *         (make that channel id is always < librg_option_get(LIBRG_NETWORK_CHANNELS), or use librg_option_set to change the default value)
#  *     if reliable is 1 (true) - message will be sent using reliable methods, else using unreliable
# 
proc librg_message_sendex*(ctx: ptr librg_ctx, id: u16, target: ptr librg_peer, `except`: ptr librg_peer, channel: u16, reliable: b8, data: pointer, size: usize) {.implibrgC.}
proc librg_message_sendex_instream*(ctx: ptr librg_ctx, id: u16, entity_id: u32, `except`: ptr librg_peer, channel: u16, reliable: b8, data: pointer, size: usize) {.implibrgC.}

#  =======================================================================
#  !
#  ! Helper methods
#  !
#  =======================================================================
# *
#  * Get current execution time
#  * On server side returns time aprox since process is up and running
#  * On client side returns synced time client time to server time.
#  * Ideally calling this method on both connected client and server at the same time
#  * will return pretty much similar time, or at least 2 that are quite close to one another
#  *
#  * Retuned time is a CLOCK_MONOTONIC, the format is second before comma,
#  * and parts lesser than second after comma
#  *
#  * EXAMPLE:
#  *     1.2424244 - 1 second 242 milliseconds
#  *     255.4224244 - 255 seconds
# 
proc librg_time_now*(ctx: ptr librg_ctx): f64 {.implibrgC.}

# *
#  * Calcualte standard devaiation for a set of values
#  * Example:
#  * f64 values[3] = { 1, 2, 3 };
#  * f64 dev = librg_standard_deviation(&values, 3);
# 
proc librg_standard_deviation*(values: ptr f64, size: usize): f64 {.implibrgC.}

# *
#  * Get global cross-instance option for librg
# 
proc librg_option_get*(option: u32): u32 {.implibrgC.}

# *
#  * Set global cross-instance option for librg
# 
proc librg_option_set*(option: u32, value: u32) {.implibrgC.}
proc zpl_ring_librg_snapshot_init*(pad: ptr zpl_ring_librg_snapshot, a: zpl_allocator, max_size: zpl_isize) {.implibrgC.}
proc zpl_ring_librg_snapshot_free*(pad: ptr zpl_ring_librg_snapshot) {.implibrgC.}
proc zpl_ring_librg_snapshot_full*(pad: ptr zpl_ring_librg_snapshot): zpl_b32 {.implibrgC.}
proc zpl_ring_librg_snapshot_empty*(pad: ptr zpl_ring_librg_snapshot): zpl_b32 {.implibrgC.}
proc zpl_ring_librg_snapshot_append*(pad: ptr zpl_ring_librg_snapshot, data: librg_snapshot) {.implibrgC.}
proc zpl_ring_librg_snapshot_append_array*(pad: ptr zpl_ring_librg_snapshot, data: ptr librg_snapshot) {.implibrgC.}
proc zpl_ring_librg_snapshot_get*(pad: ptr zpl_ring_librg_snapshot): ptr librg_snapshot {.implibrgC.}
proc zpl_ring_librg_snapshot_get_array*(pad: ptr zpl_ring_librg_snapshot, max_size: zpl_usize, a: zpl_allocator): ptr librg_snapshot {.implibrgC.}


