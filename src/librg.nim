import os, strutils

import nimterop/[cimport, build, paths]

when defined(Windows):
  import winim

# Documentation:
#   https://github.com/nimterop/nimterop
#   https://nimterop.github.io/nimterop/cimport.html

const
  # Location where any sources should get downloaded. Adjust depending on
  # actual location of wrapper file relative to project.
  includePath = currentSourcePath.parentDir().parentDir() / "include"

static:
  # Print generated Nim to output
  cDebug()
  # cDisableCaching()

cPlugin:
  import strutils

  # Symbol renaming examples
  proc onSymbol*(sym: var Symbol) {.exportc, dynlib.} =
    # Get rid of leading and trailing underscores
    sym.name = sym.name.strip(chars = {'_'})
    sym.name = sym.name.replace("__", "_")
    sym.name = sym.name.replace("va_list", "types.va_list")


cOverride:
  const ENET_BUFFER_MAXIMUM = 65
  type
    # voidpf = ptr object
    # voidpc = ptr object
    # voidp = ptr object
    # uLongf = culong
    # z_size_t = culong
    # z_crc_t = culong
    # alloc_func* {.importc.} = proc(opaque: voidpf, items, size: uint) {.cdecl.}
    # Bytef {.importc.} = object

    zpl_opts_entry_sub* {.union.} = object
      text*: zpl_string
      integer*: zpl_i64
      real*: zpl_f64

    zpl_opts_entry* = object
      name*: cstring
      lname*: cstring
      desc*: cstring
      `type`*: zpl_u8
      met*: zpl_b32
      pos*: zpl_b32

      opts*: zpl_opts_entry_sub

    zpl_xy* = object
      x*: zpl_f32
      y*: zpl_f32

    zpl_xyz* = object
      x*: zpl_f32
      y*: zpl_f32
      z*: zpl_f32

    zpl_rgb* = object
      r*: zpl_f32
      g*: zpl_f32
      b*: zpl_f32

    zpl_vec2* {.union.} = object
      xy*: zpl_xy
      e*: array[0..1, zpl_f32]

    zpl_vec3* {.union.} = object
      xyz*: zpl_xyz
      rgb*: zpl_rgb

      xy*: zpl_vec2
      e*: array[0..2, zpl_f32]

    zpl_xyzw* = object
      x*: zpl_f32
      y*: zpl_f32
      z*: zpl_f32
      w*: zpl_f32

    zpl_rgba* = object
      r*: zpl_f32
      g*: zpl_f32
      b*: zpl_f32
      a*: zpl_f32

    zpl_xy_zw_vec4* = object
      xy*: zpl_vec2
      zw*: zpl_vec2

    zpl_vec4* {.union.} = object
      xyzw*: zpl_xyzw
      rgba*: zpl_rgba
      xy_zw_vec4*: zpl_xy_zw_vec4
      xyz*: zpl_vec3
      rgb*: zpl_vec3
      e*: array[0..3, zpl_f32]

    zpl_vec2_mat2* = object
      x*: zpl_vec2
      y*: zpl_vec2

    zpl_mat2* {.union.} = object
      zpl_vec2_mat2*: zpl_vec2_mat2
      col*: array[0..1, zpl_vec2]
      e*: array[0..3, zpl_f32]

    zpl_vec3_mat3* = object
      x*: zpl_vec3
      y*: zpl_vec3
      z*: zpl_vec3

    zpl_mat3* {.union.} = object
      zpl_vec3_mat3*: zpl_vec3_mat3
      col*: array[0..2, zpl_vec3]
      e*: array[0..8, zpl_f32]

    zpl_vec4_mat4* = object
      x*: zpl_vec4
      y*: zpl_vec4
      z*: zpl_vec4
      w*: zpl_vec4

    zpl_mat4* {.union.} = object
      zpl_vec4_mat4*: zpl_vec4_mat4
      col*: array[0..3, zpl_vec4]
      e*: array[0..15, zpl_f32]

    zpl_quat* {.union.} = object
      zpl_xyzw*: zpl_xyzw
      xyzw*: zpl_vec4
      xyz*: zpl_vec3
      e*: array[0..3, zpl_f32]

    in6_addr* {.pure.} = object
      Byte*: array[16, uint8]

    ENetHost* = object
      socket*: ENetSocket
      address*: ENetAddress
      incomingBandwidth*: enet_uint32
      outgoingBandwidth*: enet_uint32
      bandwidthThrottleEpoch*: enet_uint32
      mtu*: enet_uint32
      randomSeed*: enet_uint32
      recalculateBandwidthLimits*: cint
      peers*: UncheckedArray[ENetPeer]
      peerCount*: size_t
      channelLimit*: size_t
      serviceTime*: enet_uint32
      dispatchQueue*: ENetList
      continueSending*: cint
      packetSize*: size_t
      headerFlags*: enet_uint16
      commands*: UncheckedArray[array[0..(ENET_PROTOCOL_MAXIMUM_PACKET_COMMANDS-1).int, ENetProtocol]]
      commandCount*: size_t
      buffers*: UncheckedArray[array[0..(ENET_BUFFER_MAXIMUM-1).int, ENetBuffer]]
      bufferCount*: size_t
      checksum*: ENetChecksumCallback
      compressor*: ENetCompressor
      packetData*: array[0..1, array[0..(ENET_PROTOCOL_MAXIMUM_MTU-1).int, enet_uint8]]
      receivedAddress*: ENetAddress
      receivedData*: UncheckedArray[enet_uint8]
      receivedDataLength*: size_t
      totalSentData*: enet_uint32
      totalSentPackets*: enet_uint32
      totalReceivedData*: enet_uint32
      totalReceivedPackets*: enet_uint32
      intercept*: ENetInterceptCallback
      connectedPeers*: size_t
      bandwidthLimitedPeers*: size_t
      duplicatePeers*: size_t
      maximumPacketSize*: size_t
      maximumWaitingData*: size_t

    zpl_atomic_ptr* = object
      value*: ptr cint # TODO: should be volatile
    zpl_affinity* = object
      is_accurate*: zpl_b32
      core_count*: zpl_isize
      thread_count*: zpl_isize
      core_masks*: array[0..(8 * sizeof(size_t) - 1), zpl_usize]

    zpl_json_object_real* = object
      real*: zpl_f64
      base*: zpl_i32
      base2*: zpl_i32
      exp*: zpl_i32
      exp_neg*{.bitsize:1.}: zpl_u8
      lead_digit*{.bitsize:1.}: zpl_u8

    zpl_json_object_union* {.union.} = object
      nodes*: ptr UncheckedArray[zpl_json_object]
      integer*: zpl_i64
      str*: cstring
      real*: zpl_json_object_real
      constant*: zpl_u8

    zpl_json_object* = object
      backing*: zpl_allocator
      name*: cstring
      typ*{.bitsize:6.}: zpl_u8
      name_style*{.bitsize:2.}: zpl_u8
      props*{.bitsize:7.}: zpl_u8
      cfg_mode*{.bitsize:1.}: zpl_u8
      assign_style*{.bitsize:4.}: zpl_u8
      delim_style*{.bitsize:4.}: zpl_u8
      delim_line_width*: zpl_u8 

      zpl_json_object_u*: zpl_json_object_union

cIncludeDir(includePath)

# Perform OS specific tasks
when defined(windows):
  # Windows specific symbols, options and files

  # Dynamic library to link against
  const dynlibFile = "librg.dll"

cImport(includePath / "librg.h", recurse = true, dynlib = "dynlibFile")
